import sys
import os
from copy import copy
from .abc import *
from .jsmenutemplate import JSMENU_TEMPLATE
from typing import *
from types import *
from decimal import Decimal
from datetime import datetime
import json
import inspect
import asyncio
from Crypto.Cipher import DES3

MENU_SCRIPT_SEPARATOR = '//---MENU HANDLER\n'
MENU_SCRIPT_ID_PREFIX = '//id: '

def timeLiteral(lt):
    return '%04d-%02d-%02d %02d:%02d:%02d' % (lt.tm_year, lt.tm_mon, lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec)

class ScrollableQueryParams(TypedDict):
    selectFromQueryText: str
    sortFields: List[str]
    keyField: str
    fieldMap: dict
    andWherePart: Optional[str]
    moreParams: dict
    addOrderByPart: Optional[str]
    columnTitles: dict

def getScrollableQueryTemplate(selectFromPart, sortFields, keyField, fieldMap={}, andWherePart=None, addOrderByPart=None, 
    isBound=0, valueBound=None, keyBound=None, sortField=None, sortDir=None, **kwBoundaryArgs):

    v = kwBoundaryArgs.get('is_bound', 0)
    isSearch = kwBoundaryArgs.get('is_search', 0)
    
    isBound = 1 if v else 0
    valueBound = kwBoundaryArgs.get('value_bound')
    keyBound = kwBoundaryArgs.get('key_bound')

    sortField = kwBoundaryArgs.get('sort_field')
    sortField = sortField if sortField in sortFields else keyField
    mappedSortField = fieldMap[sortField] if sortField in fieldMap else sortField

    mappedKeyField = fieldMap[keyField] if keyField in fieldMap else keyField

    sortDir = kwBoundaryArgs.get('sort_dir')
    sortDir = sortDir.upper() if sortDir in ('ASC', 'DESC', 'asc', 'desc', ) else 'ASC'
    sign = '>' if sortDir == 'ASC' else '<'
    sign += '=' if isSearch else ''

    navDir = kwBoundaryArgs.get('nav_dir')
    navDir = navDir if navDir in ('next', 'prev', ) else 'next'

    sign = sign if navDir == 'next' else {'>': '<', '<': '>', '>=': '<=', '<=': '>='}[sign] # reverse sign if navDir == 'prev'
    sortDir = sortDir if navDir == 'next' else {'ASC': 'DESC', 'DESC': 'ASC'}[sortDir] # reverse sortDir if navDir == 'prev

    andWherePart = (' AND (%s) ' % andWherePart) if (type(andWherePart) is str and andWherePart != '') else ''
    addOrderByPart = (', %s' % addOrderByPart) if (type(addOrderByPart) is str and addOrderByPart != '') else ''

    sqlMetaTemplate = '''%(selectFromPart)s
            WHERE
                ( (
                    %%(isBound)s = 0 OR 
                    %(mappedSortField)s %(sign)s %%(valueBound)s OR 
                    (%(mappedSortField)s = %%(valueBound)s AND %(mappedKeyField)s %(sign)s %%(keyBound)s)
                ) ) 
                %(andWherePart)s
            ORDER BY %(mappedSortField)s %(sortDir)s, %(mappedKeyField)s %(sortDir)s %(addOrderByPart)s
        ''' if not isSearch else '''%(selectFromPart)s
            WHERE
                ( (
                    %(mappedSortField)s %(sign)s %%(valueBound)s 
                ) ) 
                %(andWherePart)s
            ORDER BY %(mappedSortField)s %(sortDir)s, %(mappedKeyField)s %(sortDir)s %(addOrderByPart)s
        ''' 
    sqlTemplate = sqlMetaTemplate % locals()
    sqlParams = {'isBound': isBound, 'valueBound': valueBound, 'keyBound': keyBound, 'navDir': navDir, 'sortField': sortField} 
    # print('SQL template = ', sqlTemplate)
    # print('Parameters = ', sqlParams)
    return sqlTemplate, sqlParams
#-- def scrollableQueryTemplate

def loadTextFile(fileName) -> str:
    with open(fileName, 'r+') as f:
        sFileContent = ''
        sRead = f.read()
        while sRead != '':
            sFileContent += sRead
            sRead = f.read()
        return sFileContent
    #--
    pass
#-- loadTextFile

def writeTextFile(fileName, sFileContent: str) -> None:
    with open(fileName, 'w+') as f:
        f.write(sFileContent)
    #--
#-- writeTextFile

class StdAppModule(IAppModule): # StdAppModule is base class to be inherited by data handler for every resource
    resources: dict
    MAX_QUERY_ROW = 25
    allowAccessFrom: Union[None, str, list]

    def __init__(self, moduleId: str, moduleFileName: str, appResource: IAppResource):
        self.appResource = appResource
        self.engine = appResource.appServerEngine
        self.bypassAccessCheck = False 
        self.allowAccessFrom = []
        self.resources = {}
        self.moduleId = moduleId
        self.moduleFileName = moduleFileName

    def convertQueryFields(self, rows: List[dict], needConversionColumns):
        for row in rows:
            if row == None:
                continue
            for fname in needConversionColumns:
                value = row.get(fname)
                row[fname] = float(value) if isinstance(value, Decimal) else str(value)[:10] if isinstance(value, datetime) else value
        return rows

    async def handleScrollableQuery(self, dRequest, selectFromQueryText, sortFields, keyField, fieldMap = {}, andWherePart=None, moreParams = {}, addOrderByPart=None, columnTitles: dict={}):
        appResource = self.appResource
        maxRow = dRequest.get('max_row')
        maxRow = maxRow if type(maxRow) is int else self.MAX_QUERY_ROW

        # dRequest may contain {isBound, valueBound, keyBound, sortField, sortDir} bound parameters
        sqlTemplate, params = getScrollableQueryTemplate(selectFromQueryText, sortFields, keyField, fieldMap, andWherePart, addOrderByPart, **dRequest)
        params.update(moreParams)

        async with appResource.dbTransact() as conn:
            rows, (columns, columnsIdx, needConversionColumns) = await appResource.dbQueryConn(conn, sqlTemplate, params, min(maxRow, self.MAX_QUERY_ROW), True)
            moreData = rows[-1] == None if len(rows) > 0 else False
            if (moreData):
                rows = rows[:-1]
            # print(f'Query executed: {len(rows)} rows')

        self.convertQueryFields(rows, needConversionColumns)

        #-- async with transact
        navDir = params['navDir']
        isBound = params['isBound']
        if navDir == 'prev': # reverse row
            rows.reverse()

        prevDataAvailable = (navDir == 'next' and isBound != 0) or (navDir == 'prev' and moreData)
        nextDataAvailable = (navDir == 'prev' and isBound != 0) or (navDir == 'next' and moreData)

        clientFields = list(map(lambda col: {
            'name': col.name, 'type': col.ctype, 
            'title': columnTitles[col.name] if col.name in columnTitles else col.name
        }, columns)) if dRequest.get('get_client_fields') == 'true' else []

        return {
            'status': '000', 'description': 'success', 'rows': rows, 
            'prev_data': prevDataAvailable, 'next_data': nextDataAvailable, 
            'sort_fields': sortFields, 'sort_field': params['sortField'],
            'client_fields': clientFields,
            'key_field': keyField
        }
    #-- handleScrollableQuery

    async def getMenu(self, dRequest: dict, dataId: str, structureFile: str, scriptFile: str):
        def loadStructureFile():
            sStructure = loadTextFile(structureFile)
            return json.loads(sStructure)

        def loadAndFilterScriptFile(scriptFile: str, listIds: list):
            sScriptFile = loadTextFile(scriptFile)
            chunks = sScriptFile.split(MENU_SCRIPT_SEPARATOR)
            dScripts = {}
            for chunk in chunks:
                lines = chunk.split('\n')
                if len(lines) > 0 and lines[0].find(MENU_SCRIPT_ID_PREFIX) == 0:
                    menuId = lines[0].split(MENU_SCRIPT_ID_PREFIX, 1)[1].strip()
                    if menuId in listIds:
                        scriptBody = '\n'.join(lines[1:])
                        dScripts[menuId] = scriptBody
            return dScripts

        def filterMenuArray(arrStructure: List[dict], accessRights: dict, allIds: list) -> list:
            filteredArray = []
            for item in arrStructure:
                incItem = {}
                incItem['title'] = item.get('title', '')
                if 'globalItem' in item:
                    incItem['globalItem'] = item['globalItem']
                if item['title'] == '-':
                    incItem['title'] = '-'
                    filteredArray.append(incItem)
                elif 'key' in item:
                    menuKey = item['key']
                    incItem['key'] = menuKey
                    sAllowedRoles = accessRights.get(menuKey, '')
                    if sAllowedRoles != '*':
                        allowedRoles = sAllowedRoles.split(';')
                        itemSelected = False
                        for role in dRequest[RKEY_ROLES]:
                            if role in allowedRoles:
                                itemSelected = True
                                break
                    else:
                        itemSelected = True
                    if itemSelected:
                        filteredArray.append(incItem); allIds.append(menuKey)
                        subItems = filterMenuArray(item.get('items', []), accessRights, allIds)
                        if len(subItems) > 0:
                            incItem['items'] = subItems
                else:
                        subItems = filterMenuArray(item.get('items', []), accessRights, allIds)
                        if len(subItems) > 0:
                            filteredArray.append(incItem)
                            incItem['items'] = subItems
                #-- if 
                pass
            #--
            return filteredArray

        def getCacheFileBaseName():
            return self.appResource.getCacheDir() + 'mnu-' + self.moduleId + '-' + dataId + '-' + dRequest[RKEY_ACCESSID]

        def checkCache():
            # compare: 
            # cache file (if exists)
            # dRequest[RKEY_ACCESS_MODTIME]
            # menu file
            # 
            cacheFileBaseName = getCacheFileBaseName()
            cacheFileName = cacheFileBaseName + '.js'
            if os.access(cacheFileName, os.F_OK):
                cacheFTime = os.path.getmtime(cacheFileName)
                accessModTime = dRequest[RKEY_ACCESS_MODTIME]
                structFTime = os.path.getmtime(structureFile)
                scriptFTime = os.path.getmtime(scriptFile)

                if cacheFTime > accessModTime and cacheFTime > accessModTime and cacheFTime > structFTime and cacheFTime > scriptFTime:
                    return (True, loadTextFile(cacheFileName))
                else:
                    return (False, None)
            else:
                return (False, None)

        def writeCache(sMenuScript: str, allIds: list):
            cacheFileName = getCacheFileBaseName() + '.js'
            writeTextFile(cacheFileName, sMenuScript)
            accessListCacheFileName = getCacheFileBaseName() + '.acl'
            writeTextFile(accessListCacheFileName, '\n'.join(allIds))

        isCached, sCachedMenuScript = await asyncio.get_event_loop().run_in_executor(None, checkCache)
        if not isCached:
            dStructure = await asyncio.get_event_loop().run_in_executor(None, loadStructureFile)
            accessRights = dStructure.get('access_rights', {})
            arrStructure = dStructure.get('structure', [])
            allIds = []
            filteredStructure = filterMenuArray(arrStructure, accessRights, allIds)
            filteredScript = loadAndFilterScriptFile(scriptFile, allIds)

            sStructure: str = json.dumps(filteredStructure)
            sScript: str = ''        
            for scriptItem in filteredScript:
                lineSep: str = ',\n' if sScript else ''
                sScript += (f'{lineSep}{scriptItem}: {filteredScript[scriptItem]} ')
            sMenuScript: str = JSMENU_TEMPLATE % {'MENU_STRUCTURE': sStructure, 'HANDLER_TABLE': sScript}
            await asyncio.get_event_loop().run_in_executor(None, writeCache, sMenuScript, allIds)
        else:
            sMenuScript: str = sCachedMenuScript
        return sMenuScript

    async def getMenuTokens(self, dRequest: dict, dataId: str, structureFile: str):

        def loadStructureFile():
            sStructure = loadTextFile(structureFile)
            return json.loads(sStructure)

        def filterMenuTokens(arrStructure: List[dict], accessRights: dict, allIds: list) -> list:
            for item in arrStructure:
                if 'key' in item:
                    menuKey = item['key']
                    sAllowedRoles = accessRights.get(menuKey, '')
                    if sAllowedRoles != '*':
                        allowedRoles = sAllowedRoles.split(';')
                        itemSelected = False
                        for role in dRequest[RKEY_ROLES]:
                            if role in allowedRoles:
                                itemSelected = True
                                break
                    else:
                        itemSelected = True
                    if itemSelected:
                        allIds.append(menuKey)
                        filterMenuTokens(item.get('items', []), accessRights, allIds)
                else:
                        filterMenuTokens(item.get('items', []), accessRights, allIds)
                #-- if 
                pass
            #--

        def getCacheFileBaseName():
            return self.appResource.getCacheDir() + 'mnu-' + self.moduleId + '-' + dataId + '-' + dRequest[RKEY_ACCESSID]

        def checkACLCache():
            cacheFileBaseName = getCacheFileBaseName()
            cacheFileName = cacheFileBaseName + '.acl'
            if os.access(cacheFileName, os.F_OK):
                cacheFTime = os.path.getmtime(cacheFileName)
                accessModTime = dRequest[RKEY_ACCESS_MODTIME]
                structFTime = os.path.getmtime(structureFile)

                if cacheFTime > accessModTime and cacheFTime > accessModTime and cacheFTime > structFTime:
                    return (True, loadTextFile(cacheFileName).split('\n'))
                else:
                    return (False, None)
            else:
                return (False, None)

        def writeACLCache(allIds: list):
            accessListCacheFileName = getCacheFileBaseName() + '.acl'
            writeTextFile(accessListCacheFileName, '\n'.join(allIds))

        isCached, allIds = await asyncio.get_event_loop().run_in_executor(None, checkACLCache)
        if not isCached:
            dStructure = await asyncio.get_event_loop().run_in_executor(None, loadStructureFile)
            accessRights = dStructure.get('access_rights', {})
            arrStructure = dStructure.get('structure', [])
            allIds = []
            filterMenuTokens(arrStructure, accessRights, allIds)
            await asyncio.get_event_loop().run_in_executor(None, writeACLCache, allIds)
        
        # calculate token for all Ids here
        allTokens = []
        for id in allIds:
            allTokens.append({'id': id, 'token': self.calculateAuthToken(dRequest, 'menu', dataId, id) })
        return allTokens

    async def getSingleData(self, dRequest: dict, dataId: str, qSpec: dict) -> dict:

        async def internalGetSingleData(ownerRow: dict, qSpec: dict) -> list:
            appResource = self.appResource
            result: list = []
            async with appResource.dbTransact() as conn:
                sqlText = qSpec.get('query')
                if not sqlText:
                    raise WebServerResponseError(503, 'Invalid parameter: no sql text is specified')
                sqlParams = qSpec.get('params', {})
                links: dict = qSpec.get('link')
                if ownerRow and isinstance(links, dict):
                    for (pName, pSource) in links.items():
                        sqlParams[pName] = ownerRow.get(pSource)
                rows, (columns, columnsIdx, needConversionColumns) = await appResource.dbQueryConn(
                    conn, sqlText, sqlParams, self.MAX_QUERY_ROW, True)
                self.convertQueryFields(rows, needConversionColumns)

                subFields: dict = qSpec.get('subquery_fields', {})
                for row in rows:
                    if not row:
                        continue
                    thisRow = copy(row)
                    result.append(thisRow)
                    for subFieldKey in subFields:
                        subRows = await internalGetSingleData(thisRow, subFields.get(subFieldKey))
                        thisRow[subFieldKey] = subRows
                    #-- for
                    pass
                #-- for
                pass
            #-- def
            return result
            
        rows = await internalGetSingleData(None, qSpec)
        return {'status': '000', 'description': 'success', 'data': rows}
    
    # check required tokens for this module
    # the check will be carried out when auth_token for this module is requested
    def checkModuleAccessToken(self, dRequest):
        tokenKey: str = bytes().fromhex(dRequest[RKEY_KEY])
        enc = DES3.new(tokenKey)
        token = dRequest.get('access_token', None)
        if not token:
            return False
        try:
            #decResourceTokens = [decToken.rsplit('/', 1)[0] for decToken in map(lambda token: enc.decrypt(bytes().fromhex(token)).decode(encoding='utf-8', errors='ignore'), tokens)]
            decToken = enc.decrypt(bytes().fromhex(token)).decode(encoding='utf-8', errors='ignore').rsplit('/', 1)[0]
        except Exception as e:
            # print('Error decoding token: ', repr(token))
            return False
        if isinstance(self.allowAccessFrom, str):
            return decToken == self.allowAccessFrom
        elif isinstance(self.allowAccessFrom, list) or isinstance(self.allowAccessFrom, tuple):
            return decToken in self.allowAccessFrom
            #return functools.reduce(lambda s, t: s and (t in decResourceTokens), self.requiredAccessTokens, True)
        else:
            return True

    # calculate auth token for resource within this module or for the module itself
    def calculateAuthToken(self, dRequest: dict, resourceType, dataId: str, subId = None) -> str:
        tokenKey: bytes = bytes().fromhex(dRequest[RKEY_KEY])
        enc = DES3.new(tokenKey)
        sessionTimeStr: str = dRequest[RKEY_TIME]
        tokenStr = f'{resourceType}/{self.moduleId}/{dataId}{("/" + subId) if subId else ""}/{sessionTimeStr}'
        tokenStr += ('+' * (8 - (len(tokenStr) % 8)))
        return enc.encrypt(tokenStr.encode(encoding='utf-8')).hex()

    # check required module token (that is associated to this module) when acquiring resource of this module
    def checkResourceAuthToken(self, dRequest: dict):
        authToken = dRequest.get('auth_token')
        if not authToken:
            return False
        tokenKey: bytes = bytes().fromhex(dRequest[RKEY_KEY])
        enc = DES3.new(tokenKey)
        sessionTimeStr: str = dRequest[RKEY_TIME]
        try:
            moduleIdent, calcSessionTimeStr = enc.decrypt(bytes().fromhex(authToken)).decode(encoding='utf-8', errors='ignore').rsplit('/', 1)
            calcSessionTimeStr = calcSessionTimeStr.split('+', 1)[0]
        except:
            return False
        return calcSessionTimeStr == sessionTimeStr and moduleIdent == f'module/{self.moduleId}/auth_token'

    async def handleRequest(self, dRequest: dict, rawRequest: Any = None) -> Tuple[Union[dict, str], str]: #returns data and MIME type
        ELLIPSIS = '...'
        NO_ELLIPSIS = ''

        dScrollableParams: ScrollableQueryParams

        dataId = str(dRequest['data_id'])
        if dataId == 'auth_token': # special id for getting authentication token of this moduleId
            if not self.bypassAccessCheck and not self.checkModuleAccessToken(dRequest):
                raise WebServerResponseError(403, 'Access denied (invalid token)')
            return ({'status': '000', 'description': 'success', 'token': self.calculateAuthToken(dRequest, 'module', 'auth_token') }, 'application/json')
        else:
            if dataId in self.resources:
                resource = self.resources[dataId]
                reqResourceType = dRequest.get('type')
                resourceType = str(resource.get('type'))
                if reqResourceType == 'menu_tokens':
                    isGetToken: bool = True
                    reqResourceType = 'menu'
                else:
                    isGetToken: bool = False
                if resourceType != reqResourceType:
                    raise WebServerResponseError(404, 'Unmatched resource type')
                if resourceType == 'scroll_query':
                    handler = resource['handler']
                    if not callable(handler):
                        raise WebServerResponseError(405, 'Invalid (non-callable) handler')
                    if not self.bypassAccessCheck and not self.checkResourceAuthToken(dRequest):
                        raise WebServerResponseError(403, 'Access denied (invalid token)')
                    dScrollableParams = handler(self.moduleId, dataId, dRequest) if inspect.ismethod(handler) else handler(self, self.moduleId, dataId, dRequest)
                    return (await self.handleScrollableQuery(dRequest, **dScrollableParams), 'application/json')
                elif resourceType == 'single_data':
                    handler = resource['handler']
                    if not callable(handler):
                        raise WebServerResponseError(405, 'Invalid (non-callable) handler')
                    if not self.bypassAccessCheck and not self.checkResourceAuthToken(dRequest):
                        raise WebServerResponseError(403, 'Access denied (invalid token)')
                    qSpec = handler(self.moduleId, dataId, dRequest) if inspect.ismethod(handler) else handler(self, self.moduleId, dataId, dRequest)
                    return (await self.getSingleData(dRequest, dataId, qSpec), 'application/json')
                elif resourceType == 'menu':
                    paths = self.moduleFileName.rsplit(os.sep, 1)
                    moduleFilePath = paths[0] if len(paths) > 1 else ''
                    structureFile = os.path.join(moduleFilePath, resource['structure_file'])
                    scriptFile = os.path.join(moduleFilePath, resource['script_file'])
                    absCurrentPath = os.path.abspath('')
                    if (structureFile and os.path.abspath(structureFile).find(absCurrentPath) != 0) or (scriptFile and os.path.abspath(scriptFile).find(absCurrentPath) != 0):
                        raise WebServerResponseError(403, 'Security violation for resource "%s" data_id "%s" ' % (self.moduleId, dataId))
                    if isGetToken:
                        if not self.bypassAccessCheck and not self.checkResourceAuthToken(dRequest):
                            raise WebServerResponseError(403, 'Access denied (invalid token)')
                        return ({'tokens': await self.getMenuTokens(dRequest, dataId, structureFile)}, 'applicaton/json')
                    else:
                        return (await self.getMenu(dRequest, dataId, structureFile, scriptFile), 'text/javascript')
                elif resourceType == 'raw_data' or resourceType == 'method':
                    handler = resource['handler']
                    if not callable(handler):
                        raise WebServerResponseError(405, 'Invalid (non-callable) handler')
                    if not self.bypassAccessCheck and not self.checkResourceAuthToken(dRequest):
                        raise WebServerResponseError(403, 'Access denied (invalid token)')
                    if inspect.iscoroutinefunction(handler):
                        handlerResult = await handler(self.moduleId, dataId, dRequest, rawRequest)
                    else:
                        handlerResult = handler(self.moduleId, dataId, dRequest, rawRequest)
                    if isinstance(handlerResult, dict):
                        returnResult = handlerResult
                        mimeType = 'application/json'
                    elif not isinstance(handlerResult, tuple) or len(handlerResult) != 2 or not isinstance(handlerResult[1], str):
                        raise WebServerResponseError(503, 'Invalid handler result (tuple with two items or JSON required)')
                    else:
                        returnResult, mimeType = handlerResult
                    return (returnResult, mimeType)
                else:
                    raise WebServerResponseError(404, 'Unsupported resource type (%s) for resource "%s" data_id "%s" ' % (resourceType, self.moduleId, dataId))
            else:
                raise WebServerResponseError(404, f'unknown resource \"{repr(dataId)[:50]}{ELLIPSIS if len(repr(dataId)) > 50 else NO_ELLIPSIS}\" designated in "data_id" parameter')


