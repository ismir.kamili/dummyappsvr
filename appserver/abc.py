import sys
import os
from typing import *
import types

MODULE_PATH_SEPARATOR = '.'

RKEY_KEY = 'session.key'
RKEY_APPID = 'session.appId'
RKEY_CONFIGID = 'session.configId'
RKEY_USERID = 'session.userId'
RKEY_USERNAME = 'session.userName'
RKEY_ROLES = 'session.roles'
RKEY_MODTIME = 'session.userModTime'
RKEY_ACCESSID = 'session.accessId'
RKEY_ACCESS_MODTIME = 'session.accessModTime'
RKEY_ACCESSREFID = 'session.accessRefId'
RKEY_TIME = 'session.creationTime'

DEFAULT_RESOURCE_TYPE = 'postgres'

class UnimplementedError(Exception):
    def __init__(self, *args):
        super().__init__('Abstract method invoked', *args)

class WebServerResponseError(Exception):
    errCode: int
    errInfo: str
    def __init__(self, errCode: int, errInfo: str, *args):
        self.errCode = errCode
        self.errInfo = errInfo
        super().__init__(*(errCode, errInfo), *args)

class IAppServerEngine:
    rootAppPath: str
    absRootAppPath: str
    rootModulePath: str

    def __init__(self, appServer: Any, rootConfig: dict): raise UnimplementedError()
    def getAppConfig(self, appId: str, configId: str) -> dict: raise UnimplementedError()
    async def start(self) -> None: raise UnimplementedError()
    async def activateResource(self, appId: str, configId: str) -> 'IAppResource': raise UnimplementedError()
    async def login(self, dRequest: dict) -> dict: raise UnimplementedError()
    async def checkSession(self, sessionId: bytes) -> tuple: raise UnimplementedError()
    async def deleteSession(self, sessionId: bytes) -> None: raise UnimplementedError()
    async def getModule(self, dRequest: dict, rawRequest: Any = None) -> str: raise UnimplementedError()
    async def getResource(self, dRequest: dict, rawRequest: Any = None) -> Tuple[Union[dict, str], str]: raise UnimplementedError()

class IAppResource:
    appServerEngine: IAppServerEngine # forward declaration
    def __init__(self, appId: str, configId: str, appServerEngine: IAppServerEngine, appConfig: dict): raise UnimplementedError()
    async def dbEngineConnect(self) -> Any: raise UnimplementedError()
    def dbTransact(self) -> Any: raise UnimplementedError()
    async def dbQueryConn(self, db_conn: Any, sql: str, params=(), maxRowCount: int=None, withColumnInfo: bool=False) -> Any: raise UnimplementedError()
    async def dbQuery(self, sql: str, params=(), maxRowCount: int=None, withColumnInfo: bool=False) -> Any: raise UnimplementedError()
    async def dbExecConn(self, db_conn: Any, sql, params = ()) -> Any: raise UnimplementedError()
    async def dbExec(self, sql: str, params=()) -> Any: raise UnimplementedError()
    async def dbGetSequence(self, sequenceName: str) -> int: raise UnimplementedError()
    async def activate(self) -> None: raise UnimplementedError()
    async def resolveFilePath(self, filePath: str) -> str: raise UnimplementedError()
    def resolveModulePath(self, modulePath: str) -> str: raise UnimplementedError()
    def getCacheDir(self) -> str: raise UnimplementedError()

class IWebAppServer:
    def apiResponse(self, dResult: dict, cookies=None, status: int=200, reason: str='OK', content_type: str='application/json'): raise UnimplementedError()

class IAppModule:
    appResource: IAppResource
    
    async def handleRequest(self, dRequest: dict, rawRequest: Any = None) -> dict: raise UnimplementedError()

class IResourceFactory:
    def createAppResource(appId: str, configId: str, appServerEngine: IAppServerEngine, appConfig: dict) -> IAppResource:
        raise UnimplementedError()
 