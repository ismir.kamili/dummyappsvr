import os
import sys
import time
import types
from datetime import datetime
import importlib
import base64
import json
import os.path
import pathlib
from typing import *
import asyncio
import aioredis
from Crypto.Cipher import DES3

from .abc import *

DEFAULT_SESSION_TIMEOUT = 1800 # default session time out for 30 minutes
DEFAULT_APP_ID = 'default'
DEFAULT_CONFIG_ID = 'defaultapp'
CKEY_ROOT_APP_PATH = 'root_app_path'

class DetailSessionInfo:
    userName: str
    roles: str # list of roles separated by ; character
    userLastMod: datetime
    accessId: str
    accessLastMod: datetime
    accessRefId: str

def getSessionId(appSessionKey) -> bytes:
    counter = 0
    while True:
        counter += 1
        baseValue = (hex(os.getpid())[2:].encode() + os.urandom(8)).ljust(16, b'0') + hex(counter)[2:].zfill(8)[:8].encode()
        encSession = DES3.new(appSessionKey)
        encValue = encSession.encrypt(baseValue)
        yield base64.b64encode(encValue) # 32-bytes

def getSessionIdRedisKey(sessionId: str) -> str: # bSessionId is assumed as result from base64 encoding and hence is safe to decode
    return f'iappsvr:{hex(os.getpid())[2:].zfill(16)}:{sessionId}'

class BasicAppServerEngine(IAppServerEngine):
    def __init__(self, appServer, rootConfig: dict, resourceFactories: Dict[str, IResourceFactory]):
        self.appServer = appServer
        self.rootConfig = rootConfig
        self.rootAppPath = os.path.join(rootConfig.get(CKEY_ROOT_APP_PATH, ''), '')
        self.resourceFactories = resourceFactories
        self.absRootAppPath = os.path.abspath(self.rootAppPath)
        self.cachedAppConfigs = {} # key: (appId, configId)
        self.appConfigsTimeStamp = {} # key: (appId, configId)
        self.cachedAppResources = {} # key: (appId, configId)
        self.cachedDataModules = {} # key: Tuple[appId: str, moduleId: str] value: Tuple[loadTime: float, module, class_instance]
        self.sessionTimeout: int = rootConfig.get('session_timeout', None) or DEFAULT_SESSION_TIMEOUT 
        appSessionKeyStr: str = rootConfig.get('app_session_key', None)
        if appSessionKeyStr == None:
            raise Exception('Undefined app_session_key')
        try:
            appSessionKey = bytes.fromhex(appSessionKeyStr)
        except:
            raise Exception(f'Invalid app session key "{appSessionKeyStr}"')

        self.genSession = getSessionId(appSessionKey)
        self.redisConn = None
        pathComps = self.rootAppPath.split(os.sep)
        for p in pathComps:
            if p != '' and not str.isalnum(p):
                raise Exception('Invalid path component in "%s" config element' % (CKEY_ROOT_APP_PATH))
        self.rootModulePath = '.'.join(pathComps)

    def getAppConfig(self, appId, configId):
        appConfigFileName = self.rootAppPath + appId + os.sep + (configId or 'default') + '.json'
        configKey = (appId, configId)
        if configKey in self.cachedAppConfigs:
            lastUpdate = pathlib.Path(appConfigFileName).stat().st_mtime
            cachedUpdate = self.appConfigsTimeStamp[configKey]
            isReload = lastUpdate > cachedUpdate
        else:
            filePath = pathlib.Path(appConfigFileName)
            if not filePath.exists():
                raise Exception(f'Cannot open app config file {appConfigFileName}')
            lastUpdate = pathlib.Path(appConfigFileName).stat().st_mtime
            isReload = True

        if isReload:
            with open(appConfigFileName, 'rb') as cf:
                sConfig = cf.read().decode(encoding='utf-8')
            appConfig = json.loads(sConfig)
            self.cachedAppConfigs[configKey] = appConfig
            self.appConfigsTimeStamp[configKey] = lastUpdate
            return appConfig
        else:
            return self.cachedAppConfigs[configKey]

    async def start(self) -> None:
        self.redisConn = await aioredis.create_redis_pool(
            (
                self.rootConfig.get('redis_host', 'localhost'),
                self.rootConfig.get('redis_port', 6379)
            ), encoding='utf-8'
        )

    async def activateResource(self, appId: str, configId: str) -> IAppResource:
        configKey = (appId, configId)
        resource = self.cachedAppResources[configKey] if configKey in self.cachedAppResources else None
        if resource == None:
            config = await asyncio.get_event_loop().run_in_executor(None, self.getAppConfig, appId, configId)
            resourceType: str = config.get('resource', DEFAULT_RESOURCE_TYPE)
            resourceFactory: IResourceFactory = self.resourceFactories.get(resourceType, None)
            if resourceFactory == None:
                raise Exception(f'Invalid resource type "{resourceType}" ')
            resource = resourceFactory.createAppResource(appId, configId, self, config)
            await resource.activate()
            self.cachedAppResources[configKey] = resource
            return resource
        else:
            return resource

    async def validateUser(self, resource, userId, password):
        async with resource.dbTransact() as conn:
            qUser = await resource.dbQueryConn(conn, 'select * from users where user_id = %s and enc_password = %s', [userId, password])
        return (len(qUser) > 0, qUser[0] if len(qUser) > 0 else None)

    async def login(self, dRequest: dict) -> dict:
        userId = dRequest.get('user_id')
        password = dRequest.get('password')
        appId = dRequest.get('app_id', DEFAULT_APP_ID)
        if not str.isalnum(appId):
            raise WebServerResponseError(405, 'Invalid appId "%s"' % appId)
        configId = dRequest.get('config_id', DEFAULT_CONFIG_ID)

        resource = await self.activateResource(appId, configId)
        isValid, userData = await self.validateUser(resource, userId, password)

        sessionId = await self.createSession(userId, appId, configId, userData) if isValid else None
        dResponse = {'status': '010', 'description': 'invalid userid / password'} if not isValid else (
            {'status': '000', 'description': 'login successful', 'user_name': userData.get('user_name') or 'Unknown user', 'session_id': sessionId} 
        )
        return dResponse

    async def createDetailSessionInfo(self, resource: IAppResource, userId: str, appId: str, configId: str, userData: dict) -> DetailSessionInfo:
        userLastMod = (userData.get('last_change') or datetime.now()).timestamp()
        userName = userData.get('user_name') or 'Unknown user'
        accessRefId: str = userData.get('access_ref_id', '')
        useAccessRef: bool = accessRefId != ''
        accessId: str = accessRefId if useAccessRef else userId
        accessLastMod: float

        async with resource.dbTransact() as db_conn:
            rows: List[dict]
            row: Union[dict, None]

            if not useAccessRef:
                rows = await resource.dbQueryConn(db_conn, 'SELECT * FROM users_roles WHERE user_id = %s', (userId, ), 25, False)
            else:
                rows = await resource.dbQueryConn(db_conn, 'SELECT * FROM access_ref_roles WHERE access_ref_id = %s', (accessRefId, ), 25, False)
            sRoles = ''
            for row in rows:
                if not row:
                    break
                sRoles += (';' + row['role_id']) if sRoles != '' else row['role_id']

            if useAccessRef:
                qARef = await resource.dbQueryConn(db_conn, 'SELECT * FROM access_ref WHERE access_ref_id = %s', (accessRefId, ))
                accessLastMod = qARef[0].get('last_change', datetime.now()).timestamp()
            else:
                accessLastMod = userLastMod
        
        dsi = DetailSessionInfo()
        dsi.userName = userName
        dsi.roles = sRoles
        dsi.userLastMod = userLastMod
        dsi.accessId = accessId
        dsi.accessLastMod = accessLastMod
        dsi.accessRefId = accessRefId

        return dsi

    async def createSession(self, userId: str, appId: str, configId: str, userData: dict) -> str:
        bSessionId = next(self.genSession)
        sessionIdKey = getSessionIdRedisKey(bSessionId.decode(encoding='utf-8', errors='ignore')) 

        # create redis hash with sessionIdKey as key
        resource = await self.activateResource(appId, configId)
        dsi = await self.createDetailSessionInfo(resource, userId, appId, configId, userData)
        await self.redisConn.hmset(sessionIdKey, 'exist', 1, 'user_id', userId, 'app_id', appId, 
            'config_id', configId, 'roles', dsi.roles, 'user_last_mod', dsi.userLastMod, 
            'user_name', dsi.userName, 'access_id', dsi.accessId, 'access_last_mod', dsi.accessLastMod, 'access_ref_id', dsi.accessRefId, 
            'key', os.urandom(16).hex(),
            'time', str(datetime.now()))

        await self.redisConn.expire(sessionIdKey, self.sessionTimeout)

        return bSessionId.decode(encoding='utf-8', errors='ignore')

    async def checkSession(self, sessionId: str) -> tuple:

        sessionIdKey = getSessionIdRedisKey(sessionId)
        values = await self.redisConn.hmget(sessionIdKey, 
            'exist', 'user_id', 'app_id', 'config_id', 'roles', 'user_last_mod', 
            'user_name', 'access_id', 'access_last_mod', 'access_ref_id', 
            'key', 'time'
        )
        if not values[0]:
            return (False, 'Session expired')
        else:
            await self.redisConn.expire(sessionIdKey, self.sessionTimeout)
        return (True, 
            {
                RKEY_USERID: values[1], RKEY_APPID: values[2], RKEY_CONFIGID: values[3], 
                RKEY_ROLES: (values[4] or '').split(';'), 
                RKEY_MODTIME: float(values[5]), RKEY_USERNAME: values[6], 
                RKEY_ACCESSID: values[7], RKEY_ACCESS_MODTIME: float(values[8]), 
                RKEY_ACCESSREFID: values[9], RKEY_KEY: values[10],
                RKEY_TIME: values[11]
            }
        )

    async def deleteSession(self, sessionId: str) -> None:
        sessionIdKey = getSessionIdRedisKey(sessionId)
        await self.redisConn.delete(sessionIdKey)

    @classmethod
    def getModuleIdAndFile(classType, dRequest: dict, sExtension: str='') -> Tuple[str, str]:
        moduleId = str(dRequest.get('module_name') or dRequest.get('module_id'))
        return moduleId, (moduleId.replace(MODULE_PATH_SEPARATOR, os.sep) + sExtension)

    async def getModule(self, dRequest: dict, rawRequest: Any = None) -> str:
        moduleFileName: str
        appResource: IAppResource

        def readModuleFile(fileName: str) -> str:
            f = open(fileName, 'r+')
            try:
                sFileContent = ''
                sRead = f.read()
                while sRead != '':
                    sFileContent += sRead
                    sRead = f.read()
            finally:
                f.close()
            return sFileContent

        appResource = await self.activateResource(dRequest[RKEY_APPID], dRequest[RKEY_CONFIGID])
        moduleId, moduleFileName = self.getModuleIdAndFile(dRequest, '.js')

        moduleCode = await asyncio.get_event_loop().run_in_executor(None, readModuleFile, appResource.resolveFilePath(moduleFileName))
        return moduleCode
    
    async def getResource(self, dRequest: dict, rawRequest: Any = None) -> Tuple[Union[dict, str], str]: #returns data and MIME type

        moduleId: str
        appResource: IAppResource
        mTime: float
        loadTime: float
        eventLoop = asyncio.get_event_loop()

        def getMTime(fileName: str) -> float:
            return os.path.getmtime(fileName)

        def reloadModule(module):
            importlib.reload(module)

        def importModule(moduleId: str) -> types.ModuleType:
            return importlib.import_module(moduleId)

        appId = dRequest[RKEY_APPID]
        configId = dRequest[RKEY_CONFIGID]

        appResource = await self.activateResource(appId, configId)
        moduleId, moduleFileName = BasicAppServerEngine.getModuleIdAndFile(dRequest, '.py')
        moduleFileName = appResource.resolveFilePath(moduleFileName)
        pyModuleId = appResource.resolveModulePath(moduleId)
        
        if (appId, moduleId) in self.cachedDataModules:
            mTime = await eventLoop.run_in_executor(None, getMTime, moduleFileName)
            loadTime, module, instance = self.cachedDataModules[(appId, moduleId)]
            if loadTime < mTime:
                await eventLoop.run_in_executor(None, reloadModule, module)
                instance = module.AppModule(moduleId, moduleFileName, appResource)
                self.cachedDataModules[(appId, moduleId)] = (time.time(), module, instance)
        else:
            module = await eventLoop.run_in_executor(None, importModule, pyModuleId)
            instance = module.AppModule(moduleId, moduleFileName, appResource)
            self.cachedDataModules[(appId, moduleId)] = (time.time(), module, instance)

        return await instance.handleRequest(dRequest, rawRequest)

