JSMENU_TEMPLATE = '''
'use strict';

(function () { 

  async function getImports (React, globals) {
    const { appAction, frameAction, jsdset, _moduleId, _resourceId } = globals
    if (!appAction || !frameAction || !jsdset || !_moduleId || !_resourceId) {
      throw new Error('One of required components (appAction, jsdset, _moduleId, _resourceId) not found in globals')
    }

    const menuModuleId = _moduleId
    const menuId = _resourceId
    const getMenuItem = (key) => ({ menuModuleId, menuId, key })
    
    const MENU_STRUCTURE = %(MENU_STRUCTURE)s
    const HANDLER_TABLE = {
      %(HANDLER_TABLE)s
    }

    function componentFactory (params) {
      const component = { menuStructure: MENU_STRUCTURE, menuHandlers: HANDLER_TABLE }
      return component
    }

    return { componentFactory }
  }

  async function initModuleF(aReact, globals) {
    return await getImports(aReact, globals)
  }

  return initModuleF
})()

'''