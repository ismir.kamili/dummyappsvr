import os
import sys
import time
import hashlib
import os.path
from typing import *

import asyncio
from aiopg.sa import create_engine

from is_lib import odlloader
from appserver.abc import *

DBDT_CHAR = 18
DBDT_VARCHAR = 1043
DBDT_NUMERIC = 1700
DBDT_FLOAT4 = 700
DBDT_FLOAT8 = 701
DBDT_TIMESTAMP = 1114
DBDT_TIMESTAMP_TZ = 1184
DBDT_INT4 = 23
DBDT_INT8 = 20

CLIENT_DT_MAPPING = {
    DBDT_CHAR: 'string',
    DBDT_VARCHAR: 'string',
    DBDT_NUMERIC: 'float',
    DBDT_FLOAT4: 'float',
    DBDT_FLOAT8: 'float',
    DBDT_INT4: 'int',
    DBDT_INT8: 'int',
    DBDT_TIMESTAMP: 'date',
}

def getClientDataType(column):
    ct = column.type_code
    if ct == DBDT_NUMERIC and column.precision == 0:
        return 'int'
    else:
        return CLIENT_DT_MAPPING[column.type_code]
    #--
    pass
#-- def getClientDataType

class DBTxContext:
    def __init__(self, dbEngine):
        self.dbEngine = dbEngine
        self.connection = None
        self.transaction = None
        pass

    async def __aenter__(self):
        return await self.begin()

    async def __aexit__(self, exc_type, exc_value, exc_traceback):
        try:
            if exc_type == None:
                await self.transaction.commit()
            else:
                await self.transaction.rollback()
        finally:
            self.transaction = None
            self.dbEngine.release(self.connection)
            self.connection = None
        pass

    # manual method for resource control: start, commit and rollback
    async def begin(self):
        self.connection = await self.dbEngine._acquire()
        try:
            self.transaction = await self.connection._begin(None, False, False)
        except:
            self.dbEngine.release(self.connection)
            self.connection = None
            raise
        return self.connection

    async def commit(self):
        try:
            await self.transaction.commit()
        finally:
            self.transaction = None
            try:
                self.dbEngine.release(self.connection)
            finally:
                self.connection = None

    async def rollback(self):
        try:
            await self.transaction.rollback()
        finally:
            self.transaction = None
            try:
                self.dbEngine.release(self.connection)
            finally:
                self.connection = None

class ColumnInfo:
    name: str
    ctype: str
    datalen: int
    dbtype: int

class AppResource(IAppResource):
    appServerEngine: IAppServerEngine
    def __init__(self, appId: str, configId: str, appServerEngine: IAppServerEngine, appConfig: dict):
        self.appServerEngine = appServerEngine
        self.dbEngine = None
        self.dataModel = None
        self.appConfig = appConfig
        self.appId = appId
        self.configId = configId

    async def dbEngineConnect(self) -> Any: # to be overriden by subclasses
        cfg = self.appConfig
        dbName = cfg.get('db_name', 'database')
        dbHost = cfg.get('db_host', 'localhost')
        dbPort = int(cfg.get('db_port', 5432))
        self.dbEngine = await create_engine(user=cfg.get('db_user', ''), password=cfg.get('db_password', ''), host=dbHost, port=dbPort, database=dbName)
        print(f'-- app: {self.appId} config: {self.configId} - Connected to database {dbName} at {dbHost}:{dbPort} ======')

    def dbTransact(self) -> Any: # return context manager of aggregated (connection and transaction)
        return DBTxContext(self.dbEngine)

    async def dbQueryConn(self, db_conn: Any, sql: str, params=(), maxRowCount: int=None, withColumnInfo: bool=False) -> Any:
        data = []
        columnsIdx = {}
        columns = []
        needConversionColumns = [] #decimal and date fields

        iRow = 0
        moreData = False
        qWrapper = await db_conn.execute(sql, params)
        qDesc = qWrapper.cursor.description
        row = await qWrapper.fetchone()
        while row:
            iRow += 1
            if (maxRowCount != None and iRow > maxRowCount):
                moreData = True
                break
            data.append(dict(row))
            row = await qWrapper.fetchone()
        if moreData:
            data.append(None) # mark for more data sign

        if withColumnInfo:
            for qColumn in qDesc:
                tc = qColumn.type_code
                cInfo = ColumnInfo()
                cInfo.name = qColumn.name
                cInfo.ctype = getClientDataType(qColumn)
                cInfo.datalen = qColumn.internal_size
                cInfo.dbtype = tc
                columns.append(cInfo)
                columnsIdx[cInfo.name] = cInfo
                if tc == DBDT_TIMESTAMP or tc == DBDT_TIMESTAMP_TZ or tc == DBDT_NUMERIC:
                    needConversionColumns.append(cInfo.name)
            #-- for
            pass
        #-- if
        return data if not withColumnInfo else (data, (columns, columnsIdx, needConversionColumns))

    async def dbQuery(self, sql: str, params=(), maxRowCount: int=None, withColumnInfo: bool=False) -> Any:
        async with self.dbEngine.acquire() as db_conn:
            return self.dbQueryConn(db_conn, sql, params, maxRowCount, withColumnInfo)

    async def dbExecConn(self, db_conn: Any, sql, params = ()) -> Any:
        return await db_conn.execute(sql, params)

    async def dbExec(self, sql: str, params=()) -> Any:
        async with self.dbEngine.acquire() as db_conn:
            return await db_conn.execute(sql, params)

    async def dbGetSequence(self, sequenceName: str) -> int:
        r = await self.dbQuery('''SELECT nextval(%s) as sval''', (sequenceName, ))
        return r[0]['sval']

    async def activate(self) -> None:
        def loadPMD(fileName: str) -> Any:
            return odlloader.loadPMD(fileName)

        if self.dbEngine == None:
            await self.dbEngineConnect()
        if self.dataModel == None and ('data_model' in self.appConfig):
            mdFileName: str = self.resolveFilePath(str(self.appConfig['data_model']))
            self.dataModel = await asyncio.get_event_loop().run_in_executor(None, loadPMD, mdFileName)

    def resolveFilePath(self, filePath: str) -> str:

        resolvedPath = os.path.join(self.appServerEngine.rootAppPath, self.appId, filePath)
        if os.path.abspath(resolvedPath).find(self.appServerEngine.absRootAppPath) != 0:
            raise WebServerResponseError(405, 'Security violation of module_name parameter. Access outside home folder is denied')
        return resolvedPath

    def resolveModulePath(self, modulePath: str) -> str:
        return self.appServerEngine.rootModulePath + self.appId + '.' + modulePath

    def getCacheDir(self) -> str:
        return os.path.join(self.appServerEngine.rootAppPath, self.appId, '__cache__', '')

    async def getFileHash(self, filePath: str) -> str:
        def _getFileHash():
            resolvedPath = os.path.join(self.appServerEngine.rootAppPath, self.appId, filePath)
            baseStr = filePath + time.asctime(time.localtime(os.path.getmtime(resolvedPath)))
            m = hashlib.sha256(); m.update(baseStr.encode(encoding='utf-8'))
            return m.hexdigest().upper()

        return asyncio.get_event_loop().run_in_executor(None, _getFileHash)

class AppResourceFactory(IResourceFactory):
    def createAppResource(appId: str, configId: str, appServerEngine: IAppServerEngine, appConfig: dict) -> IAppResource:
        return AppResource(appId, configId, appServerEngine, appConfig)
