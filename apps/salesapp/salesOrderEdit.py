# server-side python module
#
import sys
import os
import time
from appserver.abc import *
from appserver.stdModule import StdAppModule
from appserver.stdModule import timeLiteral
from copy import copy

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.allowAccessFrom = [
            'menu/salesOrderList/popup_menu/mnuEditSalesOrder'
        ]
        self.resources = {
            'dataSO': {
                'type': 'single_data', 
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'query': '''
                        select s.id_sales_order, s.ref_no, s.customer_no, s.order_date, c.customer_name, s.description
                        from sales_order s join customer c on s.customer_no = c.customer_no
                        where ref_no = %(ref_no)s ''', 
                    'params': {'ref_no': dRequest.get('ref_no')},
                    'field_map': {},
                    'subquery_fields': {
                        'details': {
                            'query': ''' 
                                select 
                                d.*, p.product_name from so_detail d join sales_product p on d.product_code = p.product_code
                                where id_sales_order = %(id)s order by order_no
                            ''',
                            'params': {},
                            'link': {'id': 'id_sales_order'},
                            'field_map': {}
                        }
                    },
                }
            },
            'listCustomer': {
                'type': 'scroll_query',
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'selectFromQueryText': '''
                        select * from customer
                    ''',
                    'sortFields': ('customer_no', 'customer_name', ),
                    'keyField': 'customer_no',
                    'fieldMap': {},
                    'andWherePart': 'customer_no LIKE %(searchKey)s' if dRequest.get('searchMode') and dRequest.get('searchKey') else None,
                    'moreParams': {'searchKey': dRequest.get('searchKey') + '%'} if (dRequest.get('searchMode') and dRequest.get('searchKey')) else {},
                    'addOrderByPart': None,
                    'columnTitles': {},
                }                
            },
            'listProduct': {
                'type': 'scroll_query',
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'selectFromQueryText': '''
                        SELECT product_code, product_name, unit_price
                        FROM sales_product
                    ''',
                    'sortFields': ('product_code', 'product_name', ),
                    'keyField': 'product_code',
                    'fieldMap': {},
                    'andWherePart': 'product_code LIKE %(searchKey)s' if dRequest.get('searchMode') and dRequest.get('searchKey') else None,
                    'moreParams': {'searchKey': dRequest.get('searchKey') + '%'} if (dRequest.get('searchMode') and dRequest.get('searchKey')) else {},
                    'addOrderByPart': None,
                    'columnTitles': {},
                }
            },
            'getCustomData': {
                'type': 'raw_data',
                'handler': self.getCustomData
            },

            'saveData': {
                'type': 'method',
                'handler': self.saveData
            }
        }
        pass

    async def getCustomData(self, moduleId, dataId, dRequest, rawRequest):
        return {'test_result': 'OK'}

    async def saveData(self, moduleId, dataId, dRequest, rawRequest):
        appResource = self.appResource
        updateInfo = copy(dRequest['data'])
        if len(updateInfo) == 0:
            return {'status': '010', 'err_info': 'No data is sent'}
        data = updateInfo[0]

        data['entry_time'] = timeLiteral(time.localtime())
        async with appResource.dbTransact() as conn:
            await appResource.dbExecConn(conn, 
                '''
                    update sales_order 
                    set 
                        customer_no = %(customer_no)s, 
                        order_date=%(order_date)s, 
                        description = %(description)s,
                        entry_time = %(entry_time)s
                    where
                        ref_no = %(ref_no)s

                ''', data )
            
            detailRows = data['details']
            if detailRows:
                for i in range(len(detailRows)):
                    detailRow = detailRows[i]
                    loadFlag = detailRow.get('__loadFlag', 'L')
                    deleteFlag = detailRow.get('__deleted', False)
                    if loadFlag == 'U' and not deleteFlag: # handle updated rows
                        pass
                    elif loadFlag == 'N': # handle new inserted row
                        pass
                    elif deleteFlag:
                        pass

                    # if detailRow
                    
            pass
        return {'status': '000', 'err_info': ''}
        pass

