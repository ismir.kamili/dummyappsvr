
'use strict';

(function () { 

  async function getImports (React, globals) {
    const { appAction, frameAction, jsdset, _moduleId, _resourceId } = globals
    if (!appAction || !frameAction || !jsdset || !_moduleId || !_resourceId) {
      throw new Error('One of required components (appAction, jsdset, _moduleId, _resourceId) not found in globals')
    }

    const menuModuleId = _moduleId
    const menuId = _resourceId
    const getMenuItem = (key) => ({ menuModuleId, menuId, key })
    
    const MENU_STRUCTURE = [{"title": "Application", "items": [{"title": "Logout", "key": "mnuLogout"}]}, {"title": "UI demonstration", "items": [{"title": "Show message", "key": "mnuShowMessage"}, {"title": "Show modal", "key": "mnuShowModal"}, {"title": "Show modal data selector", "key": "mnuModalDataBrowse"}, {"title": "Load another app form", "key": "mnuSampleEmpty"}, {"title": "Increment global counter (salesCounter)", "key": "mnuTestCounter"}]}, {"title": "-"}, {"title": "Business processes", "items": [{"title": "Sales order list", "globalItem": "salesCounter", "key": "mnuSalesOrderList"}]}]
    const HANDLER_TABLE = {
      mnuLogout: async () => {
  console.log('Logout invoked')
  await appAction.logout()
}

 ,
mnuShowMessage: async (appAction, key, e, p) => {
  await appAction.frameAction.showMessage('*** Welcome to my new application ***')
  await appAction.frameAction.showMessage('Feature completed')
}

 ,
mnuShowModal: async (appAction, key, e, p) => {
  var result = await appAction.frameAction.showModalAsync({
    contentClass: globals.ModalContent,
    size: 'tiny',
    dimmer: true,
    closeIcon: true,
    clickOverlayClose: true
  })

  console.log(`Modal returns ${result}`)
}

 ,
mnuModalDataBrowse: async (appAction, key, e, p) => {
  var result = await appAction.browseData(
    { moduleId: 'main', dataId: 'q_account' },
    [], ''
    ,['account_no', 'account_name', 'balance', 'equiv_balance']
    , {}, {width: '540px', height: 'fit'}
  )
  if (!result) {  
    console.log('browse canceled')
  }
  else {
    console.log(`Account ${result.account_no} - ${result.account_name} selected`)
  }    
}

 ,
mnuSalesOrderList: async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('salesOrderList', 'Sales orders', getMenuItem(key))
}

 ,
mnuTestCounter: (appAction, key, e, p) => {
  appAction.incGlobal('salesCounter', 1)
}

 ,
mnuSampleEmpty: async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('sampleEmpty', 'Simple form #1', getMenuItem(key))
} 
    }

    function componentFactory (params) {
      const component = { menuStructure: MENU_STRUCTURE, menuHandlers: HANDLER_TABLE }
      return component
    }

    return { componentFactory }
  }

  async function initModuleF(aReact, globals) {
    return await getImports(aReact, globals)
  }

  return initModuleF
})()

