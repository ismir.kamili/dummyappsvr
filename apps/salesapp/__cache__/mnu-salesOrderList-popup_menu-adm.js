
'use strict';

(function () { 

  async function getImports (React, globals) {
    const { appAction, frameAction, jsdset, _moduleId, _resourceId } = globals
    if (!appAction || !frameAction || !jsdset || !_moduleId || !_resourceId) {
      throw new Error('One of required components (appAction, jsdset, _moduleId, _resourceId) not found in globals')
    }

    const menuModuleId = _moduleId
    const menuId = _resourceId
    const getMenuItem = (key) => ({ menuModuleId, menuId, key })
    
    const MENU_STRUCTURE = [{"title": "View", "key": "mnuViewSalesOrder"}, {"title": "Edit", "key": "mnuEditSalesOrder"}]
    const HANDLER_TABLE = {
      mnuViewSalesOrder: async (appAction, key, e, p) => {
  const row = p.currentRow
  if (row) {
    console.log(`View sales order ${row.ref_no} of ${row.customer_no}`)
    const { _authToken, componentClass } = await appAction.fetchFrameComponentWithToken('salesOrderView', {ref_no: row.ref_no}, {}, getMenuItem(key))
    if (componentClass) {
      await appAction.frameAction.showModalAsync(
        {
          contentClass: componentClass, 
          contentProps: { _authToken },
          title: 'Sales order - view', size: 'small', 
          closeIcon: true,
        }
      )
    }
  }
  else
    console.warn('No menu context is defined')  
}
 ,
mnuEditSalesOrder: async (appAction, key, e, p) => {
  const row = p.currentRow
  if (row) {
    console.log(`Edit sales order ${row.ref_no} of ${row.customer_no}`)
    const { _authToken, componentClass } = await appAction.fetchFrameComponentWithToken('salesOrderEdit', {ref_no: row.ref_no}, {}, getMenuItem(key))

    if (componentClass) {
      await appAction.frameAction.showModalAsync(
        {
          contentClass: componentClass, 
          contentProps: { _authToken },
          title: 'Sales order - edit', size: 'small', 
          closeIcon: true
        }
      )
    }
  }
  else
    console.warn('No menu context is defined')
}
 
    }

    function componentFactory (params) {
      const component = { menuStructure: MENU_STRUCTURE, menuHandlers: HANDLER_TABLE }
      return component
    }

    return { componentFactory }
  }

  async function initModuleF(aReact, globals) {
    return await getImports(aReact, globals)
  }

  return initModuleF
})()

