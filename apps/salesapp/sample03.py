# server-side python module
#
import sys
import os
from appserver.abc import *
from appserver.stdModule import StdAppModule

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.allowAccessFrom = [
            'menu/main/mainMenu/mnuSample03'
        ]
        self.resources = {
        }
        pass
    #-- def


