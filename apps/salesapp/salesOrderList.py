# server-side python module
#
import sys
import os
from appserver.abc import *
from appserver.stdModule import StdAppModule

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.allowAccessFrom = [
            'menu/main/mainMenu/mnuSalesOrderList'
        ]
        self.resources = {
            'listSO': {
                'type': 'scroll_query', 
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'selectFromQueryText': '''
                        SELECT s.ref_no, s.customer_no, s.order_date, c.customer_name, s.description
                        FROM sales_order s join customer c on (s.customer_no = c.customer_no)
                    ''',
                    'sortFields': ('ref_no', 'order_date', 'customer_no', ),
                    'keyField': 'ref_no',
                    'fieldMap': {},
                    'andWherePart': None,
                    'moreParams': {},
                    'addOrderByPart': None,
                    'columnTitles': {
                        'ref_no': 'no referensi', 'customer_no': 'no customer', 'order_date': 'tanggal order',
                        'customer_name': 'nama customer', 'description': 'keterangan'
                    }
                }
            },
            'popup_menu': {
                'type': 'menu',
                'structure_file': 'salesOrderList_popup_structure.json',
                'script_file': 'salesOrderList_popup_handler.js'
            }
        }
        pass


