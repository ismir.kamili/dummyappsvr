# server-side python module
#
import sys
import os
from appserver.abc import *
from appserver.stdModule import StdAppModule

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.allowAccessFrom = [
            'menu/salesOrderList/popup_menu/mnuViewSalesOrder'
        ]
        self.resources = {
            'dataSO': {
                'type': 'single_data', 
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'query': '''
                        select s.id_sales_order, s.ref_no, s.customer_no, s.order_date, c.customer_name, s.description
                        from sales_order s join customer c on s.customer_no = c.customer_no
                        where ref_no = %(ref_no)s ''', 
                    'params': {'ref_no': dRequest.get('ref_no')},
                    'field_map': {},
                    'subquery_fields': {
                        'details': {
                            'query': ''' 
                                select 
                                d.*, p.product_name from so_detail d join sales_product p on d.product_code = p.product_code
                                where id_sales_order = %(id)s order by order_no
                            ''',
                            'params': {},
                            'link': {'id': 'id_sales_order'},
                            'field_map': {}
                        }
                    },
                }
            },
        }
        pass



