# server-side python module
#
import sys
import os
from appserver.abc import *
from appserver.stdModule import StdAppModule

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.allowAccessFrom = [
            'menu/main/mainMenu/mnuSample02'
        ]
        self.resources = {
            'getNames': {
                'type': 'raw_data',
                'handler': self.handleGetNames,
            }
        }
        pass
    #-- def

    async def handleGetNames(self, moduleId, dataId, dRequest, rawRequest):
        return {'data': ['AHMAD', 'BUDI', 'CICI']}
        pass


