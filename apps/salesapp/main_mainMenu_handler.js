//---MENU HANDLER
//id: mnuLogout
async () => {
  console.log('Logout invoked')
  await appAction.logout()
}

//---MENU HANDLER
//id: mnuShowMessage
async (appAction, key, e, p) => {
  await appAction.frameAction.showMessage('*** Welcome to my new application ***')
  await appAction.frameAction.showMessage('Feature completed')
}

//---MENU HANDLER
//id: mnuShowPopup
(appAction, key, e, p) => {
  appAction.frameAction.showPopUp(
    p.frameInstanceName, 'pop1', globals.PopUpExample, {}, null,
    {left: '100px', top: '100px', height: '200px', fitWidth: true}, undefined, 
    {onClose: (popupName, result) => console.log(`Popup ${popupName} closed. result=${result}`)}
  )
}

//---MENU HANDLER
//id: mnuShowModal
async (appAction, key, e, p) => {
  var result = await appAction.frameAction.showModalAsync({
    contentClass: globals.ModalContent,
    size: 'tiny',
    dimmer: true,
    closeIcon: true,
    clickOverlayClose: true
  })

  console.log(`Modal returns ${result}`)
}

//---MENU HANDLER
//id: mnuModalDataBrowse
async (appAction, key, e, p) => {
  var result = await appAction.browseData(
    { moduleId: 'main', dataId: 'q_account' },
    [], ''
    ,['account_no', 'account_name', 'balance', 'equiv_balance']
    , {}, {width: '540px', height: 'fit'}
  )
  if (!result) {  
    console.log('browse canceled')
  }
  else {
    console.log(`Account ${result.account_no} - ${result.account_name} selected`)
  }    
}

//---MENU HANDLER
//id: mnuSalesOrderList
async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('salesOrderList', 'Sales orders', getMenuItem(key))
}

//---MENU HANDLER
//id: mnuTestCounter
(appAction, key, e, p) => {
  appAction.incGlobal('salesCounter', 1)
}

//---MENU HANDLER
//id: mnuSample01
async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('sample01', 'Sample 01', getMenuItem(key))
}

//---MENU HANDLER
//id: mnuSample02
async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('sample02', 'Sample 02', getMenuItem(key))
}

//---MENU HANDLER
//id: mnuSample03
async (appAction, key, e, p) => {
  await appAction.fetchTokenizedFrame('sample03', 'Sample 03', getMenuItem(key))
}