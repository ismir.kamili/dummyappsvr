// menuModuleId and menuId are globally available identifiers
// correspond to module_id and data_id of this menu, respectively

//---MENU HANDLER
//id: mnuViewSalesOrder
async (appAction, key, e, p) => {
  const row = p.currentRow
  if (row) {
    console.log(`View sales order ${row.ref_no} of ${row.customer_no}`)
    const { _authToken, componentClass } = await appAction.fetchFrameComponentWithToken('salesOrderView', {ref_no: row.ref_no}, {}, getMenuItem(key))
    if (componentClass) {
      await appAction.frameAction.showModalAsync(
        {
          contentClass: componentClass, 
          contentProps: { _authToken },
          title: 'Sales order - view', size: 'small', 
          closeIcon: true,
        }
      )
    }
  }
  else
    console.warn('No menu context is defined')  
}
//---MENU HANDLER
//id: mnuEditSalesOrder
async (appAction, key, e, p) => {
  const row = p.currentRow
  if (row) {
    console.log(`Edit sales order ${row.ref_no} of ${row.customer_no}`)
    const { _authToken, componentClass } = await appAction.fetchFrameComponentWithToken('salesOrderEdit', {ref_no: row.ref_no}, {}, getMenuItem(key))

    if (componentClass) {
      await appAction.frameAction.showModalAsync(
        {
          contentClass: componentClass, 
          contentProps: { _authToken },
          title: 'Sales order - edit', size: 'small', 
          closeIcon: true
        }
      )
    }
  }
  else
    console.warn('No menu context is defined')
}
