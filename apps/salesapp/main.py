# server-side python module
#
import sys
import os
from appserver.abc import *
from appserver.stdModule import StdAppModule

class AppModule(StdAppModule):
    def __init__(self, resourceId: str, moduleFileName: str, appResource: IAppResource):
        super().__init__(resourceId, moduleFileName, appResource)
        self.bypassAccessCheck = True
        self.allowAccessFrom = [] # ignored since bypassAccessCheck is set
        self.resources = {
            'mainMenu': {
                'type': 'menu', 
                'structure_file': 'main_mainMenu_structure.json',
                'script_file': 'main_mainMenu_handler.js'
            },
            'q_account': {
                'type': 'scroll_query',
                'handler': lambda self, resourceId, dataId, dRequest: {
                    'selectFromQueryText': '''
                        SELECT account_no, account_name, balance, equiv_balance
                        FROM account
                    ''',
                    'sortFields': ('account_no', 'account_name', 'balance', ),
                    'keyField': 'account_no',
                    'fieldMap': {},
                    'andWherePart': None,
                    'moreParams': {},
                    'addOrderByPart': None,
                    'columnTitles': {
                        'account_no': 'no account', 'account_name': 'nama akun', 'balance': 'saldo',
                    }
                }
            }
        }
        pass
    #-- def


