import os
import sys
from aiopg.sa import create_engine
import asyncio

DB_USER = 'appdb'
DB_PASSWORD = 'solusi'
DB_HOST = 'localhost'
DB_DB = 'appdb'

async def queryDB():
    async with create_engine(user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_DB) as dbEngine:
        async with dbEngine.acquire() as conn:
            q = await conn.execute('select * from account')
            try:
                print('query completed, description: ')
                print(q.cursor.description)
                row = await q.fetchone()
                while row:
                    print('%10s %30s' % (row['account_no'], row['account_name']))
                    row = await q.fetchone()
            finally:
                q.close()
                print('query closed')


async def transactDB():
    async with create_engine(user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_DB) as dbEngine:
        async with dbEngine.acquire() as conn:
            async with conn.begin() as tr:
                await conn.execute('INSERT INTO sales_product VALUES (%s, %s, %s)', ('TEST', 'TEST PRODUCT', 10000))
                # tr.commit() # try implict commit

            print('transaction was implicitly committed')

async def getTableDesc(tableName):
    async with create_engine(user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_DB) as dbEngine:
        async with dbEngine.acquire() as conn:
            q = await conn.execute('select * from %s' % tableName)
            try:
                print('description: ')
                for c in q.cursor.description:
                    print('%-20s %5s %5s %5s %5s' % (c.name, c.type_code, c.internal_size, c.precision, c.scale))
            finally:
                q.close()

