import os
import sys
import json
from typing import *
import asyncio
from aiohttp import web
import functools

from appserver.abc import *
from appserver.basicEngine import BasicAppServerEngine
from appserver.pgResource import AppResource, AppResourceFactory

APPSERVER_ENGINES = {'basic_engine': BasicAppServerEngine}
APPSERVER_RESOURCES = {'postgres': AppResourceFactory}

INVALID_SESSION_STATUS = 481
MODULES_LIST = {}
DEFAULT_CONFIG_FILE = 'config.json'

DEFAULT_SESSION_TIMEOUT = 1800 # default session time out for 30 minutes
DEFAULT_APP_ID = 'default'
DEFAULT_CONFIG_ID = 'defaultapp'
CKEY_ROOT_APP_PATH = 'root_app_path'

def getRootConfig() -> dict:
    with open(DEFAULT_CONFIG_FILE, 'rb') as cf:
        sConfig = cf.read().decode(encoding = 'utf-8')    
    return json.loads(sConfig)

class WebAppServer(IWebAppServer):
    # type hints for members
    engine: IAppServerEngine
    
    def __init__(self):
        #engineClass: similar to BasicAppServerEngine class variable
        self.handlerPath = {'': self.defaultHandler}
        self.setupPath()
        self.config = getRootConfig()
        
        engineName = self.config.get('engine', 'basic_engine')
        engineClass = APPSERVER_ENGINES.get(engineName, None)
        if engineClass == None:
            raise Exception(f'Unknown appserver engine name "{engineName}"')
        self.engine = engineClass(self, self.config, APPSERVER_RESOURCES)

    def loadModules(self):
        for mName in MODULES_LIST:
            moduleDef = MODULES_LIST[mName]
            sMountPath = moduleDef.get('mountPath') or ''
            mountPaths = sMountPath.split('/')
            mountPaths = mountPaths[:-1] if len(mountPaths) > 0 and mountPaths[-1] == '' else mountPaths
            cHandler = self.handlerPath
            for path in mountPaths:
                cHandler = cHandler.get(path) if type(cHandler) is dict else None
                if cHandler == None or type(cHandler) is not dict:
                    raise Exception(f'Cannot resolve mount path "{sMountPath}" in module ${mName}')
            moduleFile = moduleDef.get('module')
            try:
                moduleModule = __import__(moduleFile)
                handlerClass = moduleModule.HandlerClass
                handlerInst = handlerClass(self)
            except Exception as e:
                raise Exception(f'Error loading module ${mName} in file ${moduleFile}\nDetails: ${str(e)}')
            exportedHandlers = handlerInst.getExportedHandlers()
            for hName in exportedHandlers:
                cHandler[hName] = exportedHandlers[hName]

    def setupPath(self): # to be overriden by subclasses
        self.handlerPath = {
            '': self.defaultHandler,
            'api': {
                '': self.defaultAPIHandler,
                'app': {
                    'login': {'handler': self.handleLogin, 'methods': 'POST', 'source': 'json'},
                    'access': {'handler': self.handleAccess, 'methods': 'GET'},
                    'logout': {'handler': self.handleLogout, 'methods': 'POST', 'source': 'json'},
                    'getmodule': {'handler': self.handleGetModule, 'methods': 'GET', 'source': 'query'},
                    'module': {'handler': self.handleGetModule, 'methods': 'GET', 'source': 'query'},
                    'getresource': {'handler': self.handleGetResource, 'methods': ('GET', 'POST'), 'source': 'jsonquery'},
                    'resource': {'handler': self.handleGetResource, 'methods': ('GET', 'POST'), 'source': 'jsonquery', 'pathParameter': True},
                    'postdata': {'handler': self.handlePostData, 'methods': 'POST', 'source': 'jsonquery'},
                    'post': {'handler': self.handlePostData, 'methods': 'POST', 'source': 'jsonquery', 'pathParameter': True},
                },
                'data': {
                    'query': {
                    },
                    'update': {
                    }
                }
            },
        }
        self.loadModules()

    async def defaultHandler(self, request):
        return web.Response(
            text = '''<html>
                    <body>
                        Welcome to our API host !!!
                        use /api/xxx request to access available APIs
                    </body>
                </html>''', 
            status = 200, reason = 'OK', content_type = 'text/html')

    async def defaultAPIHandler(self, request):
        return web.Response(
            text = json.dumps({'status': '000', 'description': 'API server ping OK'}), 
            status = 200, reason = 'OK', content_type = 'application/json'
        )

    async def parseRequest(self, request: web.Request, dRequest, method: Union[str, tuple]='POST', paramSource='json') -> None:
        if ((isinstance(method, tuple) or isinstance(method, list)) and request.method not in method) or (isinstance(method, str) and request.method != method):
            return web.Response(status = '401', reason = 'Invalid method for this request path (%s required)' % method)
        if paramSource == 'json' or paramSource == 'jsonquery':
            if request.content_length != None:
                bytes = b''
                while len(bytes) < request.content_length:
                    bytes += await request.content.read(1024)
                requestStr = bytes.decode(encoding = 'utf-8', errors = 'ignore')
                if requestStr:
                    try:
                        dRequestData = json.loads(requestStr)
                        print('API request: ')
                        print(dRequestData)
                        print('--------------------------------')
                    except Exception as e:
                        print('API JSON parse error. Raw message : ', requestStr)
                        print('------------------------------------')
                        return web.Response(status=403, reason='Invalid JSON message: ' + str(e))
                    #--
                    pass
                    dRequest.update(dRequestData)
                #--
                pass
            #--
        if paramSource == 'post' or paramSource == 'postquery':
            dRequest.update(await request.post())
        if paramSource == 'query' or paramSource == 'jsonquery' or paramSource == 'postquery':
            dRequest.update(request.query)        
        return None

    async def apiPreprocess(self, request, method: Union[str, tuple]='POST', paramSource='json'):
        dRequest = {}
        valid, errorOrSessionInfo = await self.engine.checkSession(request.cookies.get('session_id', b''))
        if not valid:
            return None, web.Response(status=INVALID_SESSION_STATUS, reason='Session expired')
        else:
            sessionInfo = errorOrSessionInfo
        errResponse = await self.parseRequest(request, dRequest, method, paramSource)
        if errResponse != None:
            return None, errResponse
        dRequest.update(sessionInfo)
        return dRequest, None

    def apiResponse(self, dResult: dict, cookies=None, status: int=200, reason: str='OK', content_type: str='application/json'):
        if status == 200:
            resp = web.Response(
                text = json.dumps(dResult), 
                status = 200, reason = 'OK', content_type = 'application/json'
            )
            if cookies:
                for k in cookies:
                    resp.set_cookie(k, cookies[k])
            resp.headers['Access-Control-Allow-Origin'] = '*'
        else:
            resp = web.Response(status=status, reason=reason)
        return resp

    async def handleLogin(self, request, method='POST', paramSource='json', pathParameter=None):
        dRequest = {}
        errResponse = await self.parseRequest(request, dRequest)
        if errResponse != None:
            return errResponse

        dResponse = await self.engine.login(dRequest)
        if dResponse.get('status') == '000':
            sessionId = dResponse.pop('session_id')
            response = self.apiResponse(dResponse, {'session_id': sessionId})
        else:
            response = self.apiResponse(dResponse)
        return response

    async def handleAccess(self, request, method='POST', paramSource='json', pathParameter=None):
        dRequest = {}
        errResponse = await self.parseRequest(request, dRequest, method, paramSource)
        if errResponse != None:
            return errResponse
        
        valid, error = await self.engine.checkSession(request.cookies.get('session_id', b''))
        return self.apiResponse({'status': '000', 'description': 'access valid'}) if valid else self.apiResponse({'status': '011', 'description': error})
    #--

    async def handleLogout(self, request, method='POST', paramSource='json', pathParameter=None):
        dRequest = {}
        errResponse = await self.parseRequest(request, dRequest, method, paramSource)
        if errResponse != None:
            return errResponse
        
        sessionId = request.cookies.get('session_id')
        if sessionId != None:
            await self.engine.deleteSession(sessionId) 

        response = self.apiResponse({'status': '000', 'description': 'logout from system'})
        response.del_cookie('session_id')
        return response
    #--

    async def handleGetModule(self, request, method='GET', paramSource='json', pathParameter=None):
        dRequest, errResponse = await self.apiPreprocess(request, method, paramSource)
        if errResponse != None:
            return errResponse
        moduleCode = await self.engine.getModule(dRequest, request)
        response = web.Response(status=200, body=moduleCode.encode(encoding='utf-8'), content_type='application/javascript')
        return response
    #-- handleGetModule
    
    def _processResponse(self, data, contentType):
        if isinstance(data, dict):
            dResponse = {'status': '000', 'description': 'success'}
            dResponse.update(data)
            response = self.apiResponse(dResponse, content_type=contentType)
        elif isinstance(data, str):
            response = web.Response(body=data, content_type=contentType)
        else:
            raise WebServerResponseError(504, 'Invalid result data type from engine.getResource()')
        return response
        
    async def handleGetResource(self, request, method='GET', paramSource='jsonquery', pathParameter=None):
        dRequest, errResponse = await self.apiPreprocess(request, method, paramSource)
        if errResponse != None:
            return errResponse
        if 'module_id' not in dRequest and 'module_name' not in dRequest and pathParameter:
            dRequest['module_id'] = pathParameter.replace('/', MODULE_PATH_SEPARATOR)

        data, contentType = await self.engine.getResource(dRequest, request)
        return self._processResponse(data, contentType)

    async def handlePostData(self, request, method='POST', paramSource='jsonquery', pathParameter=None):
        dRequest, errResponse = await self.apiPreprocess(request, method, paramSource)
        if errResponse != None:
            return errResponse
        if 'module_id' not in dRequest and 'module_name' not in dRequest and pathParameter:
            dRequest['module_id'] = pathParameter.replace('/', MODULE_PATH_SEPARATOR)
        dRequest['type'] = 'method'
        if 'method_id' in dRequest:
            dRequest['data_id'] = dRequest['method_id']
        data, contentType = await self.engine.getResource(dRequest, request)
        if isinstance(data, dict):
            dResponse = {'status': '000', 'description': 'success'}
            dResponse.update(data)
            response = self.apiResponse(dResponse, content_type=contentType)
        elif isinstance(data, str):
            response = web.Response(body=data, content_type=contentType)
        else:
            raise WebServerResponseError(504, 'Invalid result data type from engine.getResource()')
        return response

    def getHandler(self):
        async def handler(request):
            if request.method == 'OPTIONS': # preflight request for CORS
                # check request.headers['origin'] - not implemented yet
                print('OPTIONS request')
                print(request.headers)
                print('--------------------------')
                resp = web.Response(status = '204')
                resp.headers['Access-Control-Allow-Origin'] = '*'
                resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
                resp.headers['Access-Control-Allow-Headers'] = 'X-PINGOTHER, Content-Type'
                resp.headers['Connection'] = 'Keep-Alive'
                return resp
            #--

            try:
                path = request.path
                path = path[1:] if path[:1] == '/' else path
                hDict = self.handlerPath
                hData = None
                while path != None:
                    els = path.split('/', 1)
                    hData = hDict.get(els[0])
                    if not hData or callable(hData) or (type(hData) is dict and ('handler' in hData)):
                        path = els[1] if len(els) == 2 else None
                        break
                    path = els[1] if len(els) == 2 else None
                    hDict = hData
                
                if not hData:
                    # response with 404
                    return web.Response(status=404, reason='Invalid path')
                else:
                    if callable(hData):
                        return await hData(request, method='POST', paramSource='json')
                    elif type(hData) is dict:
                        handlerFunction = hData['handler']
                        if not callable(handlerFunction):
                            return web.Response(status=405, reason='Invalid handler')
                        return await handlerFunction(request, method=hData.get('methods', 'GET'), paramSource=hData.get('source', 'json'), pathParameter=path if hData.get('pathParameter', False) else None)
                    else:
                        return web.Response(status=405, reason='Unknown or invalid handler')
            except WebServerResponseError as werr:
                return web.Response(status=werr.errCode, reason=werr.errInfo)
        #--

        return handler

    async def webServer(self):
        server = web.Server(self.getHandler())
        runner = web.ServerRunner(server)
        await runner.setup()
        servicePort: int = self.config.get('service_port', None)
        if servicePort == None:
            raise Exception('Undefined service port')
        site = web.TCPSite(runner, 'localhost', servicePort)
        await site.start()

        print(f'======= Serving on http://localhost:{servicePort}/ ======')

    async def main(self):
        await self.engine.start()
        await self.webServer()
        while True:
            await asyncio.sleep(60)

if __name__ == '__main__':
    app = WebAppServer()
    asyncio.run(app.main())


