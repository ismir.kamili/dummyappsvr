import os
import sys
import httpserver
import asyncio
import aiohttp
from aiohttp import web

CONFIG_FILE = 'simpleserver.json'
app = httpserver.HTTPServer(CONFIG_FILE)

@app.route('/test')
async def testHandler(server: httpserver.HTTPServer, request: web.Request):
    return server.apiResponse({'status': 'OK'})

@app.route('/echo')
async def echoHandler(server: httpserver.HTTPServer, request: web.Request):
    if request.content_type != 'application/json':
      raise Exception('Invalid content')
    dRequest = await request.json()
    return server.apiResponse({'reply': 'ECHO ' + dRequest.get('message', '')})

@app.route('/load_data')
async def loadDataHandler(server: httpserver.HTTPServer, request: web.Request):
    await asyncio.sleep(2)
    return server.apiResponse(
      {
        'data': [
          {'id': '00003', 'name': 'arsyad', 'address': 'pamekasan 26'},
          {'id': '00004', 'name': 'ali', 'address': 'ters jkt 329'},
        ]
      }
    )
    

asyncio.run(app.main())
