import os
import sys
import inspect
import json
import traceback
from typing import *
import asyncio
from aiohttp import web
import functools

DEFAULT_CONFIG_FILE = 'config.simple.json'

def getRootConfig(configFile = DEFAULT_CONFIG_FILE) -> dict:
    with open(configFile, 'rb') as cf:
        sConfig = cf.read().decode(encoding = 'utf-8')    
    return json.loads(sConfig)

class HTTPServer():
    def __init__(self, configFile):
        self.config = getRootConfig(configFile)
        self.paths = {}

    def apiResponse(self, dResult: dict, cookies=None, status: int=200, reason: str='OK', content_type: str='application/json'):
        if status == 200:
            resp = web.Response(
                text = json.dumps(dResult), 
                status = 200, reason = 'OK', content_type = 'application/json'
            )
            if cookies:
                for k in cookies:
                    resp.set_cookie(k, cookies[k])
            resp.headers['Access-Control-Allow-Origin'] = '*'
        else:
            resp = web.Response(status=status, reason=reason)
        return resp

    def route(self, path): # this is a decorator method

        def decorator_route(handlerFunc):            
            @functools.wraps(handlerFunc)
            async def wrapper_handler(server, request):
                if inspect.iscoroutinefunction(handlerFunc):
                    return await handlerFunc(server, request)
                else:
                    return handlerFunc(server, request)
            
            self.paths[path] = wrapper_handler
            return wrapper_handler

        return decorator_route

    def getHandler(self):
        async def handler(request):
            try:
                wrappedHandler = self.paths.get(request.path)
                if not callable(wrappedHandler):
                    return web.Response(status=404, reason=f'Unknown path {request.path}')
                return await wrappedHandler(self, request)
            except:
                traceback.print_exc()
                return web.Response(status=502, reason='Internal server error')
        #--

        return handler

    async def webServer(self):
        server = web.Server(self.getHandler())
        runner = web.ServerRunner(server)
        await runner.setup()
        servicePort: int = self.config.get('service_port', None)
        serviceHost: str = self.config.get('service_host', 'localhost')
        if servicePort == None:
            raise Exception('Undefined service port')
        site = web.TCPSite(runner, serviceHost, servicePort)
        await site.start()

        print(f'======= Serving on http://{serviceHost}:{servicePort}/ ======')

    async def main(self):
        await self.webServer()
        while True:
            await asyncio.sleep(60)

