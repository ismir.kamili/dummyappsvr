'''com.ihsan.stringutils
'''

import time
import random
import string

def pascalQuotedStr(s):
  return "'" + s.replace("'", "''") + "'"  
#--

def QuotedStr(s):
    return "'%s'" % s

def QuoteStr(s):
    return "'%s'" % s

def IncludeTrailingStr(s):
    if s[-1] == '\\':
        return s
    else:
        return s + '\\'

def StringToSObjectText(s):
    s = s.replace('<', '&lt;')
    s = s.replace('>', '&gt;')
    s = s.replace('&QUOT;', '')
    s = s.replace('&', '&amp;')
    return s

def SObjectTextToString(s):
    s = s.replace('&lt;', '<')
    s = s.replace('&gt;', '>')
    s = s.replace('&amp;', '&')
    return s

def GetRandomText(n, type=0):
    # type:
    # 0 - default, only random number
    # 1 - use julian date as prefix
    def Today():
        d = time.localtime()
        return d

    def GetJulianDate():
        julianDate = Today()[7]
        return str(julianDate).zfill(3)

    if not n > 0:
        raise Exception('panjang harus lebih besar dari 0')

    s = ''
    if type == 1:
        if n > 3:
            s = GetJulianDate()
            n -= 3

    mulN = pow(10, n-1)
    s += str(random.randint(1*mulN, 9*mulN))

    return s

def isValidIdentifier(s):
  if len(s) == 0: return False
  cc = ord(s[0].lower())
  if (cc < ord('a') or cc > ord('z')) and cc != ord('_'): return False
  n = len(s); i = 1
  while i < n:
    cc = ord(s[i].lower())
    if (cc < ord('a') or cc > ord('z')) and (cc < ord('0') or cc > ord('9')) and cc != ord('_'): 
      return
    i += 1
  #--
  return True
#--
  
def NominalTerbilang(config,nominal,matauang):
    strNominal = config.FormatFloat('0.00', nominal)
      
    lsNominal = string.split(strNominal,".")
     
    matauang = " " + matauang
    cJmlNominalUtama = terbilang(lsNominal[0]) + matauang 
    cJmlNominalSen = ""
    if int(lsNominal[1]) != 0:
       cJmlNominalSen = " Koma " + terbilang(lsNominal[1]) + " Sen"
    Nominal_Terbilang = cJmlNominalUtama + cJmlNominalSen
    
    return Nominal_Terbilang
    
def terbilang(b):
    def ParsingTiga(b):    
      if b == 0:
        return "Nol"
      bs = str(b)
      pjg = len(bs)
      kalimat = []
      for i in range(pjg):
        c = int(bs[pjg-i-1])
        if c == 0:
          s = ""
        else:
          s = angka[c]
          if i == 1:
            if c == 1:
              sebelum = bs[pjg-1]
              if sebelum == "0":
                s = "Sepuluh "
              else:
                j = len(kalimat)
                if sebelum == "1":
                  s = "Sebelas "
                else:
                  s = kalimat[j-2] + "Belas "
                kalimat[j-2] = ""
            else:
              s = s + "Puluh "
          elif i == 2:
            if c == 1:
              s = "Seratus "
            else:
              s = s + "Ratus "

        kalimat.append(s)
      kalimat.reverse()
      s = ""
      for i in kalimat:
        s = string.strip(s + " " + i)
      return s
    
    blok = ['','Ribu ','Juta ','Milyar ','Trilyun ','Quadriliun ']
    angka = ['','Satu ','Dua ','Tiga ','Empat ','Lima ','Enam ','Tujuh ','Delapan ','Sembilan ']
    bs = str(b)
    pjg = len(bs)
    JmlBlok = pjg / 3
    if pjg % 3 > 0:
      JmlBlok = JmlBlok + 1
    k = []
    for i in range(JmlBlok):
      c = int(bs[-3:])
      if i == 1 and c == 1:
        s = "seribu"
      elif c == 0 and JmlBlok > 1:
        s = ""
      else:
        s = ParsingTiga(c) + " " + blok[i]
      k.append(string.strip(s))
      bs = bs[:-3]
    k.reverse()
    s = ""
    for i in k:
      s = string.strip(s + " " + i)
    return s
  
