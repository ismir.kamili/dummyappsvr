import os
import sys
from .sobjectintf import *
from types import *
from . import timeutil
from .strutil import pascalQuotedStr
import traceback
import time
import array
import weakref

SOBJHELPER_READ = 0
SOBJHELPER_WRITE = 1

PROPERTY_DEFAULT_VALUES = {
    ptSmallint: 0,
    ptInteger: 0,
    ptByte: 0,
    ptSingle: 0.0,
    ptDouble: 0.0,
    ptCurrency: 0.0,
    ptBoolean: False,
    ptString: "",
    ptObject: None,
    ptObjectRef: None
}

READABLE_CHARSET_START = 32
READABLE_CHARSET_END = 127
MAX_BUFFER_SZ = 1024
MAX_ID_LEN = 128
MAX_LINE_LEN = 512
MAX_REAL_EXPON = 38
MAX_REAL_MANTISSA = 1.7
ALPHABET = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '_']
DIGIT = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
WHITESPACE = [' ', '\x09', '\x0A', '\x0D']
APPOSTROPHE = '\x27'
O_APPOSTROPHE = 0x27
QUOTE = '"'

IDENTIFIER_T = 0 
OBJECT_T = 1
NIL_T = 2
ROOT_T = 3
DOCUMENT_T = 4
LIBVERSIONID_T = 5
REFERENCES_T = 6
DATE_T = 7
OPENPAR_T = 8 
CLOSEPAR_T = 9 
COLON_T = 10 
COMMA_T = 11
SEMICOLON_T = 12 
PERIOD_T = 13
EQUAL_T = 14
OPENBRACKET_T = 15  
CLOSEBRACKET_T = 16
OPENCURL_T = 17
CLOSECURL_T = 18
NE_T = 19
LT_T = 20
LE_T = 21
GT_T = 22
GE_T = 23
PLUS_T = 24
MINUS_T = 25
MUL_T = 26
DIV_T = 27
EXCLAMATION_T = 28 
CONCAT_T = 29
DOLLAR_T = 30
PERCENT_T = 31
ACURL_T = 32
STRCONST_T = 33  
INTCONST_T = 34
REALCONST_T = 35  
BOOLCONST_T = 36
EOF_T = 37

TOKEN_NAMES = [
      'identifier', 'object', 'nil', 'root', 'document',
      'libversionid', 'references', 'date',

      '(', ')', ':', ',',';',
      '.', '=', '[', ']', '{',
      '}', '<>', '<', '<=', '>',
      '>=', '+', '-', '*', '/',
      '!', '||', '$', '%', '@',
      '<string constant>', '<integer constant>', '<float constant>', '<boolean>', '<end of file>'
    ]
  
ID_TOKEN_MAPPING = {'object': OBJECT_T, 'nil': NIL_T, 'root': ROOT_T, 'document': DOCUMENT_T,
  'libversionid': LIBVERSIONID_T, 'references': REFERENCES_T, 'date': DATE_T}
  
SYM_TOKEN_MAPPING = {
    '(': OPENPAR_T, ')': CLOSEPAR_T, ':': COLON_T, ',': COMMA_T, ';': SEMICOLON_T,
    '.': PERIOD_T, '=': EQUAL_T, '[': OPENBRACKET_T, ']': CLOSEBRACKET_T, '{': OPENCURL_T,
    '}': CLOSECURL_T, '<>': NE_T, '<': LT_T, '<=': LE_T, '>': GT_T,
    '>=': GE_T, '+': PLUS_T, '-': MINUS_T, '*': MUL_T, '/': DIV_T,
    '!': EXCLAMATION_T, '||': CONCAT_T, '$': DOLLAR_T, '%': PERCENT_T, '@': ACURL_T
  }

  
CHAR_MAP = []
IS_OTHER = 0
IS_WHITESPACE = 1
IS_ALPHABET = 2
IS_DIGIT = 3
IS_STRSTARTER = 4

PR_OK = 0
PR_NOSTARTER = 1


def isAlphabet(occ):
    return  occ >= 97 and occ <= 122 or occ >= 65 and occ <= 90 or occ == 95
    
def isDigit(occ):
    return occ >= 0x30 and occ <= 0x39
  
def isWS(occ):
    return occ == 0x20 or occ == 0x09 or occ == 0x0a or occ == 0x0d
  
def isStrStarter(occ):
    return occ == O_APPOSTROPHE or occ == 0x23 or occ == 0x25
    
def buildCharMap():
    n = 255
    i = 0
    while i  < n:
        occ = i
        if isWS(occ):
            CHAR_MAP.append(IS_WHITESPACE)
        elif isAlphabet(occ):
            CHAR_MAP.append(IS_ALPHABET)
        elif isDigit(occ):
            CHAR_MAP.append(IS_DIGIT)
        elif isStrStarter(occ):
            CHAR_MAP.append(IS_STRSTARTER)
        else:
            CHAR_MAP.append(IS_OTHER)
        i += 1
    #--
    pass
#--

class ScanException(Exception):
    pass
#--

class ParseException(Exception):
    pass
   #--
   
class SObjectException(Exception):
    pass
#--

class sobjscanner:
    
    def __init__(self, dataStream):
        self.dataStream = dataStream;
        self.cc = chr(0)
        self.occ = 0
        self.lineNum = 1
        self.bEOF = False
        self.NextChar()
    #--
    
    def getTokenInfo(self, token_kind, token_val):
        return {'token_kind': token_kind, 'token_value': token_val}
        
    def TestKeyword(self, idtext):
        chk_id = idtext.lower()
        if chk_id in ID_TOKEN_MAPPING:
            return self.getTokenInfo(ID_TOKEN_MAPPING[chk_id], None)
        else:
            if chk_id == 'true':
                return self.getTokenInfo(BOOLCONST_T, True)
            elif idtext == 'false':
                return self.getTokenInfo(BOOLCONST_T, False)
            else:
                return self.getTokenInfo(IDENTIFIER_T, idtext)
            #--
        #--
    #--
      
    def NextChar(self):
        cc = self.dataStream.readChar()
        if cc == None:
            self.bEOF = True
            self.cc = '\x00'
            self.occ = 0x00
        else:
            self.cc = cc
            self.occ = ord(cc)
    #--
    
    def PassWhiteSpaces(self):
        while CHAR_MAP[self.occ] == IS_WHITESPACE and not self.bEOF:
            if self.occ == 0x0a:
                self.lineNum = self.lineNum + 1
            #--
            self.NextChar()
        pass
        
    def AcquireSingleCharConstant(self):
        ch_ord = 0
        dd = 0
        
        self.NextChar()
        if CHAR_MAP[self.occ] != IS_DIGIT or self.bEOF:
            raise ScanException("Digit expected")
        ch_ord = 0
        while True:
            dd = ord(self.cc) - ord('0')
            if ((255 - dd) / 10) < ch_ord:
                raise ScanException("Constant out of range")
            ch_ord = ch_ord * 10 + dd
            self.NextChar()
            if CHAR_MAP[self.occ] != IS_DIGIT or self.bEOF:
                break
        #--
        return chr(ch_ord)
        
    def AcquireCharConstants(self):
        cc = '\x00'
        
        s = ""
        while True:
            cc = self.AcquireSingleCharConstant()
            s += cc
            if self.bEOF or (self.cc != '#'):
                break;
        #--
        return s
    #--
      
    def AcquireQuotedString(self):
        bQuotePassed = False
        sTmp = ""
        self.NextChar()
        while True:
            if self.occ == 0x0d or self.occ == 0x0a or self.bEOF:
                raise ScanException("Unterminated string string")
            #_-
            
            while not self.bEOF and self.occ != O_APPOSTROPHE and self.cc != 0x0d:
                #buff.append(self.cc)
                sTmp += self.cc
                self.NextChar()
            #--
            
            if self.occ == O_APPOSTROPHE:
                self.NextChar()
                if self.occ == O_APPOSTROPHE:
                    #buff.append(self.cc)
                    sTmp += self.cc
                else:
                    bQuotePassed = True
                #--
            elif not self.bEOF and  self.occ != 0x0d:
                #buff.append(self.cc)
                sTmp += self.cc
                self.NextChar()
            #--
            
            if bQuotePassed:
                break
            #--
        #-- while True
        return sTmp #buff.tostring()
    #-- def
    
    def AcquireCEncodedString(self):
        buff = array.array('c')
        
        self.NextChar()
        sLen = ""; iCounter = 0
        while True:
            bTerminated = self.bEOF or not (((self.occ >= 0x30) and (self.occ <= 0x39)) or ((self.occ >= 0x41) and (self.occ <= 0x46)))
            if not bTerminated:
                iCounter += 1
                sLen += self.cc
                self.NextChar()
            #--
            
            if bTerminated or (iCounter == 8):
                break
            #--
        #--
        
        if iCounter != 8:
            raise ScanException("Uncompleted C-encoded string information")
          
        try:  
            iLength = int(sLen, 16)
        except:
            raise ScanException("Invalid string length in C-encoded string information")
        #--
        
        iDataRead = 0
        while not self.bEOF and (iDataRead < iLength):
            buff.append(self.cc)
            self.NextChar()
            iDataRead += 1
        #--
        
        if iDataRead != iLength:
            raise ScanException("String length mismatch")
          
        sEncodedStr = buff.tostring()
        try:
            sDecoded = sEncodedStr.decode("string_escape")
        except:
            raise ScanException("Invalid C-encoded string")
        #--
        
        return sDecoded
    #--
      
    def AcquireString(self):
        s = ""
        while True:
            if self.occ == O_APPOSTROPHE:
                tmp = self.AcquireQuotedString()
            elif self.occ == 0x23:
                tmp = self.AcquireCharConstants()
            elif self.cc == 0x25:
                tmp = self.AcquireCEncodedString()
            s = s + tmp
            
            if self.bEOF or CHAR_MAP[self.occ] != IS_STRSTARTER:
                break
            #--
        #--
        return self.getTokenInfo(STRCONST_T, s)
    #--
      
    def AcquireNumber(self, csign):
        stmp = csign
        while True:
            stmp = stmp + self.cc
            self.NextChar()
            if self.bEOF or CHAR_MAP[self.occ] != IS_DIGIT:
                break;
            #--
        #--
        
        if self.cc == '.':
            stmp = stmp + self.cc
            self.NextChar()
            if CHAR_MAP[self.cc] == IS_DIGIT:
                while True:
                    stmp = stmp + self.cc
                    self.NextChar()
                    if self.bEOF or CHAR_MAP[self.occ] != IS_DIGIT:
                        break
                    #--
                #--
            else:
                raise ScanException("Digits are expected after '.' ")
            #--
            return self.getTokenInfo(REALCONST_T, float(stmp))
        else:
            return self.getTokenInfo(INTCONST_T, int(stmp))
        #--
    #--
      
    def PickID(self):
        stmp = ""
        iCount = 0
        while True:
            stmp += self.cc
            iCount = iCount + 1
            self.NextChar()
            if iCount == MAX_ID_LEN or self.bEOF or CHAR_MAP[self.occ] != IS_ALPHABET and CHAR_MAP[self.occ] != IS_DIGIT:
                break
        #--
        if iCount == MAX_ID_LEN and (CHAR_MAP[self.occ] == IS_ALPHABET or CHAR_MAP[self.occ] == IS_DIGIT):
            raise ScanException("Identifier too long")
        return stmp
    #--
      
    def AcquireQuotedID(self):
        self.NextChar()
        if CHAR_MAP[self.occ] != IS_ALPHABET:
            raise ScanException("Identifier expected")
        #--
        result = self.PickID()
        if cc != QUOTE:
            raise ScanException("Closing quote required")
        #--
        return self.getTokenInfo(IDENTIFIER_T, result)
    #--
        
    def AcquireOpenParOrComment(self):
        self.NextChar()
        if self.occ != 0x2a:
            return self.getTokenInfo(OPENPAR_T, None)
        commentPassed = False
        self.NextChar()
        while True:
            while self.occ != 0x2a and not self.bEOF:
                if self.occ == 0x0a:
                    self.lineNum += 1
                self.NextChar()
            #--
            if self.bEOF: 
                raise ScanException("Unclosed comment")
            #--
            while self.occ == 0x2a:
                self.NextChar()
            #--
            if self.occ == 0x29:
                self.NextChar()
                break
            else:
                self.NextChar()
            #--
        #--
        return None
    #--
      
    def AcquireDivOrComment(self):
        self.NextChar()
        if self.occ != 0x2a:
            return self.getTokenInfo(DIV_T, None)
        commentPassed = False
        self.NextChar()
        while True:
            while self.occ != 0x2a and not self.bEOF:
                if self.cc == '\x0a':
                    self.lineNum += 1
                self.NextChar()
            #--
            if self.bEOF: 
                raise ScanException("Unclosed comment")
            #--
            while self.occ == 0x2a:
                self.NextChar()
            #--
            if self.occ == 0x2f:
                self.NextChar()
                break
            else:
                self.NextChar()
            #--
        #--
        return None
    #--
      
    def AcquireGTE(self):
        self.NextChar()
        if self.occ == 0x3d:
            self.NextChar()
            return self.getTokenInfo(GE_T, None)
        else:
            return self.getTokenInfo(GT_T, None)
        #--
    #--
    
    def AcquireLTE(self):
        self.NextChar()
        if self.occ == 0x3d:
            self.NextChar()
            return self.getTokenInfo(LE_T, None)
        else:
            return self.getTokenInfo(LT_T, None)
        #--
    #--
    
    def Scan(self):
        while True:
            self.PassWhiteSpaces()
            if self.bEOF:
                return None
            t = CHAR_MAP[self.occ]
            if t == IS_ALPHABET:
                s = self.PickID()
                return self.TestKeyword(s)
            elif self.occ == 0x22:
                return self.AcquireQuotedID()
            elif t == IS_STRSTARTER:
                return self.AcquireString()
            elif self.occ == 0x2d:
                self.NextChar()
                t = CHAR_MAP[self.occ]
                if t == IS_DIGIT:
                    return self.AcquireNumber('-')
                else:
                    return self.getTokenInfo(MINUS_T, None)
                #--
            #--
            elif t == IS_DIGIT:
                return self.AcquireNumber('')
            elif self.occ == 0x28:
                t = self.AcquireOpenParOrComment()
                if t != None:
                    return t
                #--
            elif self.occ == 0x2f:
                t = self.AcquireDivOrComment()
                if t != None:
                    return t
            elif self.occ == 0x3e:
                return self.AcquireGTE()
            elif self.occ == 0x3c:
                return self.AcquireLTE()
            elif self.occ == 0x7c:
                self.NextChar()
                if self.occ == 0x7c:
                    raise ScanException("Invalid concat symbol")
                self.NextChar()
                return self.getTokenInfo(CONCAT_T, None)
            else:
                tokmapping = SYM_TOKEN_MAPPING.get(self.cc)
                if tokmapping != None:
                    self.NextChar()
                    return self.getTokenInfo(tokmapping, None)
                else:
                    raise ScanException("Invalid character. chr(0x%x)" % self.occ)
                #--
            #--
        #-- while
    #--
    pass
#--

class sobjstreamwrapper: # prototype class for stream wrapper
    def getActualStream(self): pass
    def readHeading(self): pass
    def readPropertyName(self): pass
    def readObjectInfo(self): pass
    def readObjectClosing(self): pass
  
    def readValue(self, propInfo): pass
    def readReference(self): pass
  
    def readVectorInfo(self): pass
    def readEndOfVector(self, bCheckSeparator): pass
  
    def writeHeading(self, versionRecord): pass
    def writePropertyName(self, propertyName): pass
    def writeObjectInfo(self, objTypeName): pass
    def writeObjectClosing(self): pass
  
    def writeValue(self, propInfo, value): pass
    def writeReference(self, aObjRef): pass
    def writeVectorInfo(self): pass
    def writeVectorItemSeparator(self): pass
    def writeEndOfVector(self): pass
  
    def prepareLoad(self): pass
    def prepareSave(self): pass
  
    def readEnumeration(self): pass
    def writeEnumeration(self, aEnumConst): pass
    
    def getLocationInfo(self): return ""
#--

def getM(obj, sAttr, defaultVal = None): 
    return obj.__dict__.get(sAttr, defaultVal)

class sobjhelper:
    def __init__(self, helperMode, metadata, rootInstance, dataStreamWrapper = None):
        self.metadata = metadata
        self.rootInstance = rootInstance
        self.headerlessMode = False
        if helperMode == 0: # read
            self.streamWrapper = dataStreamWrapper
            self.streamWrapper.prepareLoad()
        elif helperMode == 1: # write
            self.streamWrapper = dataStreamWrapper
            self.streamWrapper.prepareSave()
        elif helperMode == 2: # other
            self.streamWrapper = None
        #--
        pass
      
    def processMDInheritances(metadata):
        # call this static method before using metadata
        # change: need to get list all properties for only name, type and index (for vector)
        for key in list(metadata.keys()):
            clsInfo = metadata[key]
            if MD_ALLPROPERTIES in clsInfo:
                continue # already defined
            dAllProperties = {}
            dAllPropBasic = {}        
            clsInfo[MD_ALLPROPERTIES] = dAllProperties
            clsInfo[MD_ALLPROPERTIES_BASIC_INFO] = dAllPropBasic
            currentClass = clsInfo
            clsInfos = []
            while currentClass != None:
                clsInfos.insert(0, currentClass)
                if MD_ANCESTOR in currentClass:
                    ancestorClsName = currentClass[MD_ANCESTOR]
                    ancestorClsInfo = metadata[ancestorClsName.lower()]
                    currentClass = ancestorClsInfo
                else:
                    currentClass = None
                #--
            #--
            n = len(clsInfos)
            for i in range(n):
                aInfo = clsInfos[i]
                props = aInfo[MD_PROPERTIES]
                for key in list(props.keys()):
                    dAllProperties[key] = props[key]
                    dAllPropBasic[key] = dict(
                        [kv for kv in list(props[key].items()) if kv[0] in (
                                MD_PROPERTYNAME, MD_DATATYPE, MD_NAMEATTRNAME, MD_IDX_DICT_PROP
                            )]
                    )
            #--
        #--

        pass
    #--
    processMDInheritances = staticmethod(processMDInheritances)
    
    def getRootInstanceInfo(self):
        dRoot = self.rootInstance.__dict__
        if 'soDocTypeGUID' not in dRoot:
            raise SObjectException("Root instance does not have soDocTypeGUID attribute")
        docTypeGUID = str(self.rootInstance.soDocTypeGUID)
        docID = ""
        if 'soDocID' in dRoot and type(self.rootInstance.soDocID) is StringType:
            docID = self.rootInstance.soDocID
        majorVersion = 0
        if 'soMajorVersion' in dRoot and type(self.rootInstance.soMajorVersion) is IntType:
            majorVersion = self.rootInstance.soMajorVersion
        minorVersion = 0
        if 'soMinorVersion' in dRoot and type(self.rootInstance.soMinorVersion) is IntType:
            minorVersion = self.rootInstance.soMinorVersion
        return {'docTypeGUID': docTypeGUID, 'docID': docID, 'majorVersion': majorVersion, 'minorVersion': minorVersion}
    #--
    
    def checkWriteProperty(self, obj, propDef):
        if MD_WRITEMODE in propDef:
            wm = propDef[MD_WRITEMODE]
        else:
            wm = wmNormal      
        if wm == wmNeverWrite:
            return False;
        if wm == wmAlwaysWrite:
            return True
        dtype = propDef[MD_DATATYPE]
        pKind = PROP_TYPEKIND_MAP[dtype]
        if MD_CHKSTOREPROPF in propDef:
            f = propDef[MD_CHKSTOREPROPF]
            if callable(f):
                return f(obj)
            #-
        #-
        if pKind in [pkOrdinary, pkObject, pkObjectRef]:
            val = self.getPropertyValue(obj, propDef)
            if val == None:
                return False
            return val != "" and val != 0 and val != 0.0 and val != False
        elif pKind == pkVector:
            val = self.getPropertyValue(obj, propDef)
            return val != None and type(val) is list and len(val) > 0
          
        return True
    #--
    
    def getClassMetadata(self, obj):
        clsName = obj.soClassName
        try:
            clsInfo = self.metadata[clsName]
        except:
            raise SObjectException("Class metadata %s not found" % obj.soClassName)
        #--
        return clsInfo
    #--
    
    def getObjectName(self, obj, sExcMsgIfNone = None):
        clsInfo = self.getClassMetadata(obj)
        if MD_NAMEATTRNAME in clsInfo and clsInfo[MD_NAMEATTRNAME] != None:
            namePropName = clsInfo[MD_NAMEATTRNAME]
            namePropInfo = clsInfo[MD_ALLPROPERTIES][namePropName.lower()]
            nameValue = self.getPropertyValue(obj, namePropInfo)
        else:
            nameValue = None
        if nameValue == None and sExcMsgIfNone != None :
            raise SObjectException(sExcMsgIfNone)
        return nameValue
    #--
    
    def isAnyPropertyToWrite(self, obj):
        clsInfo = self.getClassMetadata(obj)
        if MD_NAMEATTRNAME in clsInfo and clsInfo[MD_NAMEATTRNAME] != None:
            return True
        dProps = clsInfo[MD_PROPERTIES]
        anyProp = False
        for key in list(dProps.keys()):
            if self.checkWriteProperty(obj, dProps[key]):
                anyProp = True
                break
            #--
        #--
        return anyProp
    #--
      
    def isThisObjectWritten(self, obj):
        clsInfo = self.getClassMetadata(obj)
        if MD_CHKSTOREOBJF in clsInfo and clsInfo[MD_CHKSTOREOBJF]  != None:
            f = clsInfo[MD_CHKSTOREOBJF]
            return f(obj)
        else:
            return True
        #--
    #--
      
    def extendAddress(self, obj, prevAddress, propInfo, itemName):
        addrExt = [propInfo[MD_PROPERTYNAME], itemName, propInfo]
        newAddress = prevAddress[:]
        newAddress.append(addrExt) # copy and append
        obj.__dict__['__soAddress'] =  newAddress
    #--
      
    def delAddress(self, obj):
        dObj = obj.__dict__
        dObj['__soState'] = cstNormal
        if '__soAddress' in dObj: del dObj['__soAddress']
        clsInfo = self.getClassMetadata(obj)
        allProperties = clsInfo[MD_ALLPROPERTIES]
        for key in list(allProperties.keys()):
            propInfo = allProperties[key]
            dType = propInfo[MD_DATATYPE]
            if dType == ptObject:
                val = self.getPropertyValue(obj, propInfo)
                if val != None:
                    self.delAddress(val)
            elif dType == ptObjectVect:
                vect = self.getPropertyValue(obj, propInfo)
                nLen = len(vect)
                for i in range(nLen):
                    val = vect[i]
                    if val != None: self.delAddress(val)
                #--
            #-- elif
        #-- for
        pass
    #-- def
    
    def symSearchAddress(self, symObjAddress):
        def getNextObject(currentObject, propertyName, itemName):
            objClsInfo = self.getClassMetadata(currentObject)
            objClassName = objClsInfo[MD_CLASSNAME]
            allProperties = objClsInfo[MD_ALLPROPERTIES]
            if propertyName.lower() not in allProperties:
                raise SObjectException("Property %s not found in class %s" % (propertyName, objClassName))
            propInfo = allProperties[propertyName.lower()]
            dType = propInfo[MD_DATATYPE]
            if dType not in [ptObject, ptObjectVect] or (dType == ptObjectVect and itemName == None):
                raise SObjectException("Property %s in class %s is not valid for object resolution" % (propertyName, objClassName))
            if dType == ptObject:
                return self.getPropertyValue(currentObject, propInfo)
            else:
                if MD_RESOLVERF not in propInfo: # try to solve from vector index
                    voIndex = currentObject.__dict__['__soVectorIndexes'][propInfo[MD_PROPERTYNAME]]
                    result = voIndex[itemName.lower()]
                    #raise SObjectException, "Property %s in class %s does not have name resolution function" % (propertyName, objClassName)
                else:
                    resolverF = propInfo[MD_RESOLVERF]
                    result = resolverF(currentObject, itemName.lower())
                if result == None:
                    raise SObjectException("Symbolic name resolution failed. Property %s class %s Item name %s" % (propertyName, objClassName, itemName))
                #--
                return result
            #--
            pass
        #--
          
        if len(symObjAddress) == 0:
            return self.rootInstance
        i = 0
        result = None
        currentObject = self.rootInstance
        while True:
            nextPropName = symObjAddress[i][0]
            nextItemName = symObjAddress[i][1]
            nextObject = getNextObject(currentObject, nextPropName, nextItemName)
            currentObject = nextObject
            if nextObject == None and i < len(symObjAddress) - 1:
                raise SObjectException("SOBJ: Error resolving address. Failed to walk to the next object")
            #--
            if i == len(symObjAddress) - 1:
                return currentObject
            i += 1
            #--
        #--
        pass
    #--
    
    def resolveObjectAddress(self, obj):
        dObj = obj.__dict__
        dClass = obj.__class__.__dict__
        dObj['__soState'] = cstResolving
        clsInfo = self.getClassMetadata(obj)
        beforeResolveMethod = dClass.get('sobjStartResolveAddress', None)
    
        if beforeResolveMethod is not None and callable(beforeResolveMethod):
            beforeResolveMethod(obj)
      
        onResolveItemMethod = dClass.get('sobjOnResolveAddress', None)
    
        if '__soPendingRefs' in dObj:
            pendingRefs = dObj['__soPendingRefs']
            nLen = len(pendingRefs)
            for i in range(nLen):
                pendingRef = pendingRefs[i]
                propInfo = pendingRef['propertyInfo']
                dType = propInfo[MD_DATATYPE]
                symObjRefAddress = pendingRef['symObjRefAddress']
                try:
                    if onResolveItemMethod and callable(onResolveItemMethod):
                        refObj = onResolveItemMethod(obj, propInfo, symObjRefAddress)
                    else:
                        refObj = None
                    if refObj == None:
                        refObj = self.symSearchAddress(symObjRefAddress)
                except:
                    msg = str(sys.exc_info()[1])
                    raise SObjectException("Could not resolve reference in property %s class %s\r\nDetails: %s" % (
                      propInfo[MD_PROPERTYNAME],
                      clsInfo[MD_CLASSNAME],
                      msg
                    ))
                #--
                
                if dType == ptObjectRef:
                    self.setPropertyValue(obj, propInfo, refObj)
                elif dType == ptObjectRefVect:
                    vect = self.getPropertyValue(obj, propInfo)
                    vect[pendingRef['locationIndex']] = refObj
                #--
            #--
            del dObj['__soPendingRefs']
        #--
        allProps = clsInfo[MD_ALLPROPERTIES]
        for key in list(allProps.keys()):
            propInfo = allProps[key]
            dType = propInfo[MD_DATATYPE]
            if dType == ptObject:
                val = self.getPropertyValue(obj, propInfo)
                if val != None:
                    self.resolveObjectAddress(val)
            elif dType == ptObjectVect:
                val = self.getPropertyValue(obj, propInfo)
                nLen = len(val)
                for i in range(nLen):
                    if val[i] != None: self.resolveObjectAddress(val[i])
                #--
            #--
        #--
        pass
    #-- def
    
    def clearVectorObjectIndexes(self, obj):
      
        if '__soVectorIndexes' in obj.__dict__:
            del obj.__dict__['__soVectorIndexes']
        clsInfo = self.getClassMetadata(obj)
        allProps = clsInfo[MD_ALLPROPERTIES]
        for key in list(allProps.keys()):
            propInfo = allProps[key]
            dType = propInfo[MD_DATATYPE]
            if dType == ptObject:
                val = self.getPropertyValue(obj, propInfo)
                if val != None:
                    self.clearVectorObjectIndexes(val)
            elif dType == ptObjectVect:
                val = self.getPropertyValue(obj, propInfo)
                nLen = len(val)
                for i in range(nLen):
                    if val[i] != None: self.clearVectorObjectIndexes(val[i])
                #--
            #--
        #--
      
    def getPropertyValue(self, obj, propInfo):
        propertyName = propInfo[MD_PROPERTYNAME]
        if propertyName not in obj.__dict__:
            raise SObjectException("Property instance %s not found in object %s" % (propertyName, repr(obj)))
        dType = propInfo[MD_DATATYPE]
        pKind = PROP_TYPEKIND_MAP[dType]
        if pKind != pkVector and MD_READERF in propInfo:
            readerF = propInfo[MD_READERF]
        else:
            readerF = None
        if readerF == None:
            val = obj.__dict__[propertyName]
        else:
            val = readerF(obj)
        #--
        if pKind == pkOrdinary:
            if val == None:
                raise SObjectException("Ordinary property %s cannot have None value" % propertyName)
        # elif pKind in [pkObject, pkObjectRef]:# not valid for python 3
        #     if val != None and not (type(val) is InstanceType):
        #         raise SObjectException("Object property %s does not contain object" % propertyName)
        #--
        elif pKind == pkVector:
            if not (type(val) is list):
                raise SObjectException("List property %s does not contain list" % propertyName)
            #--
        #--
        return val
    #--      
    
    def setPropertyValue(self, obj, propInfo, value):
        propertyName = propInfo[MD_PROPERTYNAME]
        dType = propInfo[MD_DATATYPE]
        pKind = PROP_TYPEKIND_MAP[dType]
        if MD_WRITERF in propInfo:
            writerF = propInfo[MD_WRITERF]
        else:
            writerF = None
        if writerF != None:
            writerF(obj, value)
        else:
            obj.__dict__[propertyName] = value
        #--
    #--
    
    def generateObjectAddress(self, obj):
        dObj = obj.__dict__
        dObj['__soState'] = cstPreparingRef
        if '__soAddress' not in dObj: dObj['__soAddress'] = []
        clsInfo = self.getClassMetadata(obj)
        allProperties = clsInfo[MD_ALLPROPERTIES]
        for key in list(allProperties.keys()):
            propInfo = allProperties[key]
            propertyName = propInfo[MD_PROPERTYNAME]
            dType = propInfo[MD_DATATYPE]
            if MD_NOADDRESS in propInfo: continue
            if dType == ptObject:
                val = self.getPropertyValue(obj, propInfo)
                if val != None:
                    self.extendAddress(val, dObj['__soAddress'], propInfo, "")
                    self.generateObjectAddress(val)
                #--
            elif dType == ptObjectVect:
                lst = self.getPropertyValue(obj, propInfo)
                nLen = len(lst)
                for i in range(nLen):
                    val = lst[i]
                    if val != None:
                        valClsInfo = self.getClassMetadata(val)
                        valName = self.getObjectName(val, "Instance of class %s within object list / collection does not have naming attribute" % valClsInfo[MD_CLASSNAME])
                        self.extendAddress(val, dObj['__soAddress'], propInfo, valName)
                        self.generateObjectAddress(val)
                    #-- if
                #-- for i
            #-- elif
        #-- for key in allProperties
        pass
    #-- def
    
    def saveProperties(self, obj):
    
        clsInfo = None
        propInfo = None
        
        def saveObject(bWriteSeparator, anObject):
            if anObject == None:
                clsName = ""
            else:
                clsInfo = self.getClassMetadata(anObject)
                clsName = clsInfo[MD_CLASSNAME]
            #--
            if bWriteSeparator:
                self.streamWrapper.writeVectorItemSeparator()
            self.streamWrapper.writeObjectInfo(clsName)
            if anObject != None:
                self.saveProperties(anObject)
                self.streamWrapper.writeObjectClosing()
            #--
        #--
        
        def saveObjectRef(anObject):
            if anObject == None:
                self.streamWrapper.writeReference(None)
                return
            #--
            dObj = anObject.__dict__
            if '__soAddress' not in dObj:
                raise SObjectException("Variable refers to unresolved object. Class %s, property %s" % (clsInfo[MD_CLASSNAME], propInfo[MD_PROPERTYNAME]))
            objRefAddress = dObj['__soAddress']
            if objRefAddress == None: 
                raise SObjectException("Variable refers to unresolved object. Class %s, property %s" % (clsInfo[MD_CLASSNAME], propInfo[MD_PROPERTYNAME]))
            self.streamWrapper.writeReference(objRefAddress)
        #--
        
        obj.__dict__['__soState'] = cstWriting
        clsInfo = self.getClassMetadata(obj)
        allProperties = clsInfo[MD_ALLPROPERTIES]
        for prop in list(allProperties.keys()):
            propInfo = allProperties[prop]
            dType = propInfo[MD_DATATYPE]
            pKind = PROP_TYPEKIND_MAP[dType]
            bWriteThisProperty = self.checkWriteProperty(obj, propInfo)
            if not bWriteThisProperty:
                continue
            #--
            self.streamWrapper.writePropertyName(propInfo[MD_PROPERTYNAME])
            value = self.getPropertyValue(obj, propInfo)
            if pKind == pkOrdinary and (dType != ptInteger or MD_ENUM not in propInfo):
                self.streamWrapper.writeValue(propInfo, value)
            elif pKind == pkOrdinary and (dType == ptInteger and MD_ENUM in propInfo):
                try:
                    enumName = propInfo[MD_ENUM][value]
                except:
                    raise SObjectException("Enum value out of range. Class %s, property %s, value %s" % (clsInfo[MD_CLASSNAME], propInfo[MD_PROPERTYNAME], str(value)))
                #--
                self.streamWrapper.writeEnumeration(enumName)
            elif pKind == pkObject:
                saveObject(False, value)
            elif pKind == pkObjectRef:
                saveObjectRef(value)
            elif pKind == pkVector:
                self.streamWrapper.writeVectorInfo()
                nMember = len(value)
                for j in range(nMember):
                    if dType == ptObjectVect:
                        saveObject(j > 0, value[j])
                    elif dType == ptObjectRefVect:
                        saveObjectRef(value[j])
                    #--
                #--
                self.streamWrapper.writeEndOfVector()
            #-- ifs
        #-- for 
        pass    
    #-- def saveProperties           
      
    def loadProperties(self, obj):
        clsInfo = None
        propInfo = None
        locIndex = 0
        
        def loadObject(oObject, basicPropertyInfo):
            clsName = self.streamWrapper.readObjectInfo()
            if clsName == "":
                return None
              
            try:
                clsInfo = self.metadata[clsName.lower()] 
            except:
                raise SObjectException("Metadata not found for class %s" % clsName)
            #--
            
            classType = clsInfo[MD_CLASSF]
            if oObject != None:
                if not isinstance(oObject, classType):
                    raise SObjectException("Contained object has different class specification")
                anObject = oObject
            else:
                anObject = classType(obj)

            # newly added code to comply with objUtil
            anObject.__dict__['..'] = obj # set parent
            anObject.__dict__['...'] = basicPropertyInfo # set property info
            anObject.__dict__['@properties'] = clsInfo[MD_ALLPROPERTIES_BASIC_INFO] # set class info
            # end of newly added code
            self.loadProperties(anObject)
            self.streamWrapper.readObjectClosing()
            return anObject
        #--
        
        def loadObjectRef():
            symref = self.streamWrapper.readReference()
            if symref == None:
                return None
            dObj = obj.__dict__
            if '__soPendingRefs' not in dObj:
                dObj['__soPendingRefs'] = []
            pendingRef = {
              'propertyInfo': propInfo,
              'locationIndex': locIndex,
              'symObjRefAddress': symref
            }
            dObj['__soPendingRefs'].append(pendingRef)
            return pendingRef
        #--
        
        #t0 = time.time()
        dObj = obj.__dict__
        dObj['__soState'] = cstReading
        clsInfo = self.getClassMetadata(obj)
        allProperties = clsInfo[MD_ALLPROPERTIES]
        propertyName = self.streamWrapper.readPropertyName()
        while propertyName != None:
            try:
                propInfo = allProperties[propertyName.lower()]
            except:
                raise SObjectException("Property name not found %s in class %s" % (propertyName, clsInfo[MD_CLASSNAME]))
            #--
            basicPropInfo = clsInfo[MD_ALLPROPERTIES_BASIC_INFO][propertyName.lower()]
            dType = propInfo[MD_DATATYPE]
            pKind = PROP_TYPEKIND_MAP[dType]
            if pKind == pkOrdinary and (dType != ptInteger or MD_ENUM not in propInfo):
                value = self.streamWrapper.readValue(propInfo)
                self.setPropertyValue(obj, propInfo, value)
            elif pKind == pkOrdinary and dType == ptInteger and MD_ENUM in propInfo:
                enumVal = self.streamWrapper.readEnumeration()
                enumValues = propInfo[MD_ENUM]
                iVal = enumValues.index(enumVal)
                if iVal < 0:
                    raise SObjectException("%s is not a valid enum constant" % enumVal)
                self.setPropertyValue(obj, propInfo, iVal)
            elif pKind == pkObject:
                anObject = self.getPropertyValue(obj, propInfo)
                if anObject == None:
                    anObject = loadObject(None, basicPropInfo)
                    self.setPropertyValue(obj, propInfo, anObject)
                else:
                    loadObject(anObject, basicPropInfo)
                #--
            elif pKind == pkObjectRef:
                locIndex = 0
                loadObjectRef()
            elif pKind == pkVector:
                vector = self.getPropertyValue(obj, propInfo)
                if dType == ptObjectVect and MD_RESOLVERF not in propInfo and MD_NOADDRESS not in propInfo:
                    ## for object vector properties without custom resolver function
                    ## we create a dummy property named __soVectorIndexes to index all object vectors by its identification property
                    ## except for property that are marked with MD_NOADDRESS.
                    ## __soVectorIndexes is a dictionary with vector property names as keys
                    ## every member of the dictionary will contain another dictionary which act as index
                    if '__soVectorIndexes' not in dObj:
                        dObj['__soVectorIndexes'] = {}
                    voIndex = {}
                    dObj['__soVectorIndexes'][propInfo[MD_PROPERTYNAME]] = voIndex
                else:
                    voIndex = None
                    
                #--
                del vector[:] # clear all members
                self.streamWrapper.readVectorInfo()
                if dType == ptObjectVect:
                    if MD_ADDERF in propInfo:
                        adderF = propInfo[MD_ADDERF]
                        if not callable(adderF):
                            raise SObjectException("Invalid collection adder function in property %s class %s" % (propInfo[MD_PROPERTYNAME], clsInfo[MD_CLASSNAME]))
                    else:
                        adderF = None
                    #--
                else:
                    adderF = None
                j = 0
                bEndOfListMember = self.streamWrapper.readEndOfVector(False)
                while not bEndOfListMember:
                    locIndex = j
                    if dType == ptObjectVect:
                        anObject = loadObject(None, basicPropInfo)
                        if adderF != None:
                            adderF(obj, anObject)
                        else:
                            vector.append(anObject)
                        if voIndex != None and anObject != None:
                            itemClsInfo = self.getClassMetadata(anObject)
                            id = self.getObjectName(anObject, "Class %s within object list / collection does not have naming attribute" % itemClsInfo[MD_CLASSNAME])
                            voIndex[id.lower()] = anObject
                        #-- if voIndex != None
                    else:
                        loadObjectRef()
                        vector.append(None)
                    #--
                    j += 1
                    bEndOfListMember = self.streamWrapper.readEndOfVector(True)
                #-- while
            #-- elif pKind == pkVector
            propertyName = self.streamWrapper.readPropertyName()
        #-- while propertyName
        t1 = time.time()
        #print "Load object: %s. Time = %f secs" % (clsInfo[MD_CLASSNAME], t1 - t0)
        pass
    #--
    
    def saveInstance(self):
        self.generateObjectAddress(self.rootInstance)
        try:
            if not self.headerlessMode:
                vRecord = self.getRootInstanceInfo()
                vRecord['libGUID'] = LibGUID
                self.streamWrapper.writeHeading(vRecord)
            #--
            clsInfo = self.getClassMetadata(self.rootInstance)
            self.streamWrapper.writeObjectInfo(clsInfo[MD_CLASSNAME])
            self.saveProperties(self.rootInstance)
            self.streamWrapper.writeObjectClosing()
        finally:
            self.delAddress(self.rootInstance)
        #--
        pass
    #-- def
      
    def internalLoadInstance(self):
        self.loadState = 0
        if not self.headerlessMode:
            heading = self.streamWrapper.readHeading()
            if heading['libGUID'] != LibGUID:
                raise SObjectException("Invalid library version. Expected %s read %s" % (
                  LibGUID,
                  heading['libGUID']
                ))
            if heading['docTypeGUID'] != self.rootInstance.soDocTypeGUID:
                raise SObjectException("Invalid document type version. Expected %s read %s" % (
                  self.rootInstance.soDocTypeGUID,
                  heading['docTypeGUID']
                ))
            #--
        #-- if not self.headerlessMode
        objClassName = self.streamWrapper.readObjectInfo()
        clsInfo = self.getClassMetadata(self.rootInstance)
        if objClassName != clsInfo[MD_CLASSNAME]:
            raise SObjectException("Invalid class name. Expected %s found %s" % (
              clsInfo[MD_CLASSNAME],
              objClassName
            ))
        #--
        try:
            t0 = time.time()

            # newly added code for compatibility with objUtil
            self.rootInstance.__dict__['..'] = None
            self.rootInstance.__dict__['...'] = None
            self.rootInstance.__dict__['@properties'] = clsInfo[MD_ALLPROPERTIES_BASIC_INFO]
            # end of newly added code

            self.loadProperties(self.rootInstance)
            self.streamWrapper.readObjectClosing()
            self.loadState = 1
            t1 = time.time()
            methBeforeResolve = self.rootInstance.__class__.__dict__.get('beforeResolveAddress', None)
            if callable(methBeforeResolve):
                methBeforeResolve(self.rootInstance)
            self.resolveObjectAddress(self.rootInstance)
            t2 = time.time()
            #print "load time = %f secs" % (t1 - t0)
            #print "resolve time = %f secs" % (t2 - t1)
            self.clearVectorObjectIndexes(self.rootInstance)
        finally:
            self.rootInstance.__dict__['__soState'] = cstNormal
        #--
        pass
    #--
  
    # def loadInstance(self):
        # self.internalLoadInstance()
        
    def loadInstance(self):
        try:
            self.internalLoadInstance()
        except:
            if self.loadState == 0:
                excinfo = sys.exc_info()
                tb = excinfo[2]
                msg = str(excinfo[1])
                err = "%s - process file (%s)" % (msg, self.streamWrapper.getLocationInfo())
                raise SObjectException(err)
            else:
                raise
            #--
        #--
        pass
    #--
    
    def initializeInstance(metadata, className, obj, parentObj = None):
        # init default properties for a class instance with help from metadata
        if className.lower() not in metadata:
            raise SObjectException("Metadata not found for class %s" % className)
        clsInfo = metadata[className.lower()]
        if obj == None:
            obj = clsInfo[MD_CLASSF](parentObj)
        allProperties = clsInfo[MD_ALLPROPERTIES]
        allKeys = list(allProperties.keys())
        for key in allKeys:
            propInfo = allProperties[key]
            propName = propInfo[MD_PROPERTYNAME]
            dType = int(propInfo[MD_DATATYPE])
            if dType == ptDate:
                obj.__dict__[propName] = [1970, 12, 31, 12, 0, 0, 0, 0, 0]
            elif dType == ptObjectVect:
                obj.__dict__[propName] = []
            elif dType == ptObjectRefVect:
                obj.__dict__[propName] = []
            else:
                obj.__dict__[propName] = PROPERTY_DEFAULT_VALUES[dType]
            #--
        #--
        return obj
    #-- def
    initializeInstance = staticmethod(initializeInstance)
    
    def resolvePath(self, objectPath, base = None): # if base == None, self.rootInstance is used as source path
        def getNextMember(obj, nextAddr):
            classInfo = getM(obj, '@properties') 
            propInfo = classInfo.get(nextAddr.lower())
            if propInfo == None:
                raise Exception('Property "%s" not found in class of type "%s"' % (nextAddr, obj.__class__.SO_CLASSNAME))
            return getM(obj, propInfo[MD_IDX_DICT_PROP]) if propInfo[MD_DATATYPE] == ptObjectVect and propInfo.get(MD_IDX_DICT_PROP) != None else getM(obj, nextAddr)

        def internalResolve(cur, revAddrList, i):
            nextAddr = revAddrList[i].lower()
            typeInfo = type(cur)
            next = (
                    getNextMember(cur, nextAddr) if nextAddr != '..' else getM(cur, '..')
                ) if typeInfo is InstanceType else (
                cur[int(nextAddr)] if typeInfo is ListType else (
                    cur[nextAddr] if typeInfo is DictionaryType else None
                )
            )
            return internalResolve(next, revAddrList, i - 1) if i > 0 else next

        ral = tuple(reversed(objectPath.split('/')))
        oBase = base or self.rootInstance
        return internalResolve(oBase, ral, len(ral) - 1) if len(ral) > 0 and ral[0] != '' else oBase
  
    def dfmGetObjClassAndObjName(self, objVal):
        objClassInfo = self.getClassMetadata(objVal)
        pObjDfmOptions = objClassInfo.get(MD_DFMOPTIONS, {})
        objClassName = pObjDfmOptions.get(DFM_ALIAS, None) or objClassInfo[MD_CLASSNAME]
        objName = self.getObjectName(objVal, "The name of class %s instance is not defined" % objClassInfo[MD_CLASSNAME])
        return objName, objClassName
    #--
    
    def dfmWriteSubObject(self, dfmStream, obj, preNameOfObjAndType = None):
        objClassInfo = self.getClassMetadata(obj)
        pObjDfmOptions = objClassInfo.get(MD_DFMOPTIONS, {})
        if preNameOfObjAndType == None:
            objName, objClassName = self.dfmGetObjClassAndObjName(obj)
        else:
            objName, objClassName = preNameOfObjAndType
        if DFM_DELEGATEPROPS not in pObjDfmOptions:
            dfmStream.writeObjectOpening(objName, objClassName)
            self.internalWriteAsDFM(dfmStream, obj)
            dfmStream.writeObjectClosing()
        else:
            delPropOptions = pObjDfmOptions[DFM_DELEGATEPROPS]
            delObjName = delPropOptions[DFM_DPOBJNAME]
            delClsName = delPropOptions[DFM_DPCLSNAME]
            delProps = delPropOptions[DFM_DPPROPNAMES]
            undelProps = []
            allProps = objClassInfo[MD_ALLPROPERTIES]
            allPropKeys = list(allProps.keys())
            for key in allPropKeys:
                if key not in delProps:
                    undelProps.append(key)
                #--
            #--
            dfmStream.writeObjectOpening(objName, objClassName)
            self.internalWriteAsDFM(dfmStream, obj, True, undelProps)
            dfmStream.writeObjectOpening(delObjName, delClsName)
            self.internalWriteAsDFM(dfmStream, obj, False, delProps)
            dfmStream.writeObjectClosing()
            dfmStream.writeObjectClosing()
        #-- else
        pass
    #-- def
    
    def internalWriteAsDFM(self, dfmStream, obj, includeDfmProperties = True, propertyFilterNames = None, objFlatPrevPropName = None):
    
        clsInfo = self.getClassMetadata(obj)
        pClsDfmOptions = clsInfo.get(MD_DFMOPTIONS, {})
        if DFM_PREPROCESS in pClsDfmOptions:
            obj.dfmPreprocess()
            
        allPropsExt = {}
        props = clsInfo[MD_ALLPROPERTIES]; propKeys = list(props.keys())
        for propID in propKeys:
            if propertyFilterNames != None and propID not in propertyFilterNames:
                continue
            allPropsExt[propID] = props[propID]
        #--
        # take into account all dfm extended properties
        if includeDfmProperties:
            dfmExtProps = pClsDfmOptions.get(DFM_PROPERTIES, {}); propKeys = list(dfmExtProps.keys())
            for propID in propKeys:
                allPropsExt[propID] = dfmExtProps[propID]
        #--
    
        propKeys = list(allPropsExt.keys())
        tmp = []
        for propID in allPropsExt:
            tmp.append(allPropsExt[propID])
        tmp.sort(lambda p1, p2: cmp(p1[MD_DATATYPE], p2[MD_DATATYPE]))
        allProperties = tmp
        
        for propInfo in allProperties:
            dType = propInfo[MD_DATATYPE]
            pKind = PROP_TYPEKIND_MAP[dType]
            pDfmOptions = propInfo.get(MD_DFMOPTIONS, {})
            if DFM_NOWRITE in pDfmOptions:
                continue
            if not self.checkWriteProperty(obj, propInfo):
                continue
            if DFM_CONDITIONALALIAS in pDfmOptions:
                dfmPropertyName = obj.dfmGetClassAlias()
            else:
                dfmPropertyName = pDfmOptions.get(DFM_ALIAS, None) or propInfo[MD_PROPERTYNAME]
            if objFlatPrevPropName != None:
                dfmPropertyName = "%s.%s" % (objFlatPrevPropName, dfmPropertyName)
            if dType == ptString and DFM_STRASLIST in pDfmOptions:
                dfmPropertyName = "%s.Strings" % dfmPropertyName
            if pKind == pkOrdinary:
                dfmStream.writePropertyName(dfmPropertyName)
                propVal = self.getPropertyValue(obj, propInfo)
                if dType == ptString and DFM_STRASIDENTIFIER in pDfmOptions:
                    dfmStream.writeIdentifier(propVal)
                elif dType == ptString and DFM_STRASLIST in pDfmOptions:
                    sList = propVal.split('\r\n')
                    if len(sList) > 0 and sList[-1] == '': 
                        sList = sList[0:-1]
                    dfmStream.writeStrList(sList)
                elif dType in [ptSmallint, ptInteger, ptByte] and DFM_ENUM in pDfmOptions:
                    enumValues = pDfmOptions[DFM_ENUM]
                    vEnum = enumValues[propVal]
                    dfmStream.writeIdentifier(vEnum)
                elif dType in [ptSmallint, ptInteger, ptByte] and MD_ENUM in propInfo:
                    enumValues = propInfo[MD_ENUM]
                    vEnum = enumValues[propVal]
                    dfmStream.writeIdentifier(vEnum)
                else:
                    dfmStream.writeValue(propVal)
                #--
            #--
            elif pKind == pkObject:
                objVal = self.getPropertyValue(obj, propInfo)
                if objVal != None:
                    oMapOption = pDfmOptions.get(DFM_OBJ, dmoSubObject)
                    if oMapOption == dmoFlat:
                        self.internalWriteAsDFM(dfmStream, obj, True, None, dfmPropertyName)
                    elif oMapOption == dmoSubObject:
                        self.dfmWriteSubObject(dfmStream, objVal)
                    #--
                #--
            elif pKind == pkObjectRef:
                objVal = self.getPropertyValue(obj, propInfo)
                if objVal != None:
                    objClassInfo = self.getClassMetadata(objVal)
                    objName = self.getObjectName(objVal, "The name of class %s instance is not defined" % objClassInfo[MD_CLASSNAME])
                    dfmStream.writePropertyName(dfmPropertyName)
                    dfmStream.writeIdentifier(objName)
                #--
            elif pKind == pkVector and dType == ptObjectVect:
                vect = self.getPropertyValue(obj, propInfo)
                vectOption = pDfmOptions.get(DFM_OBJVECTOR, dmcFlat)
                if vectOption == dmcCollection:
                    dfmStream.writeCollectionOpening(dfmPropertyName)
                n = len(vect); i = 0
                while i < n:
                    aObj = vect[i]
                    if aObj == None:
                        continue
                    #--
                    if vectOption == dmcCollection:
                        dfmStream.writeCollItemBegin()
                        self.internalWriteAsDFM(dfmStream, aObj)
                        dfmStream.writeCollItemEnd()
                    else:
                        self.dfmWriteSubObject(dfmStream, aObj)
                    #--
                    i += 1
                #-- while
                if vectOption == dmcCollection:
                    dfmStream.writeCollectionClosing()
                #--
            else:
                raise SObjectException("Unsupported property type. Property %s in instance of class %s" % (propInfo[MD_PROPERTYNAME], clsInfo[MD_CLASSNAME]))
            #-- else
        #-- for
        pass
    #-- def
      
    def writeAsDFM(self, outStream):
        dfmStream = sobjdfmtextwrapper(outStream)
        rootObjName = self.getObjectName(self.rootInstance, "Root instance does not have valid name")
        rootClassName = "T%s" % rootObjName
        self.dfmWriteSubObject(dfmStream, self.rootInstance, (rootObjName, rootClassName))
    #--
    
    def mergeDFMMetadata(aMetadata, aDfmMetadata):
        # aDfmMetadata is a dictionary with class names as keys
        # each entry is a tuple of two members
        # the first member contains class-level DFM options for the class
        # the second level contains dictionary with property names as keys and every entry of the dict is property-level DFM options for the class
        dfmClasses = list(aDfmMetadata.keys())
        for dfmClass in dfmClasses:
            dfmInfoEntry = aDfmMetadata[dfmClass]
            mdClass = aMetadata.get(dfmClass.lower(), None)
            if mdClass == None:
                raise SObjectException("Cannot find metadata for class %s" % dfmClass)
            dfmClassSetting = dfmInfoEntry[0]
            if dfmClassSetting != None:
                if MD_DFMOPTIONS not in mdClass:
                    mdClass[MD_DFMOPTIONS] = dfmClassSetting
                else:
                    extMD = mdClass[MD_DFMOPTIONS]
                    extMD.update(dfmClassSetting)
                #--
            #-- if dfmClassSetting != None
            mdProperties = mdClass[MD_ALLPROPERTIES]
            dfmPropSettings = dfmInfoEntry[1]
            if dfmPropSettings != None:
                dfmPropSettingsKeys = list(dfmPropSettings.keys())
                for propName in dfmPropSettingsKeys:
                    mdProperty = mdProperties.get(propName.lower(), None)
                    if mdProperty == None:
                        raise SObjectException("Cannot find property %s in class %s" % (propName, dfmClass))
                    if MD_DFMOPTIONS not in mdProperty:
                        mdProperty[MD_DFMOPTIONS] = dfmPropSettings[propName]
                    else:
                        extMD = mdProperty[MD_DFMOPTIONS]
                        extMD.update(dfmPropSettings[propName])
                    #--
                #-- for propName
            #-- if dfmPropSettings != None
        #-- for dfmClass
        pass
    #--
    mergeDFMMetadata = staticmethod(mergeDFMMetadata)
    
    def processDFMMDInheritances(metadata): # process inheritance of DFM metadata (after merged)
        # call this static method before using metadata
        for key in list(metadata.keys()):
            clsInfo = metadata[key]
            dAllDfmProperties = {}        
            currentClass = clsInfo
            clsInfos = []
            while currentClass != None:
                clsInfos.insert(0, currentClass)
                if MD_ANCESTOR in currentClass:
                    ancestorClsName = currentClass[MD_ANCESTOR]
                    ancestorClsInfo = metadata[ancestorClsName.lower()]
                    currentClass = ancestorClsInfo
                else:
                    currentClass = None
                #--
            #--
            n = len(clsInfos)
            for i in range(n):
                aInfo = clsInfos[i]
                cDfmOptions = aInfo.get(MD_DFMOPTIONS, None)
                if cDfmOptions == None:
                    continue
                cDfmProperties = cDfmOptions.get(DFM_PROPERTIES, None)
                if cDfmProperties == None:
                    continue
                for key in list(cDfmProperties.keys()):
                    dAllDfmProperties[key] = cDfmProperties[key]
            #--
            cDfmOptions = clsInfo.get(MD_DFMOPTIONS, None)
            
            if len(dAllDfmProperties) > 0:
                if cDfmOptions == None:
                    cDfmOptions = {}
                    clsInfo[MD_DFMOPTIONS] = cDfmOptions
                #-- if cDfmOptions
                cDfmOptions[DFM_PROPERTIES] = dAllDfmProperties
            #-- if len()
        #-- for
        pass
    #-- def
    processDFMMDInheritances = staticmethod(processDFMMDInheritances)
    
#-- class
  
  
class sobjtextwrapper(sobjstreamwrapper):
    def __init__(self, aStream):
        self.stream = aStream
        self.scanner = None
        self.token = None
        self.prev_token = None
        self.indentLevel = 0
        self.newLine = True
        
    def raiseInvalidToken(self, exp_token):
        t1 = TOKEN_NAMES[self.token['token_kind']]
        t2 = TOKEN_NAMES[exp_token]
        raise ParseException("Expected: %s found: %s" % (t2, t1))
    #--
    
    def testTraverseAllTokens(self):
        t0 = time.time()
        cnt = 0
        self.prepareLoad()
        while True:
            token = self.scanner.Scan()
            if token == None:
                break
            else:
                cnt += 1
        t1 = time.time()
        #print "%d token(s) scanned. total scan time: %f secs" % (cnt, t1 - t0)
      
    def checkToken(self, exp_token):
        if self.token['token_kind'] != exp_token:
            self.raiseInvalidToken(exp_token)
        result = self.token['token_value']
        self.scan()
        return result
    #--
    
    def isToken(self, exp_token):
        return self.token['token_kind'] == exp_token
        
    def scan(self):
        self.prev_token = self.token
        self.token = self.scanner.Scan()
        if self.token == None:
            self.token = {}
            self.token['token_kind'] = EOF_T
            self.token['token_value'] = None
        #-- put dummy token
    #--
    
    def p_heading(self):
        if not self.isToken(DOCUMENT_T):
            return None
        result = {'libGUID': "", 'docTypeGUID': "", 'docID': "", 'majorVersion': 0, 'minorVersion': 0}
        self.scan()
        self.checkToken(LT_T)
        self.checkToken(LIBVERSIONID_T)
        self.checkToken(EQUAL_T)
        result['libGUID'] = self.checkToken(STRCONST_T)
        while True:
            p_res = self.p_heading_set(result)
            if p_res == PR_NOSTARTER: break
        #--
        self.checkToken(GT_T)
        return result
    #--
    
    def p_heading_set(self, headRecord):
        if not self.isToken(IDENTIFIER_T):
            return PR_NOSTARTER
        or_setting_kw = self.token['token_value']
        setting_kw = or_setting_kw.lower()
        self.scan()
        self.checkToken(EQUAL_T)
        setting_str = self.checkToken(STRCONST_T)
        if setting_kw == "documenttypeid":
            headRecord['docTypeGUID'] = setting_str
        elif setting_kw == "documentid":
            headRecord['docID'] = setting_str
        elif setting_kw == "majorversion":
            try:
                headRecord['majorVersion'] = int(setting_str)
            except: pass
        elif setting_kw == "minorversion":
            try:
                headRecord['minorVersion'] = int(setting_str)
            except: pass
        else:
            raise SObjectException("Invalid header field: %s" % (or_setting_kw))
        #--
        return PR_OK
    #--
    
    def p_object_heading(self):
        if not self.isToken(OBJECT_T):
            return None
        self.scan()
        self.checkToken(COLON_T)
        type_name1 = self.checkToken(IDENTIFIER_T)
        type_name2 = ""
        if self.isToken(COLON_T):
            self.scan()
            self.checkToken(COLON_T)
            type_name2 = self.checkToken(IDENTIFIER_T)
        #--
        if type_name2 != "":
            type_name = type_name1 + "::" + type_name2
        else:
            type_name = type_name1
        #--
        return type_name
        
    def p_string_value(self):
        if self.isToken(STRCONST_T):
            result = self.token['token_value']
            self.scan()
        elif self.isToken(IDENTIFIER_T):
            result = self.token['token_value']
            self.scan()
        elif self.isToken(OPENBRACKET_T):
            self.scan()
            sList = []
            astr = self.checkToken(STRCONST_T)
            sList.append(astr)
            while True:
                if self.isToken(COMMA_T):
                    self.scan()
                    astr = self.checkToken(STRCONST_T)
                    sList.append(astr)
                else:
                    break
                #--
            #--
            self.checkToken(CLOSEBRACKET_T)
            result = "".join(sList)
        else:
            result = None
        return result
    #--
    
    def p_integer_value(self):
        if self.isToken(INTCONST_T):
            result = self.token['token_value']
            self.scan()
        else:
            result = None
        return result
    #--
    
    def p_float_value(self):
        if self.isToken(INTCONST_T):
            result = (self.token['token_value']) * 1.0
            self.scan()
        elif self.isToken(REALCONST_T):
            result = self.token['token_value']
            self.scan()
        else:
            result = None
        return result
    #--
    
    def p_boolean_value(self):
        if self.isToken(INTCONST_T):
            result = self.token['token_value'] != 0
            self.scan()
        elif self.isToken(BOOLCONST_T):
            result = self.token['token_value']
            self.scan()
        else:
            result = None
        return result
    #--
    
    def p_date_value(self):
        if not self.isToken(DATE_T):
            return None
          
        self.scan()
        self.checkToken(OPENPAR_T)
        sDate = self.checkToken(STRCONST_T)
        result = itimeutil.StrToDateTime(sDate)
        self.checkToken(CLOSEPAR_T)
        return result
    #--
    
    def p_reference_value(self):
        if self.isToken(DOLLAR_T):
            self.scan()
            ref_details = self.p_ref_details()
            if ref_details == None:
                raise SObjectException("Invalid reference")
            return ref_details
        elif self.isToken(ROOT_T):
            self.scan()
            return []
        else:
            return None
        #--
        pass
    #--
    
    def p_ref_details(self):
        if not self.isToken(IDENTIFIER_T):
            return None
           
        result = []
        while True:
            propertyName = self.checkToken(IDENTIFIER_T)
            if self.isToken(OPENBRACKET_T):
                self.scan()
                if self.isToken(STRCONST_T) or self.isToken(IDENTIFIER_T):
                    itemName = self.token['token_value']
                    self.scan()
                else:
                    raise SObjectException("Invalid reference")
                self.checkToken(CLOSEBRACKET_T)
            else:
                itemName = None
            result.append([propertyName, itemName])
            if not self.isToken(PERIOD_T):
                break
            else:
                self.scan()
            #--
        #-- while
        
        return result
    #--
       
    def increaseIndent(self):
        self.indentLevel += 1
      
    def decreaseIndent(self):
        self.indentLevel -= 1
      
    def writeIndent(self):
        self.stream.write(self.indentLevel * "  ")
        
    def write(self, aText):
        if self.newLine and aText != "":
            self.writeIndent()
        if aText != "":
            self.stream.write(aText)
            self.newLine = False
        #--
        pass
    #--
    
    def writeln(self, aText):
        if self.newLine and aText != "":
            self.writeIndent()
        aText += "\r\n"
        self.stream.write(aText)
        self.newLine = True
        #--
        pass
    #--
    
    def readHeading(self):
        heading = self.p_heading()
        if heading == None: raise SObjectException("No heading information")
        return heading
       
    def readPropertyName(self):
        if self.isToken(EXCLAMATION_T):
            prefix = "!"
            self.scan()
        else:
            prefix = ""
        bPropertyNameFound = False
        propertyName = ""
        separator = ''
        while self.isToken(IDENTIFIER_T):
            id = self.token['token_value']
            self.scan()
            if propertyName == "":
                propertyName = id
            else:
                propertyName += separator + id
            bPropertyNameFound = True
            if self.isToken(PERIOD_T) or self.isToken(ACURL_T):
                if self.isToken(PERIOD_T):
                    separator = '.'
                else:
                    separator = '@'
                self.scan()
                if not self.isToken(IDENTIFIER_T):
                    raise SObjectException("Invalid property name")
                #--
            #-- if
        #-- while
        if bPropertyNameFound:
            propertyName = prefix + propertyName
            self.checkToken(EQUAL_T)
            return propertyName
        else: 
            return None
        #--
        pass
    #--
        
    def readObjectInfo(self):
        if self.isToken(NIL_T):
            result = ""
            self.scan()
        else:
            result = self.p_object_heading()
            if result == None:
                raise SObjectException("Invalid object tag")
            self.checkToken(LT_T)
        #--
        return result
        
    def readObjectClosing(self):
        self.checkToken(GT_T)
    #--
  
    def readValue(self, propInfo):
        dType = propInfo[MD_DATATYPE]
        if dType == ptString:
            v = self.p_string_value()
        elif dType in [ptSmallint, ptInteger, ptByte]:
            v = self.p_integer_value()
        elif dType in [ptSingle, ptDouble, ptCurrency]:
            v = self.p_float_value()
        elif dType == ptBoolean:
            v = self.p_boolean_value()
        elif dType == ptDate:
            v = self.p_date_value()
        else:
            raise SObjectException("Invalid property type 0x%x" % dType)
        if v == None:
            raise SObjectException("Invalid property value")
        return v
    #--
    
    def readReference(self):
        if self.isToken(NIL_T):
            return None
        else:
            return self.p_reference_value()
        #--
        pass
    #--
  
    def readVectorInfo(self):
        self.checkToken(OPENCURL_T)
        return
    #--
    
    def readEndOfVector(self, bCheckSeparator):
        if self.isToken(CLOSECURL_T):
            self.scan()
            return True
        else:
            if bCheckSeparator and self.isToken(COMMA_T):
                self.scan()
            return False
        #--
        pass
    #-- def
  
    def writeHeading(self, versionRecord):
        self.writeln("document <")
        self.increaseIndent()
        self.write("libversionID="); self.writeln(pascalQuotedStr(LibGUID));
        self.write("DocumentTypeID="); self.writeln(pascalQuotedStr(versionRecord['docTypeGUID']))
        self.write("DocumentID="); self.writeln(pascalQuotedStr(versionRecord['docID']))
        self.write("MajorVersion="); self.writeln(pascalQuotedStr(str(versionRecord['majorVersion'])))
        self.write("MinorVersion="); self.writeln(pascalQuotedStr(str(versionRecord['minorVersion'])))
        self.decreaseIndent()
        self.writeln(">")
    #--
    
    def writePropertyName(self, propertyName):
        self.write(propertyName)
        self.write("=")
    #--
    
    def writeObjectInfo(self, objTypeName):
        if objTypeName == "":
            self.writeln("nil")
        else:
            self.writeln("object:%s<" % objTypeName)
            self.increaseIndent()
        #--
        
    def writeObjectClosing(self):
        self.decreaseIndent()
        self.writeln(">")
    #--
  
    def writeString(self, aString):
        i = 0; k = 0; n = 0; iRPosStart = 0; iRPosEnd = 0; iUPosStart = 0; iUPosEnd = 0;
        iActRPosStart = 0; iActRPosEnd = 0; iActUPosStart = 0; iActUPosEnd = 0;
        cc = ''; ucc = ''
        bAcquire = False; bReadingUnreadable = False; bAcquireLast = False
        sReadable = ""; sUnreadable = ""; sCurrent = ""
        strList = []
       
        def acquire():
            j = 0
            if iActRPosEnd >= iActRPosStart:
                sReadable = pascalQuotedStr(aString[iActRPosStart: iActRPosEnd + 1])
            else:
                sReadable = ""
            if iActUPosEnd >= iActUPosStart:
                sUnreadable = ""
                j = iActUPosStart
                while j <= iActUPosEnd:
                    ucc = aString[j];
                    sUnreadable = sUnreadable + '#' + str(ord(ucc))
                    j += 1
                # while
            else:
                sUnreadable = ""
            #--
            sCurrent = sReadable + sUnreadable
            strList.append(sCurrent)
            bAcquire = False
            pass
        #-- def acquire()
        
        n = len(aString)
    
        if n == 0:
            self.writeln(pascalQuotedStr(""))
            return
        #--
        i = 0
        iRPosStart = 0
        bReadingUnreadable = False
        bAcquire = False
        bAcquireLast = False
        while i < n:
            cc = aString[i]
            if not bReadingUnreadable and (ord(cc) < READABLE_CHARSET_START or ord(cc) > READABLE_CHARSET_END):
                bReadingUnreadable = True
                iRPosEnd = i - 1
                iUPosStart = i
            elif bReadingUnreadable and (ord(cc) >= READABLE_CHARSET_START and ord(cc) <= READABLE_CHARSET_END):
                bReadingUnreadable = False
                iUPosEnd = i - 1
                iActRPosStart = iRPosStart
                iActRPosEnd = iRPosEnd
                iActUPosStart = iUPosStart
                iActUPosEnd = iUPosEnd
                iRPosStart = i
                bAcquire = True
                if i == n - 1: bAcquireLast = True
            #-- elif
            
            if i == n - 1:
                if bReadingUnreadable:
                    iUPosEnd = n - 1
                else:
                    iRPosEnd = n - 1
                    iUPosStart = 0
                    iUPosEnd = -1
                #--
                if not bAcquireLast:
                    iActRPosStart = iRPosStart
                    iActRPosEnd = iRPosEnd
                    iActUPosStart = iUPosStart
                    iActUPosEnd = iUPosEnd
                #-- if
                bAcquire = True
            #--
            if bAcquire:
                acquire(); bAcquire = False
            i += 1
        #-- while
        
        if bAcquireLast:
            iActRPosStart = iRPosStart
            iActRPosEnd = iRPosEnd
            iActUPosStart = iUPosStart
            iActUPosEnd = iUPosEnd
            acquire(); bAcquire = False
        #--
    
        if len(strList) == 1:
            self.writeln(strList[0])
        else:
            n = len(strList)
            self.write("[")
            if n > 1:
                self.increaseIndent()
            for i in range(n): 
                self.write(strList[i])
                if i + 1 < n:
                    self.writeln(',')
            #-- for
            if n > 1:
                self.decreaseIndent()
            self.writeln("]")
        #--
        pass
    #-- def
        
    def writeValue(self, propInfo, value):
        dType = propInfo[MD_DATATYPE]
        if dType == ptString:
            self.writeString(value)
        elif dType in [ptSmallint, ptInteger, ptByte, ptSingle, ptDouble, ptCurrency]:
            s = str(value)
            self.writeln(s)
        elif dType == ptDate:
            s = "date('%d/%d/%d %d:%d:%d')" % (value[0], value[1], value[2], value[3], value[4], value[5])
            self.writeln(s)
        elif dType == ptBoolean:
            if value:
                s = "True"
            else:
                s = "False"
            self.writeln(s)
        else:
            raise SObjectException("Unsupported data type 0x%x" % dType)
        #--
        pass
    #--
        
    def writeReference(self, aObjRef):
        if aObjRef == None:
            self.writeln("nil")
        elif len(aObjRef) == 0:
            self.writeln("root")
        else:
            self.write("$")
            depth = len(aObjRef)
            for i in range(depth):
                ref = aObjRef[i]
                self.write(ref[0])
                if ref[1] != None:
                    self.write("[%s]" % pascalQuotedStr(ref[1]))
                if i + 1 < depth:
                    self.write(".")
                #--
            #-- for
            self.writeln("")
        #-- else
        pass
    #-- def
    
    def writeVectorInfo(self):
        self.writeln("{")
        self.increaseIndent()
    #--
    
    def writeVectorItemSeparator(self):
        self.write(",")
        
    def writeEndOfVector(self):
        self.decreaseIndent()
        self.writeln("}")
    
    def prepareLoad(self):
        self.scanner = sobjscanner(self.stream)
        self.scan()
    #--
    
    def prepareSave(self):
        self.newLine = True
    #--
  
    def readEnumeration(self):
        if self.isToken(IDENTIFIER_T):
            result = self.token['token_value']
            self.scan()
            return result
        elif self.isToken(INTCONST_T):
            result = str(self.token['token_value'])
            self.scan()
            return result
        else:
            raise SObjectException("invalid enumeration constant")
        #--
    #--
    
    def writeEnumeration(self, aEnumConst):
        self.writeln(aEnumConst)
    #--
  
    def getLocationInfo(self): 
        if self.scanner == None:
            return ""
        else:
            return "in line %d" % self.scanner.lineNum
        
    pass
#--

class sobjdfmtextwrapper:
    def __init__(self, aStream):
        self.stream = aStream
        self.indentLevel = 0
        self.newLine = True
        
    def increaseIndent(self):
        self.indentLevel += 1
      
    def decreaseIndent(self):
        self.indentLevel -= 1
        
    def writeIndent(self):
        self.stream.write(self.indentLevel * "  ")
        
    def write(self, aText):
        if self.newLine and aText != "":
            self.writeIndent()
        if aText != "":
            self.stream.write(aText)
            self.newLine = False
        #--
        pass
    #--
    
    def writeln(self, aText):
        if self.newLine and aText != "":
            self.writeIndent()
        aText += "\r\n"
        self.stream.write(aText)
        self.newLine = True
        #--
        pass
    #--
  
    def writePropertyName(self, dfmPropertyName):
        self.write("%s = " % dfmPropertyName)
    #--
    
    def writeIdentifier(self, id):
        self.writeln(id)
    #--
    
    def writeValue(self, value):
        if type(value) is str:
            self.writeln(pascalQuotedStr(value))
        else:
            self.writeln(str(value))
    #--
    
    def writeObjectOpening(self, objName, objClassName):
        self.writeln("object %s: %s" % (objName, objClassName))
        self.increaseIndent()
    #--
    
    def writeObjectClosing(self): 
        self.decreaseIndent()
        self.writeln("end")
    #--
    
    def writeStrList(self, sList):
        self.writeln("(")
        self.increaseIndent()
        n = len(sList)
        for i in range(n):
            self.writeln(pascalQuotedStr(sList[i]))
        self.decreaseIndent()
        self.writeln(")")
    #--
    
    def writeCollectionOpening(self, dfmPropertyName):
        self.writeln("%s = <" % dfmPropertyName)
        self.increaseIndent()
    #--
    
    def writeCollectionClosing(self):
        self.decreaseIndent()
        self.writeln(">")
        
    def writeCollItemBegin(self):
        self.writeln("item")
        self.increaseIndent()
        
    def writeCollItemEnd(self):
        self.decreaseIndent()
        self.writeln("end")
    #--
    
    pass
#-- class

def vectorLookup(obj, vectorName, namePropertyName, aName):
    aName = aName.lower()
    vector = obj.__dict__[vectorName]
    i = len(vector) - 1
    while i >= 0 and aName != vector[i].__dict__[namePropertyName].lower() != aName: i -= 1
    if i >= 0:
        return vector[i]
    else:
        return None
    #--
    pass
#--

buildCharMap()
