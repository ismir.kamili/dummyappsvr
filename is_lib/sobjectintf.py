# sobjectintf.py
# constants of sobject.py

'''
  sobject metadata description
  
  <metadata> = dictionary {
    (lc_class_name: string: <lower-case class name>, <class description>)*
  }
  <class description> = dictionary {
    MD_CLASSNAME: <class name>
    , MD_CLASSF: class_type(parentObject): <function to class which implement an object>
    , MD_PROPERTIES: <property descriptions>
    , MD_ALLPROPERTIES: <property descriptions> --> this member is automatically generated when calling processMDInheritances()
    [,MD_NAMEATTRNAME: string: <attribute name used as unique naming>]
    [,MD_CHKSTOREOBJF: method_pointer(self): <method pointer which returns boolean whether an object is stored>]
    [,MD_ANCESTOR: <string-class name>: <point to ancestor class name>]
    [,MD_DFMOPTIONS: <dfm class mapping descriptions>]
  }
  
  <dfm class mapping descriptions> = dictionary {
    [[,]DFM_ALIAS: <string> (class alias name)]
    [, DFM_PREPROCESS: None (run preprocess for instance of this class. The method shall be named 'dfmPreprocess(self)')]
    [, DFM_PROPERTIES: <property descriptions> (additional properties to be added during DFM conversion)]
    [, DFM_DELEGATEPROPS: <property delegation description>]
    [, DFM_CONDITIONALALIAS: None (call function named dfmGetClassAlias(self) to define alias class name]
  }
  
  <property delegation description> = dictionary {
    DFM_DPOBJNAME: <string> (name of object to delegate properties),
    , DFM_DPCLSNAME: <string> (name of class to delegate properties)
    , DFM_DPPROPNAMES: <list of property names> (name of properties (lower case) to delegate)
  }
  
  <property descriptions> = dictionary {
    (lc_property_name: string: <lower-case property name>, <property description>)*
  }
  
  <property description> = dictionary {
    MD_PROPERTYNAME: <property name>
    , MD_DATATYPE: <data type, see pt* constants below>
    [, MD_CHKSTOREPROPF: method_pointer(self): <method pointer which returns boolean whether an object is stored>]
    [, MD_READERF: method_pointer(self): <method pointer getting value for the property>]
    [, MD_WRITERF: method_pointer(self, value): <method pointer setting value for the property>]
    [, MD_ADDERF: method_pointer(self, value): <method pointer add value to collection>]
    [, MD_RESOLVERF: method_pointer(self, name): <method pointer resolver value for vector property>]
    [, MD_WRITEMODE: integer (wmNormal, wmAlwaysWrite, wmNeverWrite)]
    [, MD_NOADDRESS: anytype: <when exists, no address will be generated for this property]
    [, MD_ENUM: list of keywords: list of enumeration constants (0-based) for integer property]
    [, MD_DFMOPTIONS: <dfm mapping options>]
  }
  
  <dfm mapping descriptions> = dictionary {
    [[,]DFM_ALIAS: <string> (property alias name)]
    [, DFM_OBJ: dmoSubObject | dmoFlat]
    [, DFM_OBJVECTOR: dmcFlat | dmcCollection]
    [, DFM_STRASIDENTIFIER: None (specify if this string property shall be written as identifier (without quote)]
    [, DFM_ENUM: <string list> (enumeration of DFM)]
    [, DFM_NOWRITE: None (dismiss this property when converting to DFM)]
    [, DFM_STRASLIST: None (specify if this string property shall be written as string list)
  }
  
  linking object to metadata: each object to be involved shall have the property 'soClassName' to link itself to the metadata system
  container object required fields:
    * soDocTypeGUID field (string)
    * soDocID field (string)
    * soMajorVersion field (integer)
    * soMinorVersion field (integer)
'''

## SObject GUID

LibGUID = '{34FC4F41-D74A-11D5-AB5B-FB56D633B52C}'

## SObject metadata keywords

MD_CLASSNAME = 'className'
MD_CLASSF = 'classF'
MD_PROPERTIES = 'properties'
MD_ALLPROPERTIES = 'allProperties'
MD_ALLPROPERTIES_BASIC_INFO = 'allPropertiesBasicInfo'
MD_NAMEATTRNAME = 'nameAttr'
MD_CHKSTOREOBJF = 'storeObjectCheckF'
MD_ANCESTOR = 'ancestor'
MD_PROPERTYNAME = 'propName'
MD_DATATYPE = 'dataType'
MD_CHKSTOREPROPF = 'storePropCheckF'
MD_READERF = 'readerF'
MD_WRITERF = 'writerF'
MD_WRITEMODE = 'writeMode'
MD_ADDERF = 'adderF'
MD_RESOLVERF = 'resolverF'
MD_ENUM = 'enum'
MD_NOADDRESS = 'noAddress'
MD_DFMOPTIONS = 'dfmOptions'

# new, extended tags (added by IK Oct 4 2018)
MD_IDX_DICT_PROP = 'idxDictProp'

DFM_PREPROCESS = 'preprocess'
DFM_PROPERTIES = 'dfmProperties'
DFM_ALIAS = 'alias'
DFM_OBJVECTOR = 'objVector'
DFM_STRASIDENTIFIER = 'strAsId'
DFM_STRASLIST = 'strAsList'
DFM_ENUM = 'enum'
DFM_OBJ = 'obj'
DFM_NOWRITE = 'noWrite'
DFM_DELEGATEPROPS = 'delegate'
DFM_DPOBJNAME = 'objName'
DFM_DPCLSNAME = 'objClass'
DFM_DPPROPNAMES = 'delPropNames'
DFM_CONDITIONALALIAS = 'condAlias'

dmcFlat = 0
dmcCollection = 1
dmoSubObject = 0
dmoFlat = 1

wmNormal = 0
wmAlwaysWrite = 1
wmNeverWrite = 2

cstNormal = 0
cstPreparingRef = 1
cstWriting = 2
cstReading = 3
cstResolving = 4


## SObject property type names
ptInvalidType     = 0x0000
ptSmallint        = 0x0002
ptInteger         = 0x0003
ptSingle          = 0x0004
ptDouble          = 0x0005
ptCurrency        = 0x0006
ptDate            = 0x0007
ptBoolean         = 0x0008
ptByte            = 0x0009
ptString          = 0x000A
ptObject          = 0x000B
ptObjectVect      = ptObject | 0x0100;
ptObjectRef       = 0x000C;
ptObjectRefVect   = ptObjectRef | 0x0100;

## SObject property kind name and their mapping
pkOrdinary        = 0
pkObject          = 1
pkObjectRef       = 2
pkVector          = 3

PROP_TYPEKIND_MAP = {
  ptSmallint        : pkOrdinary,
  ptInteger         : pkOrdinary,
  ptSingle          : pkOrdinary,
  ptDouble          : pkOrdinary,
  ptCurrency        : pkOrdinary,
  ptDate            : pkOrdinary,
  ptBoolean         : pkOrdinary,
  ptByte            : pkOrdinary,
  ptString          : pkOrdinary,
  ptObject          : pkObject,
  ptObjectVect      : pkVector,
  ptObjectRef       : pkObjectRef,
  ptObjectRefVect   : pkVector
}

##

