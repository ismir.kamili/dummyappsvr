class DataStream:
  # raw prototype of stream object
  def readChar(self):
    return chr(0)
  #--
  
  def getPosition(self):
    return 0
  #--
  
  def write(self, sValue):
    return 0
#--

class FileStream:
  def __init__(self, fileName, fileMode = "r"):
    self.fHandle = None
    self.fHandle = open(fileName, fileMode)
  #--
  
  def __del__(self):
    if self.fHandle:
      self.fHandle.close()
  
  def readChar(self):
    cc = self.fHandle.read(1)
    if cc == '':
      return None
    else:
      return cc
  #--
  
  def getPosition(self):
    return self.fHandle.tell()
  #--
  
  def write(self, sValue):
    return self.fHandle.write(sValue)
#--

class StringStream:
  def __init__(self, initialString = None):
    self.position = 0
    self.data = initialString if type(initialString) is str else ''
  #--
  
  def __del__(self):
    pass
  
  def readChar(self):
    if self.position < len(self.data):
      cc = self.data[self.position]
      self.position += 1
    else:
      cc = None
    return cc
  #--      
  
  def getPosition(self):
    return self.position
  #--
  
  def write(self, sValue):
    self.data += sValue
#--

class StrFileStream:
  def __init__(self, fileName, fileMode = "r"):
    fHandle = open(fileName, fileMode)
    try:
      s = ""
      while True:
        sTaken = fHandle.read()
        s = s + sTaken.decode(encoding='utf-8', errors='ignore')
        if sTaken == b"":
          break
      self.size = len(s)
      self.data = s
      self.iPos = 0
    finally:
      fHandle.close()
  #--
  
  def readChar(self):
    if self.iPos < self.size:
      cc = self.data[self.iPos]
      self.iPos += 1
      return cc
    else:
      return None
  #--
  
  def getPosition(self):
    return 0
  #--
  
  def write(self, sValue):
    raise Exception("write operation is not supported")
#--

