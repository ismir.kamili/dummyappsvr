import pickle as pickle
import sys
from . import odlsys
from . import sobject
import weakref

def loadPMD(pmdFile):
    f = open(pmdFile, 'rb')
    result = pickle.load(f)
    return result

def loadDMD(dmdFile):
    import dill
    f = open(dmdFile, 'rb')
    result = dill.load(f)
    return result

def loadFromFile(loader, pmdFile):
    return loadPMD(pmdFile)

def createHelper(odlObj):
    return sobject.sobjhelper(2, odlsys.odlMetadata, odlObj)


