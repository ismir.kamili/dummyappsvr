'''com.ihsan.timeutils
'''

import time
import datetime

def StrToDateTime(s, datetype=0):
    if (s == None) or (s.strip() == ''):
        return None
    
    s = s.strip()
    s = s.split(' ')
    strdate = s[0]
    pdate = strdate.split('/')
    if datetype == 0:
        month = int(pdate[0])
        day   = int(pdate[1])
    elif datetype == 1:
        month = int(pdate[1])
        day   = int(pdate[0])
    else:
        raise Exception('unknown datetype')
    
    if len(s) > 1:
        ptime = s[1].split(':')
        hour  = int(ptime[0]); minute = int(ptime[1]); second = int(ptime[2])
    else:
        hour = 0; minute = 0; second = 0
        
    pdatetime = int(pdate[2]), month, day, hour, minute, second, 0, 0, 0
    d = time.localtime(time.mktime(pdatetime))
    return d

def StrToDate(s, datetype=0):
    if (s == None) or (s.strip() == ''):
        return None
    
    s = s.strip()
    pdate = s.split('/')
    if datetype == 0:
        month = int(pdate[0])
        day   = int(pdate[1])
    elif datetype == 1:
        month = int(pdate[1])
        day   = int(pdate[0])
    else:
        raise Exception('unknown datetype')
            
    pdate = int(pdate[2]), month, day, 0, 0, 0, 0, 0, 0
    d = time.localtime(time.mktime(pdate))
    return d

def StrToTime(s):
    if (s == None) or (s.strip() == ''):
        return None
    
    s = s.strip()
    ptime = s.split(':')
    ptime = 0, 0, 0, ptime[0], ptime[1], ptime[2], 0, 0, 0
    d = time.localtime(time.mktime(ptime))
    return d
    
def DateTimeToStr(d, datetype=0):
    if d == None:
        return ''
    
    if datetype == 0:
        d1 = d[1]
        d2 = d[2]
    elif datetype == 1:
        d1 = d[2]
        d2 = d[1]
    else:
        raise Exception('unknown datetype')
    
    sdatetime = '%d/%d/%d %d:%d:%d' % (d1, d2, d[0], d[3], d[4], d[5])
    return sdatetime

def DateToStr(d, datetype=0):
    if d == None:
        return ''
    
    if datetype == 0:
        d1 = d[1]
        d2 = d[2]
    elif datetype == 1:
        d1 = d[2]
        d2 = d[1]
    else:
        raise Exception('unknown datetype')
    
    sdate = '%d/%d/%d' % (d1, d2, d[0])
    return sdate

def TimeToStr(d):
    if d == None:
        return ''
    
    stime = '%d:%d:%d' % (d[3], d[4], d[5])
    return stime

def Today():
    d = time.localtime()
    return d

def GetJulianDate():
    julianDate = Today()[7]
    
    return zfill(julianDate, 3)
    
def AsTDateTime(config, d):
    if d == None:
        return None

    lib = config.ModLibUtils
    d = lib.EncodeDate(d[0], d[1], d[2])
    return d

def TimeAsTDateTime(config, d):
    if d == None:
        return None

    lib = config.ModLibUtils
    d = lib.EncodeTime(d[3], d[4], d[5], 0)
    return d

def DateAsTDateTime(config, d):
    if d == None:
        return None

    lib = config.ModLibUtils
    d = lib.EncodeDate(d[0], d[1], d[2])
    return d

def TDateTimeAsTuple(config, d):
    lib = config.ModLibUtils
    y, m, dd = lib.DecodeDate(d)
    h, n, s, ms = lib.DecodeTime(d)
    t = y, m, dd, h, n, s, 0, 0, 0
    s = time.mktime(t)
    t = time.localtime(s)

    return t
    
def stdDate(config, aDate):
  datelib = config.ModDateTime
  if aDate is None:
    return None
  elif type(aDate) is list or type(aDate) is tuple:
    if len(aDate) >= 6:
      return datelib.EncodeDate(aDate[0], aDate[1], aDate[2]) + datelib.EncodeTime(aDate[3], aDate[4], aDate[5], 0)
    else:
      return datelib.EncodeDate(aDate[0], aDate[1], aDate[2])
  elif type(aDate) == time.struct_time:
    return datelib.EncodeDate(aDate.tm_year, aDate.tm_mon, aDate.tm_mday) + datelib.EncodeTime(aDate.tm_hour, aDate.tm_min, aDate.tm_sec)
  elif type(aDate) == datetime.datetime: # result from database query in unix / os X 
    return time.mktime(aDate.timetuple()) / (24 * 3600) + 25569 
  else:
    return aDate
  #--
#--

def parseStandardDateStr(config, sDate): # assuming yyyy-mm-dd[ hh:nn[:ss]] format
  
  def throwInvalidFormat():
    raise Exception("Invalid date format")
    
  def getInteger(p1, p2):
    try:
      s = sDate[p1:p2]
      i = int(s)
    except:
      throwInvalidFormat()
    #--
    return i
  #--
  
  def chkDSep(p):
    if sDate[p] not in ['-', '/']: throwInvalidFormat()
    
  def chkTSep(p):
    if sDate[p] != ':' : throwInvalidFormat()
  
    
  datelib = config.ModDateTime
  sDate = sDate.strip()
  yy = getInteger(0, 4); chkDSep(4)
  mm = getInteger(5, 7); chkDSep(7)
  dd = getInteger(8, 10);
  if len(sDate) > 10:
    if sDate[10] != ' ': throwInvalidFormat()
    hh = getInteger(11, 13)
    chkTSep(13)
    nn = getInteger(14, 16)
    if len(sDate) > 16:
      chkTSep(16)
      ss = getInteger(17, 19)
    else:
      ss = 0
    timeComponent = datelib.EncodeTime(hh, nn, ss, 0)
  else:
    timeComponent = 0.0
  return datelib.EncodeDate(yy, mm, dd) + timeComponent
#--

def parseShortMonthDate(config, sDate): # assuming dd-MMM-yyyy[ hh:nn[:ss]] format
  
  monthNames = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
  
  def throwInvalidFormat():
    raise Exception("Invalid date format")
    
  def getInteger(p1, p2):
    try:
      s = sDate[p1:p2]
      i = int(s)
    except:
      throwInvalidFormat()
    #--
    return i
  #--
  
  def chkDSep(p):
    if sDate[p] not in ['-']: throwInvalidFormat()
    
  def chkTSep(p):
    if sDate[p] != ':' : throwInvalidFormat()
  
    
  datelib = config.ModDateTime
  sDate = sDate.strip()
  dd = getInteger(0, 2); chkDSep(2)
  smm = sDate[3:6]; chkDSep(6)
  i = monthNames.index(smm.lower())
  if i < 0: raise Exception("Invalid month name")
  mm = i + 1
  yy = getInteger(7, 11)
  if len(sDate) > 11:
    if sDate[11] != ' ': throwInvalidFormat()
    hh = getInteger(12, 14)
    chkTSep(14)
    nn = getInteger(15, 17)
    if len(sDate) > 17:
      chkTSep(17)
      ss = getInteger(18, 20)
    else:
      ss = 0
    timeComponent = datelib.EncodeTime(hh, nn, ss, 0)
  else:
    timeComponent = 0.0
  return datelib.EncodeDate(yy, mm, dd) + timeComponent

    
def parseSwitchDateStr(config, sDate): # assuming yyyymmddhhnnss format
  mlu = config.ModLibUtils
  try:
    yy = int(sDate[0:4])
    mm = int(sDate[4:6])
    dd = int(sDate[6:8])
    hh = int(sDate[8:10])
    nn = int(sDate[10:12])
    ss = int(sDate[12:14])
    return mlu.EncodeDate(yy, mm, dd) + mlu.EncodeTime(hh, nn, ss, 0)
  except:
    raise Exception('Invalid date string %s' % sDate)

    

