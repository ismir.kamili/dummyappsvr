import os
import os.path as ospath
import sys
from . import sobject
from . import odlformula as odf
import ast
from .sobjectintf import *
from . import stream
import copy

TYPEID_INTEGER   = 0
TYPEID_SINGLE    = 1
TYPEID_DOUBLE    = 2
TYPEID_TIMESTAMP = 3
TYPEID_VARCHAR   = 4

fsNormal = 0
fsClassifier = 1
fsAncestorKey = 2
fsAutoLink = 3

class odlBase:
    SO_CLASSNAME = "undefined"
    def __init__(self, parentObject):
        self.metadata = odlMetadata
        self.soClassName = self.SO_CLASSNAME.lower()
        sobject.sobjhelper.initializeInstance(odlMetadata, self.SO_CLASSNAME, self)
        self.parentObject = parentObject
    #--
#--

class ODL(odlBase): 
    SO_CLASSNAME = "ODL"

    def __init__(self):
        odlBase.__init__(self, None)

        # persistent properties
        self.ODLVersion = 0
        self.ODLName = ""
        self.Refs = []
        self.TypeDecls = []

        # non-persistent properties
        self.idxTypes = {} # index by name (c.i)
        self.classes = []
        self.idxClasses = {} # index by name (c.i)
        self.idxClassesTblName = {} # index by table implementation name (c.i)
        self.enums = []
        self.idxEnums = {} # index by name (c.i)

        self.soDocTypeGUID = "{21DE026E-88A6-49ED-87F0-C759D5C018E5}"
        self.soDocID = "docID"
        self.soMajorVersion = 2
        self.soMinorVersion = 0
        self.actualReferences = []
        self.idxActualReferences = {} # index by name (c.i)
        self.primaryODL = None
        self.nameSpace = ''
        self.dbNameSpace = ''
        self.fileName = ''
        self.asReference = False
        self.odlLoader = fileODLLoader() # default loader

        aActRef = ODLActualReference(self, None)
        self.addActualReference(aActRef)
        self.formulas = []
        self.idxFormulas = {} # index by (className.lower(), fieldName.lower())

    def loadFromFile(self, fileName):
        aStream = stream.FileStream(fileName)
        aWrapper = sobject.sobjtextwrapper(aStream)
        sobj = sobject.sobjhelper(sobject.SOBJHELPER_READ, odlMetadata, self, aWrapper)
        sobj.loadInstance()
    #--

    def loadFromString(self, sData):
        aStream = stream.StringStream(sData)
        aWrapper = sobject.sobjtextwrapper(aStream)
        sobj = sobject.sobjhelper(sobject.SOBJHELPER_READ, odlMetadata, self, aWrapper)
        sobj.loadInstance()
    #--

    def getPrimaryODL(self):
        if self.primaryODL == None:
            return self
        else:
            return self.primaryODL

    def setPrimaryODL(self, aODL):
        self.primaryODL = aODL

    def beforeResolveAddress(self):
        for ref in self.Refs:
            ref.loadNameSpace()
    #--

    def addTypeDecl(self, aTypeDecl):
        self.TypeDecls.append(aTypeDecl)
        self.idxTypes[aTypeDecl.TypeName.lower()] = aTypeDecl
        if isinstance(aTypeDecl, ClassTypeDecl):
            self.classes.append(aTypeDecl)
            self.idxClasses[aTypeDecl.TypeName.lower()] = aTypeDecl
            self.idxClassesTblName[aTypeDecl.PTableName.lower()] = aTypeDecl
        elif isinstance(aTypeDecl, EnumTypeDecl):
            self.enums.append(aTypeDecl)
            self.idxEnums[aTypeDecl.TypeName.lower()] = aTypeDecl
        #--
    #--

    def getTypeDeclByName(self, aName):
        return self.idxTypes.get(aName.lower(), None)

    def addActualReference(self, aActRef):
        self.actualReferences.append(aActRef)
        self.idxActualReferences[aActRef.nameSpace.lower()] = aActRef

    def validate(self): pass

    def createActualRef(self, aRef):
        aPrimaryODL = self.getPrimaryODL()
        aActRef = aPrimaryODL.idxActualReferences.get(aRef.NameSpace.lower(), None)
        if aActRef == None:
            aActRef = ODLActualReference(aPrimaryODL, aRef)
            aPrimaryODL.addActualReference(aActRef)
        else:
            fileName = aRef.getAbsoluteFileName()
            if aActRef.fileName.lower() != fileName.lower():
                raise Exception('File name is inconsistent within the same metadata namespace %s\r\n%s and %s' % (
                    aActRef.nameSpace, aActRef.fileName, fileName))
            if aActRef.dbNameSpace.lower() != aRef.DBNameSpace:
                raise Exception('DB namespace is inconsistent within the same metadata namespace %s\r\n%s and %s' % (
                    aActRef.nameSpace, aActRef.dbNameSpace, aRef.DBNameSpace))
            #            if aActRef.loaded:
            #                raise Exception, 'Circular reference to metadata namespace %s' % (aActRef.nameSpace)
        #--
        return aActRef
    #--

    def setFileName(self, aFileName):
        oldFileName = self.fileName
        self.fileName = aFileName
        self.actualReferences[0].fileName = aFileName

        inFileNames = []
        outFileNames = []
        n = len(self.Refs)
        if n > 0:
            for i in range(n):
                aRef = self.Refs[i]
                inFileNames.append(aRef.RelPath)
            #--
            primaryODL = self.getPrimaryODL()
            primaryODL.odlLoader.adjustRefODLRelPath(oldFileName, aFileName, inFileNames, outFileNames)

            n = len(outFileNames)
            for i in range(n):
                aRef = self.Refs[i]
                aRef.RelPath = outFileNames[i]
            #--
        #--
    #--

    def applyNameSpaceAndExternalRefs(self):
        n = len(self.TypeDecls)
        for i in range(n):
            aTypeDecl = self.TypeDecls[i]
            if not aTypeDecl.isExternalReference:
                aTypeDecl.isExternalReference = True
                aTypeDecl.nameSpace = self.nameSpace
                aTypeDecl.TypeName = self.nameSpace + '.' + aTypeDecl.TypeName
                if isinstance(aTypeDecl, ClassTypeDecl):
                    aTypeDecl.PTableName = self.dbNameSpace + '.' + aTypeDecl.PTableName
                #--
            #--
        #--
    #--

    def importTypesFromExternalRefs(self, aRef):
        odl = aRef.actualReference.odl
        n = len(odl.TypeDecls)
        for i in range(n):
            aTypeDecl = odl.TypeDecls[i]
            if aRef.NameSpace == aTypeDecl.nameSpace:
                self.addTypeDecl(aTypeDecl)
        #--
    #--
    
    def createPhysicalDB(self):
        # import rpdb2; rpdb2.start_embedded_debugger('001')
        self.PhysicalDB.reset()
        for i, AClass in enumerate(self.classes):
            ATableName = AClass.PTableName
            ATable = self.PhysicalDB.idxTables.get(ATableName.lower(), None)
            if ATable is None:
                ATable = ODLTable(self.PhysicalDB)
                ATable.TableName = ATableName
                self.PhysicalDB.addTable(ATable)
            for j, AFieldMember in enumerate(AClass.fieldMembers):
                aField = ATable.idxFields.get(AFieldMember.PhysicalName.lower(), None)
                if AFieldMember.IsPrimaryKey:
                    if aField is None: # for primary key, only add when unique (new shared table cannot add the same primary key)
                        aField = ODLField(ATable)
                        aField.TypeID = AFieldMember.DataType.DataTypeID
                        aField.DataFormat = AFieldMember.DataFormat
                        aField.Length = AFieldMember.DataLength
                        aField.IsPrimaryKey = AFieldMember.IsPrimaryKey
                        aField.FieldName = AFieldMember.PhysicalName
                        aField.KeyOrder = AFieldMember.KeyOrder

                        ATable.addField(aField);
                else:
                    if aField is not None: # a field with the same name already exists
                        if (aField.TypeID != AFieldMember.DataType.DataTypeID) or (aField.Length != AFieldMember.DataLength):
                            name_retry = 1
                            while True:
                                name_candidate = AFieldMember.MemberName + str(name_retry)
                                name_unique = ATable.idxFields.get(name_candidate.lower(), None) is None
                                if not name_unique: 
                                    name_retry += 1
                                else:
                                    break
                            AFieldMember.PhysicalName = name_candidate
                        else:
                            name_unique = False
                    if aField is None or name_unique:
                        aField = ODLField(ATable)
                        if AFieldMember.DataType is None:
                            print('Warning: field %s in %s has no datatype' % (AFieldMember.MemberName, AFieldMember.owner.TypeName))
                        aField.TypeID = AFieldMember.DataType.DataTypeID
                        aField.DataFormat = AFieldMember.DataFormat
                        aField.Length = AFieldMember.DataLength
                        aField.IsPrimaryKey = AFieldMember.IsPrimaryKey
                        aField.FieldName = AFieldMember.PhysicalName
                        ATable.addField(aField)
                    #--
                    pass
                #--
                pass
            #-- for j
            pass
        #-- for i        
            #--

    def addFieldFormula(self, className, fieldName, sExpression):
        oClass = self.idxClasses.get(className.lower())
        if oClass == None:
            raise Exception('No class named %s' % className)
        oField = oClass.idxMembers.get(fieldName.lower())
        if oField == None or not isinstance(oField, FieldMember):
            raise Exception('No field named %s in class %s' % (fieldName, className))
        frm = FieldFormula(oField)
        frm.parse(sExpression)
        frm.validateDependency()
        if oField.formula != None:
            self.formulas.remove(oField.formula)
        oField.setFormula(frm)
        self.formulas.append(frm)
        self.idxFormulas[(className.lower(), fieldName.lower())] = frm

        pass


class odlLoader:
    def loadODL(self, AODL, fileName): pass
    def resolveRefODLFileName(self, fileName, relPath): pass
    def obtainRefODLRelPath(self, fileName, refODLAbsPath): pass
    def adjustRefODLRelPath(self, oldHostFileName, newHostFileName, oldRelPaths, newRelPaths): pass
    def loadODLFromInitStr(self, AODL, aInitStr): pass

class fileODLLoader:
    def loadODL(self, AODL, fileName):
        AODL.odlLoader = self # override current loader
        AODL.setFileName(fileName)
        AODL.loadFromFile(fileName)
    #--

    def resolveRefODLFileName(self, fileName, relPath):
        dirName = ospath.dirname(fileName) or '.'
        # retVal = ospath.normpath(dirName + os.sep + relPath)
        # print('Resolve ODL file name (%s, %s) = %s' % (fileName, relPath, retVal))
        return ospath.normpath(dirName + os.sep + relPath)

    def obtainRefODLRelPath(self, fileName, refODLAbsPath):
        return ospath.relpath(refODLAbsPath, ospath.dirname(fileName))

    def adjustRefODLRelPath(self, oldHostFileName, newHostFileName, oldRelPaths, newRelPaths): 
        n = len(oldRelPaths)
        for i in range(n):
            OldRefFileName = ospath.normpath(ospath.dirname(oldHostFileName) + os.sep + oldRelPaths[i])
            newRelPaths.append(ospath.relpath(OldRefFileName, ospath.dirname(newHostFileName)))
        #--
    #--

    def loadODLFromInitStr(self, AODL, aInitStr): 
        AODL.loadFromString(aInitStr)
    #--


class ODLActualReference:
    def __init__(self, aOwner, aReference):
        # aOwner: ODL, aReference: ODLReference
        self.owner = aOwner
        self.isPrimary = aReference == None
        if self.isPrimary:
            self.nameSpace = ""
            self.dbNameSpace = ""
            self.fileName = aOwner.fileName
        else:
            self.nameSpace = aReference.NameSpace
            self.dbNameSpace = aReference.DBNameSpace
            self.fileName = aReference.getAbsoluteFileName()
        #--
        self.odl = None
        self.loaded = False
    #--

    def load(self):
        self.loaded = False
        self.odl = ODL()
        self.odl.primaryODL = self.owner
        self.odl.setFileName(self.fileName)
        self.owner.odlLoader.loadODL(self.odl, self.fileName)
        self.odl.nameSpace = self.nameSpace
        self.odl.dbNameSpace = self.dbNameSpace
        self.odl.applyNameSpaceAndExternalRefs()
        self.loaded = True
    #--
#--  


class ODLReference(odlBase): 
    SO_CLASSNAME = "ODLReference"

    def __init__(self, odl):
        odlBase.__init__(self, odl)
        self.actualReference = None
        self.owner = odl
        self.absPath = ''

        # persistent properties
        self.RelPath = ''
        self.NameSpace = ''
        self.DBNameSpace = ''
        #--

    def loadNameSpace(self):
        if self.actualReference == None:
            aActRef = self.owner.createActualRef(self)
            if not aActRef.loaded:
                aActRef.load()
            self.actualReference = aActRef
            self.owner.importTypesFromExternalRefs(self)
        #--
    #--

    def getAbsoluteFileName(self):
        result = self.owner.getPrimaryODL().odlLoader.resolveRefODLFileName(self.owner.fileName, self.RelPath)
        self.absPath = result
        return result
    #--

    def setAbsoluteFileName(self, newAbsFileName):
        self.RelPath = self.owner.getPrimaryODL().odlLoader.obtainRefODLRelPath(self.owner.fileName, newAbsFileName)
        self.absPath = newAbsFileName
    #--



class TypeDecl(odlBase): 
    SO_CLASSNAME = "TypeDecl"

    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.owner = parentObject
        self.nameSpace = ''
        self.isExternalReference = False
    #--

class BasicTypeDecl(TypeDecl): SO_CLASSNAME = "BasicTypeDecl"
class RecordTypeDecl(TypeDecl): SO_CLASSNAME = "RecordTypeDecl"
class ListTypeDecl(TypeDecl): SO_CLASSNAME = "ListTypeDecl"
class RecordMember(odlBase): SO_CLASSNAME = "RecordMember"
class EnumTypeDecl(TypeDecl): 
    SO_CLASSNAME = "EnumTypeDecl"
    
    def getEnumeratorLength(self):
        if self.EnumType == 0:
            return 0
        if len(self.EnumMembers) == 0:
            return 1
        else:
            return len(max(self.EnumMembers, lambda x: len(x.StringValue)).StringValue)
    
class EnumMember(odlBase): SO_CLASSNAME = "EnumMember"

class MemberImpl: # member implementation information
    def __init__(self, ownerClass, classImpl, classMember, keyInh = None): # ownerClass is ClassTypeDecl object
        self.owner = ownerClass
        self.member = classMember
        self.classImpl = classImpl
        self.oriName = classMember.MemberName
        self.name = self.oriName.lower()
        self.tableName = classImpl.tableName
        self.tableIndex = classImpl.tableIndex
        self.oriCnName = '%s.%s' % (classMember.owner.TypeName, classMember.MemberName) 
        self.cnName = self.oriCnName.lower()
        self.fieldIndex = -1
        self.keyInh = keyInh
        self.ancKeyImpl = None
        self.keyIndex = 0
        if keyInh is not None:
            AInh = keyInh.parentObject
            descendant = AInh.descendantClass
            descKey = keyInh.ThisPKey
            descKeyCnName = '%s.%s' % (descendant.TypeName.lower(), descKey.MemberName.lower())
            descImpl = ownerClass.idxMemberImpls.get(descKeyCnName, None)
            if descImpl is None:
                raise Exception('Internal error: descendant key implementation (%s) not found' % descKeyCnName)
            descImpl.ancKeyImpl = self
            self.lowestDescKeyImpl = descImpl.lowestDescKeyImpl or descImpl # this will ensure we got the lowest descendant key
            self.descKeyImpl = descImpl
        else:
            self.lowestDescKeyImpl = None
            self.descKeyImpl = None

class FieldFormula:
    def __init__(self, ownerField):
        self.ownerField = ownerField
        self.cg = None
        self.rawDependencies = None
        self.dependecies = []

    def parse(self, sExpression):
        m = ast.parse(sExpression)
        self.cg = odf.CodeGenerator()
        self.cg.genCode(m)
        self.rawDependencies = copy.copy(self.cg.varRefs)

    def validateDependency(self):
        dpd = self.rawDependencies
        for vd in dpd:
            line_no = vd.node.lineno
            col_offset = vd.node.col_offset
            refText = vd.reference

            if vd.context == '': # direct dependency 
                currClass = self.ownerField.owner
                if refText.find('obj.') != 0:
                    raise Exception("dependency must start with 'obj.'")
                refText = refText[len('obj.'):]
                walkPath = [('self', 'self')]
            else:
                listMember = currClass.idxMembers.get(vd.context)
                if listMember == None or not isinstance(listMember, ListMember):
                    raise Exception('Unknown list member %s (line %d, col %d)').with_traceback((vd.context, line_no, col_offset))
                if refText.find('member.') == 0:
                    currClass = listMember.UsedLink.owner
                    refText = refText[len('member.'):]
                    walkPath = [('link', listMember.UsedLink.MemberName)]
                elif refText.find('obj.') == 0:
                    curClass = self.ownerField.owner
                    refText = refText[len('obj.'):]
                    walkPath = [('self', 'self')]
                else:
                        raise Exception("dependency must start either with 'member.' or 'obj.'")

            tokens = refText.split('.')
            finalMember = None # final member must be field
            for tok in tokens:
                if currClass == None:
                    raise Exception('Invalid reference %s in obj.%s (line %d, col %d)' % (tok, refText, line_no, col_offset)) 
                member = currClass.idxMembers.get(tok)
                if member == None:
                    raise Exception('Unknown data member %s of variable obj.%s (line %d, col %d)' % (member, refText, line_no, col_offset))
                if isinstance(member, FieldMember):
                    currClass = None
                    finalMember = member
                elif isinstance(member, LinkMember):
                    currClass = member.LinkedClass
                    walkPath.append(('revLink' , tok))
                pass
            #-- for

            if finalMember == None: # no final field was found
                raise Exception('Invalid reference obj.%s. No field is referred (line %d, col %d)' % (refText, line_no, col_offset))
            
            ofd = FieldFormulaDependency(self)
            ofd.walkPath = walkPath
            ofd.targetField = finalMember
            finalMember.addFormulaDependent(ofd)
            self.dependencies.append(ofd)

        pass
    #--

    def clearDependency(self):
        # clear dependencies
        for ofd in self.dependencies:
            ofd.targetField.removeFormulaDependent(self)

class FieldFormulaDependency:
    def __init__(self, owner): # owner is formula
        self.ownerFormula = owner
        self.targetField = None
        self.walkPath = [] # list on how to 'walk' from target to dependants
        self.linkConstraint = None # additional constraint for link

class ClassImpl: # class implementation information
    def __init__(self, ownerClass, classType, aInh = None, descImpl = None):
        self.owner = ownerClass
        self.classType = classType
        self.tableName = classType.PTableName.lower()
        self.tableIndex = -1
        self.inheritance = aInh
        self.descendantClass = aInh.descendantClass if aInh is not None else None
        self.keyImpls = []
        self.idxKeyImpls = {}
        self.descImpl = descImpl
        
    def addKeyImpl(self, aKeyImpl):
        # must be kept sorted by KeyOrder
        keyImpls = self.keyImpls
        keyImpls.append(aKeyImpl); aKeyImpl.keyIndex = len(keyImpls) - 1
        i = len(keyImpls) - 2
        while i >= 0 and keyImpls[i].member.KeyOrder > aKeyImpl.member.KeyOrder:
            tmp = keyImpls[i]
            keyImpls[i] = aKeyImpl; aKeyImpl.keyIndex = i
            keyImpls[i + 1] = tmp; tmp.keyIndex = i + 1
            i -= 1
        self.idxKeyImpls[aKeyImpl.name] = aKeyImpl
        
class ClassTypeDecl(TypeDecl): 
    SO_CLASSNAME = "ClassTypeDecl"
    def __init__(self, parentObject):
        TypeDecl.__init__(self, parentObject)
        self.idxMembers = {} # index by name c.i
        self.primaryKeys = []
        self.fieldMembers = []
        self.memberImpls = []
        self.idxMemberImpls = {} # index by name
        self.classImpls = []
        self.idxClassImpls = {} # index by name
        self.rootClassImpls = []
        self.tableNames = []
        self.primaryClassImpl = None
        self.classifierField = None
        self.descendants = []
        self.idxDescendants = {}
        self.referencings = []
    #--
    
    def attachDescendant(self, aInh):
        if aInh.descendantClass not in self.idxDescendants:
            self.idxDescendants[aInh.descendantClass] = aInh
            self.descendants.append(aInh)
    
    def detachDescendant(self, aInh):
        if aInh.descendantClass in self.idxDescendants:
            del self.idxDescendants[aInh.descendantClass]
            i = self.descendants.index(aInh)
            del self.descendants[i]
        #--
    
    def attachReference(self, aLink):
        self.referencings.append(aLink)
    
    def detachReference(self, aLink):
        try:
            self.referencings.remove(aLink)
        except:
            pass
        
    
    def findAncestorKey(self, aKey): # search keylink  in ancestor classes
        for anc in self.Ancestors:
            for inhLink in anc.InhLinks:
                if aKey == inhLink.ThisPKey:
                    return inhLink
        return None
    
    def addClassImpl(self, classType, aInh, descImpl):
        members = classType.Members
        ancestors = classType.Ancestors
        idx = self.idxMemberImpls
        pTableName = classType.PTableName.upper()
        oODL = self.owner.primaryODL or self.owner
        tblInfo = oODL.PhysicalDB.idxTables[pTableName.lower()]
        if pTableName not in self.tableNames:
            self.tableNames.append(pTableName)
            tblIndex = len(self.tableNames) - 1
        else:
            tblIndex = self.tableNames.index(pTableName)
            
        aClassImpl = ClassImpl(self, classType, aInh, descImpl)
        self.classImpls.append(aClassImpl)
        self.idxClassImpls[classType.TypeName.lower()] = aClassImpl
        aClassImpl.tableIndex = tblIndex
            
        for m in members:
            if isinstance(m, FieldMember) and m.IsPrimaryKey and aInh is not None:
                keyLink = aInh.findLinkOfAncestorKey(m)
            else:
                keyLink = None
            amimpl = MemberImpl(self, aClassImpl, m, keyLink)
            idx[amimpl.cnName] = amimpl
            if amimpl.name not in idx:
                self.memberImpls.append(amimpl)
                idx[amimpl.name] = amimpl
            #--
            if isinstance(m, FieldMember):
                if m.IsPrimaryKey:
                    aClassImpl.addKeyImpl(amimpl)
                amimpl.fieldIndex = tblInfo.idxFields[m.PhysicalName.lower()].FieldIndex
            pass
        #-- for
        for inh in ancestors:
            self.addClassImpl(inh.AncestorClass, inh, aClassImpl)
            pass
        
        if len(ancestors) == 0:
            self.rootClassImpls.append(aClassImpl)
            
        return aClassImpl
    
    def buildClsImplStructure(self):
        aClassImpl = self.addClassImpl(self, None, None)
        self.primaryClassImpl = aClassImpl
        pass

#    def sobjStartResolveAddress(self):
#        print 'Start resolving class %s' % (self.TypeName)
#        pass
    
    def addMember(self, aMember):
        self.Members.append(aMember)
        self.idxMembers[aMember.MemberName.lower()] = aMember
        if isinstance(aMember, FieldMember):
            self.fieldMembers.append(aMember)
    
    def removeMember(self, aMember):
        try:
            self.Members.remove(aMember)
        except:
            pass
        del self.idxMembers[aMember.MemberName.lower()]
        if isinstance(aMember, FieldMember):
            try:
                self.fieldMembers.remove(aMember)
            except:
                pass
            if aMember.IsPrimaryKey:
                try:
                    self.primaryKeys.remove(aMember)
                except:
                    pass
                #--
                pass
            #-- if
            pass
        #-- if
        pass
    #--

    def getMemberByName(self, memberName):
        return self.idxMembers.get(memberName.lower(), None)

    def notifyPrimaryKeySet(self, aFieldMember):
        if aFieldMember.IsPrimaryKey:
            self.primaryKeys.append(aFieldMember)
            aFieldMember.KeyOrder = len(self.primaryKeys)
        else:
            try:
                i = self.primaryKeys.index(aFieldMember)
            except:
                i = -1
                
            if i >= 0:
                del self.primaryKeys[i]
            #--
            pass
        #--
        self.notifyPrimaryKeyChanges()
        pass
    #--
    
    def isDescendantOf(self, aAncestor):
        result = False
        for ainh in self.Ancestors:
            if ainh.AncestorClass == aAncestor:
                result = True
                break
            else:
                result = ainh.AncestorClass.isDescendantOf(aAncestor)
            #--
            pass
        #-- for
        return result
    IsDescendantOf = isDescendantOf
    
    def findKeyWithAncestorKey(self, AAncestorKey):
        result = None
        for key in self.primaryKeys:
            if key.isKeyLinkedToAncestorKey(AAncestorKey):
                result = key
                break
        return result
    
    def notifyPrimaryKeyChanges(self):
        for desc in self.descendants:
            desc.notifyAncestorClassPrimaryKeyChanges()
        for ref in self.referencings:
            ref.notifyLinkedClassPrimaryKeyChanges()
            

class Inheritance(odlBase): 
    SO_CLASSNAME = "Inheritance"
    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.AutoDescendantKey = False
        self.InhLinks = []
        self.AncestorClass = None
        self.descendantClass = parentObject
        self.classifierField = None # set when AncestorClass is set

#    def sobjOnResolveAddress(self, propInfo, symAddress):
#        if self.descendantClass.TypeName == 'ProdukFleksiMax':
#            print 'Resolving in Inheritance object'
#            #import rpdb2; rpdb2.start_embedded_debugger('000')
#            print 'Resolving property %s with value %s' % (str(propInfo), str(symAddress))

    def setAncestorClass(self, value):
        prevValue = self.AncestorClass
        self.AncestorClass = value
        if prevValue != value:
            if prevValue is not None:
                prevValue.detachDescendant(self)
            if value is not None:
                value.attachDescendant(self)
        if value is None:
            self.AncestorClassName = ""
            self.classifierField = None
        else:
            self.AncestorClassName = value.TypeName
            if self.AutoDescendantKey:
                self.generateAutoDescendantKeys()
            if value.classifierField is None:
                raise Exception('Classifier field is not specified in class %s (when defining inheritance to class %s) ' % (value.TypeName, self.descendantClass.TypeName))
            self.classifierField = value.classifierField
        #-- else
    #--

    def setAutoDescendantKey(self, value):
        if value and not self.AutoDescendantKey:
            #import rpdb2; rpdb2.start_embedded_debugger('000')
            self.AutoDescendantKey = value
            self.generateAutoDescendantKeys()
        elif not value and self.AutoDescendantKey:
            self.clearAutoDescendantKeys()
            self.clearInhLinks()
            #self.DescendantClass.notifyPrimaryKeyChanges()
            self.AutoDescendantKey = value
        #--
    #--
    
    def clearAutoDescendantKeys(self):
        if self.AutoDescendantKey:
            for inhLink in self.InhLinks:
                self.descendantClass.removeMember(inhLink.ThisPKey)
        pass
    

    def generateAutoDescendantKeys(self):
        self.clearAutoDescendantKeys()
        self.clearInhLinks(False)
        if self.AncestorClass == None:
            return
        if self.AutoDescendantKey:
            n = len(self.AncestorClass.primaryKeys);
            for i in range(n):
                ainhlink = InheritanceLink(self)
                self.InhLinks.append(ainhlink)
                aKey = self.AncestorClass.primaryKeys[i]
                ainhlink.OriginPKey = aKey

                aThisKey = FieldMember(self.descendantClass)
                aThisKey.MemberName = aKey.MemberName
                aThisKey.PhysicalName = aKey.MemberName
                aThisKey.DataType = aKey.DataType
                aThisKey.Enumerator = aKey.Enumerator
                aThisKey.DataLength = aKey.DataLength
                aThisKey.DataTypeID = aKey.DataTypeID
                aThisKey.inheritanceLink = ainhlink
                aThisKey.IsPrimaryKey = True
                aThisKey.ancestorKey = aKey
                aThisKey.state = fsAncestorKey

                self.descendantClass.addMember(aThisKey)
                self.descendantClass.primaryKeys.append(aThisKey)
                aThisKey.KeyOrder = len(self.descendantClass.primaryKeys)
                ainhlink.ThisPKey = aThisKey
            #--
        #--
        self.descendantClass.notifyPrimaryKeyChanges() # propagate changes to descendant class
    #--

    def clearInhLinks(self, bNotifyPKChanges = True):
        del self.InhLinks[:]
        pass
    
    def findLinkOfAncestorKey(self, ancKey):
        for inhLink in self.InhLinks:
            if inhLink.OriginPKey == ancKey:
                return inhLink
        return None
    
    def notifyAncestorClassPrimaryKeyChanges(self):
        if self.AutoDescendantKey:
            self.generateAutoDescendantKeys()
#--

MEMBERKIND_UNKNOWN = 0
MEMBERKIND_FIELD = 1
MEMBERKIND_LINK = 2
MEMBERKIND_LIST = 3

class ClassMember(odlBase): 
    SO_CLASSNAME = "ClassMember"
    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.owner = parentObject # ClassTypeDecl
        self.memberKind = MEMBERKIND_UNKNOWN

class FieldMember(ClassMember): 
    SO_CLASSNAME = "FieldMember"

    def __init__(self, parentObject):
        ClassMember.__init__(self, parentObject)
        self.IsPrimaryKey = False
        self.KeyOrder = 0
        self.memberKind = MEMBERKIND_FIELD
        self.state = fsNormal
        self.ancestorKey = None
        self.enumerator = None
        self.DataTypeID = 0
        self.DataType = None
        self.formulaDependents = []
        self.formula = None

    def addFormulaDependent(self, ofd):
        self.formulaDependents.append(ofd)
        pass # not implemented yet

    def removeFormulaDependent(self, ofd):
        self.formulaDependents.remove(ofd)

    def setFormula(self, aFormula):
        if self.formula != None and self.formula != aFormula:
            self.formula.clearDependency()
        self.formula = aFormula

    def setDataType(self, value):
        if self.__dict__.get('__soState', None) == cstReading:
            return
        
        if value is not None and not isinstance(value, BasicTypeDecl) and not isinstance(value, EnumTypeDecl):
            raise Exception('Field member type must be basic type or enumeration')
        
        if value is not None:
            if isinstance(value, EnumTypeDecl):
                if value.EnumType == 0: # integer
                    basic = self.parentObject.owner.idxTypeDecl['integer']
                elif value.EnumType == 1: #string
                    basic = self.parentObject.owner.idxTypeDecl['varchar']
                else:
                    raise Exception('Unknown enumeration type')
                self.DataLength = value.getEnumeratorLength()
                enum = value
            else:
                enum = None
                basic = value
        else:
            enum = None
            basic = None
        self.Enumerator = enum
        self.DataType = basic
        if basic is not None:
            self.DataTypeID = basic.DataTypeID
        if self.IsPrimaryKey:
            self.owner.notifyPrimaryKeyChanges()
                
            
    def setAsPrimaryKey(self, isKey):
        if self.IsPrimaryKey ^ isKey:
            self.IsPrimaryKey = isKey
            self.owner.notifyPrimaryKeySet(self)
        #--
    #--
    
    def setIsClassifierField(self, isClassifierField):
        owner = self.owner
        if isClassifierField:
            if owner.classifierField != self:
                if owner.classifierField is not None:
                    owner.classifierField.IsClassifierField = False
                    owner.classifierField.fieldState = fsNormal
                #--
                owner.classifierField = self
            self.fieldState = fsClassifier
        else:
            owner.classifierField = None
            self.fieldState = fsNormal
        self.IsClassifierField = isClassifierField
        
    def isKeyLinkedToAncestorKey(self, AAncestorKey):
        current = self
        while current != None:
            if current == AAncestorKey:
                return True
            current = current.ancestorKey
        return False

class LinkMember(ClassMember): 
    SO_CLASSNAME = "LinkMember"
    
    def __init__(self, parentObject):
        ClassMember.__init__(self, parentObject)
        self.memberKind = MEMBERKIND_LINK
        self.AutoForeignKey = False
        self.AttrLinks = []
        self.IsWeak = False
        self.LinkedClass = None
        self.SharedLink = None
        self.sharingLinks = []
        
    def setSharedLink(self, value):
        cValue = self.SharedLink
        if value != cValue:
            if self.AutoForeignKey:
                self.setAutoForeignKey(False)
            self.clearAttrLinks()
            if cValue is not None:
                cValue.sharingLinks.remove(self)
            
            if value is not None:
                value.sharingLinks.append(self)
                
            for i, attrLink in enumerate(value.AttrLinks):
                al = AttributeLink(self)
                al.ThisField = attrLink.ThisField
                al.OriginPKey = self.LinkedClass.findKeyWithAncestorKey(attrLink.OriginPKey)
            self.SharedLink = value
        #--
        pass
    #--
        
    def clearAttrLinks(self):
        self.AttrLinks = []
        
    def clearAutoForeignKeys(self):
        if self.AutoForeignKey:
            for attrLink in self.AttrLinks:
                self.owner.removeMember(attrLink.ThisField)
            #-- for
            pass
        #-- if
        pass
        
    def generateAutoForeignKeys(self):
        self.clearAutoForeignKeys()
        self.clearAttrLinks()
        lclass = self.LinkedClass
        if lclass is not None:
            pkeys = lclass.primaryKeys

            for AKey in pkeys:
                AAttrLink = AttributeLink(self)
                self.AttrLinks.append(AAttrLink)

                AAttrLink.OriginPKey = AKey
                AThisField = FieldMember(self.parentObject)
                AThisField.MemberName = AKey.MemberName
                AThisField.PhysicalName = AKey.MemberName
                AThisField.DataType = AKey.DataType
                AThisField.Enumerator = AKey.Enumerator
                AThisField.DataTypeID = AKey.DataTypeID
                AThisField.DataLength = AKey.DataLength
                AThisField.AttributeLink = AAttrLink
                AThisField.state = fsAutoLink
                AThisField.IsUsedInLink = True
                self.owner.addMember(AThisField)
                AAttrLink.ThisField = AThisField
            #-- for
            pass
        #-- if
        self.notifyAttrLinkChangesToSharingLinks()
        pass
    
    def setAutoForeignKey(self, value):
        if value and not self.AutoForeignKey:
            self.AutoForeignKey = value
            self.generateAutoForeignKeys()
        else:
            self.clearAutoForeignKeys()
            self.clearAttrLinks()
            self.AutoForeignKey = value
        pass
    
    def setLinkedClass(self, value):
        if self.AutoForeignKey:
            self.clearAttrLinks()
        if self.LinkedClass is not None:
            self.LinkedClass.detachReference(self)
        self.LinkedClass = value
        if value is not None:
            value.attachReference(self)
        if self.AutoForeignKey:
            self.generateAutoForeignKeys()
        self.notifyAttrLinkChangesToSharingLinks()
    
    def notifyAttrLinkChangesToSharingLinks(self):
        for sl in self.sharingLinks:
            if self.LinkedClass is None:
                sl.setSharedLink(None)
            else:
                sl.setSharedLink(None)
                sl.setSharedLink(self)
        pass
    
    def notifyLinkedClassPrimaryKeyChanges(self):
        if self.AutoForeignKey:
            self.generateAutoForeignKeys()
        pass

    
        
class ListMember(ClassMember): 
    SO_CLASSNAME = "ListMember"
    def __init__(self, parentObject):
        ClassMember.__init__(self, parentObject)
        self.memberKind = MEMBERKIND_LIST
        
    
class InheritanceLink(odlBase): 
    SO_CLASSNAME = "InheritanceLink"
    def setThisPKey(self, value):
        self.ThisPKey = value
        if value == None:
            self.ThisPKeyName = ""
        else:
            self.ThisPKeyName = value.MemberName
        #--
        pass
    #--
    
    def setOriginPKey(self, value):
        self.OriginPKey = value
        if self.ThisPKey != None:
            self.ThisPKey.ancestorKey = value
    pass
#--

class AttributeLink(odlBase): SO_CLASSNAME = "AttributeLink"
class FormalParameter(odlBase): SO_CLASSNAME = "FormalParameter"
class Method(ClassMember): SO_CLASSNAME = "Method"

class TargetDBInfo(odlBase): SO_CLASSNAME = "TargetDBInfo"
class ODLDatabase(odlBase): 
    SO_CLASSNAME = "ODLDatabase"
    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.Tables = []
        self.idxTables = {}
        pass
    
    def reset(self):
        self.Tables = []
        self.idxTables = {}
        
    def addTable(self, aTable):
        self.Tables.append(aTable)
        self.idxTables[aTable.TableName.lower()] = aTable
    #--
    
class ODLTable(odlBase): 
    SO_CLASSNAME = "ODLTable"
    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.Fields = []
        self.PrimaryKeys = [] #must be sorted according to key number      
        self.idxFields = {}
        self.TableName = ''
        self.Shared = False
    
    def addField(self, aField):
        self.Fields.append(aField)
        self.idxFields[aField.FieldName.lower()] = aField
        aField.FieldIndex = len(self.Fields) - 1
        if aField.IsPrimaryKey:
            #sort according to KeyOrder field here
            self.PrimaryKeys.append(aField)
            i = len(self.PrimaryKeys) - 1
            i = i - 1
            while i >= 0 and self.PrimaryKeys[i].KeyOrder > aField.KeyOrder:
                tmp = self.PrimaryKeys[i]
                #swap position
                self.PrimaryKeys[i] = aField
                self.PrimaryKeys[i + 1] = tmp
                i = i - 1
        
class ODLField(odlBase): 
    SO_CLASSNAME = "ODLField"
    def __init__(self, parentObject):
        odlBase.__init__(self, parentObject)
        self.Owner = parentObject
        self.FieldName = ''
        self.TypeID = 0
        self.DataFormat = ''
        self.Length = 0
        self.IsPrimaryKey = False
        self.KeyOrder = 0
        self.FieldIndex = -1
#--

odlMetadata = {
    'odl': {
        MD_CLASSNAME: "ODL",
        MD_CLASSF: ODL,
        MD_PROPERTIES: {
            'odlversion': {MD_PROPERTYNAME: "ODLVersion", MD_DATATYPE: ptInteger, MD_WRITEMODE: wmNeverWrite},
            'odlname': {MD_PROPERTYNAME: "ODLName", MD_DATATYPE: ptString},
            'refs': {MD_PROPERTYNAME: "Refs", MD_DATATYPE: ptObjectVect},
            'typedecls': {
                MD_PROPERTYNAME: "TypeDecls", MD_DATATYPE: ptObjectVect, MD_ADDERF: ODL.addTypeDecl, 
                MD_RESOLVERF: ODL.getTypeDeclByName,
                MD_IDX_DICT_PROP: 'idxTypes'
            },
            'classes': {MD_PROPERTYNAME: "Classes", MD_DATATYPE: ptObjectRefVect, MD_WRITEMODE: wmNeverWrite},
            'physicaldb': {MD_PROPERTYNAME: "PhysicalDB", MD_DATATYPE: ptObject},
            'targetdb': {MD_PROPERTYNAME: "TargetDB", MD_DATATYPE: ptObject},
            'lastmethodid': {MD_PROPERTYNAME: "LastMethodID", MD_DATATYPE: ptInteger}
        }
    }

    , 'odlreference': {
        MD_CLASSNAME: "ODLReference",
        MD_CLASSF: ODLReference,
        MD_NAMEATTRNAME: "NameSpace",
        MD_PROPERTIES: {
            'relpath': {MD_PROPERTYNAME: "RelPath", MD_DATATYPE: ptString},
            'namespace': {MD_PROPERTYNAME: "NameSpace", MD_DATATYPE: ptString},
            'dbnamespace': {MD_PROPERTYNAME: "DBNameSpace", MD_DATATYPE: ptString}
        }
    }

    , 'typedecl': {
        MD_CLASSNAME: "TypeDecl",
        MD_CLASSF: TypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_PROPERTIES: {
            'typename': {MD_PROPERTYNAME: "TypeName", MD_DATATYPE: ptString},
            'reserved': {MD_PROPERTYNAME: "Reserved", MD_DATATYPE: ptBoolean}
        }
    }
    , 'basictypedecl': {
        MD_CLASSNAME: "BasicTypeDecl",
        MD_CLASSF: BasicTypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_ANCESTOR: "TypeDecl",
        MD_PROPERTIES: {
            'datalength': {MD_PROPERTYNAME: "DataLength", MD_DATATYPE: ptInteger},
            'variablelength': {MD_PROPERTYNAME: "VariableLength", MD_DATATYPE: ptBoolean},
            'datatypeid': {MD_PROPERTYNAME: "DataTypeID", MD_DATATYPE: ptInteger}
        }
    }
    , 'recordtypedecl': {
        MD_CLASSNAME: "RecordTypeDecl",
        MD_CLASSF: RecordTypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_ANCESTOR: "TypeDecl",
        MD_PROPERTIES: {
            'recordmembers': {MD_PROPERTYNAME: "RecordMembers", MD_DATATYPE: ptObjectVect}
        }
    }
    , 'listtypedecl': {
        MD_CLASSNAME: "ListTypeDecl",
        MD_CLASSF: ListTypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_ANCESTOR: "TypeDecl",
        MD_PROPERTIES: {
            'elementtype': {MD_PROPERTYNAME: "ElementType", MD_DATATYPE: ptObjectRef},
            'datalength': {MD_PROPERTYNAME: "DataLength", MD_DATATYPE: ptInteger}
        }
    }
    , 'recordmember': {
        MD_CLASSNAME: "RecordMember",
        MD_CLASSF: RecordMember,
        MD_NAMEATTRNAME: "FieldName",
        MD_PROPERTIES: {
            'fieldname': {MD_PROPERTYNAME: "FieldName", MD_DATATYPE: ptString},
            'datatype': {MD_PROPERTYNAME: "DataType", MD_DATATYPE: ptObjectRef},
            'datalength': {MD_PROPERTYNAME: "DataLength", MD_DATATYPE: ptInteger}
        }
    }
    , 'enumtypedecl': {
        MD_CLASSNAME: "EnumTypeDecl",
        MD_CLASSF: EnumTypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_ANCESTOR: "TypeDecl",
        MD_PROPERTIES: {
            'enumtype': {MD_PROPERTYNAME: "EnumType", MD_DATATYPE: ptInteger, MD_ENUM: ['etInteger', 'etString']},
            'enummembers': {MD_PROPERTYNAME: "EnumMembers", MD_DATATYPE: ptObjectVect, MD_NOADDRESS: None}
        }
    }
    , 'enummember': {
        MD_CLASSNAME: "EnumMember",
        MD_CLASSF: EnumMember,    
        MD_PROPERTIES: {
            'integervalue': {MD_PROPERTYNAME: "IntegerValue", MD_DATATYPE: ptInteger},
            'stringvalue': {MD_PROPERTYNAME: "StringValue", MD_DATATYPE: ptString},
            'descriptivename': {MD_PROPERTYNAME: "DescriptiveName", MD_DATATYPE: ptString}
        }
    }
    , 'classtypedecl': {
        MD_CLASSNAME: "ClassTypeDecl",
        MD_CLASSF: ClassTypeDecl,
        MD_NAMEATTRNAME: "TypeName",
        MD_ANCESTOR: "TypeDecl",
        MD_PROPERTIES: {
            'ptablename': {MD_PROPERTYNAME: "PTableName", MD_DATATYPE: ptString},
            'isextensible': {MD_PROPERTYNAME: "IsExtensible", MD_DATATYPE: ptBoolean},
            'members': {
                MD_PROPERTYNAME: "Members", 
                MD_DATATYPE: ptObjectVect, MD_ADDERF: ClassTypeDecl.addMember, 
                MD_RESOLVERF: ClassTypeDecl.getMemberByName,
                MD_IDX_DICT_PROP: 'idxMembers',
            },
            'ancestors': {MD_PROPERTYNAME: "Ancestors", MD_DATATYPE: ptObjectVect},
            'classid': {MD_PROPERTYNAME: "ClassID", MD_DATATYPE: ptString},
            'autoprimarykey': {MD_PROPERTYNAME: "AutoPrimaryKey", MD_DATATYPE: ptObjectRef},
            'memberimpls': {
                MD_PROPERTYNAME: "memberImpls", 
                MD_DATATYPE: ptObjectVect, 
                MD_WRITEMODE: wmNeverWrite,
                MD_IDX_DICT_PROP: 'idxMemberImpls',               
            }
        }
    }
    , 'inheritance': {
        MD_CLASSNAME: "Inheritance",
        MD_CLASSF: Inheritance,
        MD_NAMEATTRNAME: "AncestorClassName",
        MD_PROPERTIES: {
            'ancestorclassname': {MD_PROPERTYNAME: "AncestorClassName", MD_DATATYPE: ptString, MD_WRITEMODE: wmNeverWrite},
            'classifierintvalue': {MD_PROPERTYNAME: "ClassifierIntValue", MD_DATATYPE: ptInteger},
            'classifierstringvalue': {MD_PROPERTYNAME: "ClassifierStringValue", MD_DATATYPE: ptString},
            'ancestorclass': {MD_PROPERTYNAME: "AncestorClass", MD_DATATYPE: ptObjectRef, MD_WRITERF: Inheritance.setAncestorClass},
            'inhlinks': {MD_PROPERTYNAME: "InhLinks", MD_DATATYPE: ptObjectVect},
            'autodescendantkey': {MD_PROPERTYNAME: "AutoDescendantKey", MD_DATATYPE: ptBoolean, MD_WRITERF: Inheritance.setAutoDescendantKey}
        }
    }
    , 'classmember': {
        MD_CLASSNAME: "ClassMember",
        MD_CLASSF: ClassMember,
        MD_NAMEATTRNAME: "MemberName",
        MD_PROPERTIES: {
            'membername': {MD_PROPERTYNAME: "MemberName", MD_DATATYPE: ptString}
        }
    }
    , 'fieldmember': {
        MD_CLASSNAME: "FieldMember",
        MD_CLASSF: FieldMember,
        MD_NAMEATTRNAME: "MemberName",
        MD_ANCESTOR: "ClassMember",
        MD_PROPERTIES: {
            'physicalname': {MD_PROPERTYNAME: "PhysicalName", MD_DATATYPE: ptString},
            'datatype': {MD_PROPERTYNAME: "DataType", MD_DATATYPE: ptObjectRef, MD_WRITERF: FieldMember.setDataType},
            'dataformat': {MD_PROPERTYNAME: "DataFormat", MD_DATATYPE: ptString},
            'datalength': {MD_PROPERTYNAME: "DataLength", MD_DATATYPE: ptInteger},
            'isprimarykey': {MD_PROPERTYNAME: "IsPrimaryKey", MD_DATATYPE: ptBoolean, MD_WRITERF: FieldMember.setAsPrimaryKey},
            'visibilitycode': {MD_PROPERTYNAME: "VisibilityCode", MD_DATATYPE: ptInteger},
            'denynull': {MD_PROPERTYNAME: "DenyNull", MD_DATATYPE: ptBoolean},
            'isdefaultlookupfield': {MD_PROPERTYNAME: "IsDefaultLookupField", MD_DATATYPE: ptBoolean},
            'isclassifierfield': {MD_PROPERTYNAME: "IsClassifierField", MD_DATATYPE: ptBoolean, MD_WRITERF: FieldMember.setIsClassifierField},
            'enumerator': {MD_PROPERTYNAME: "Enumerator", MD_DATATYPE: ptObjectRef}
        }
    }
    , 'linkmember': {
        MD_CLASSNAME: "LinkMember",
        MD_CLASSF: LinkMember,
        MD_NAMEATTRNAME: "MemberName",
        MD_ANCESTOR: "ClassMember",
        MD_PROPERTIES: {
            'optional': {MD_PROPERTYNAME: "Optional", MD_DATATYPE: ptBoolean},
            'attrlinks': {MD_PROPERTYNAME: "AttrLinks", MD_DATATYPE: ptObjectVect, MD_NOADDRESS: None},
            'isweak': {MD_PROPERTYNAME: "IsWeak", MD_DATATYPE: ptBoolean},
            'linkedclass': {MD_PROPERTYNAME: "LinkedClass", MD_DATATYPE: ptObjectRef, MD_WRITERF: LinkMember.setLinkedClass},
            'sharedlink': {MD_PROPERTYNAME: "SharedLink", MD_DATATYPE: ptObjectRef, MD_WRITERF: LinkMember.setSharedLink},
            'autoforeignkey': {MD_PROPERTYNAME: "AutoForeignKey", MD_DATATYPE: ptBoolean, MD_WRITERF: LinkMember.setAutoForeignKey},
            'autolist': {MD_PROPERTYNAME: "AutoList", MD_DATATYPE: ptBoolean}
        }
    }
    , 'listmember': {
        MD_CLASSNAME: "ListMember",
        MD_CLASSF: ListMember,
        MD_NAMEATTRNAME: "MemberName",
        MD_ANCESTOR: "ClassMember",
        MD_PROPERTIES: {
            'usedlink': {MD_PROPERTYNAME: "UsedLink", MD_DATATYPE: ptObjectRef}
        }
    }
    , 'inheritancelink': {
        MD_CLASSNAME: "InheritanceLink",
        MD_CLASSF: InheritanceLink,
        MD_NAMEATTRNAME: "ThisPKeyName",
        MD_PROPERTIES: {
            'thispkeyname': {MD_PROPERTYNAME: "ThisPKeyName", MD_DATATYPE: ptString, MD_WRITEMODE: wmNeverWrite},
            'thispkey': {MD_PROPERTYNAME: "ThisPKey", MD_DATATYPE: ptObjectRef, MD_WRITERF: InheritanceLink.setThisPKey},
            'originpkey': {MD_PROPERTYNAME: "OriginPKey", MD_DATATYPE: ptObjectRef, MD_WRITERF: InheritanceLink.setOriginPKey}
        }
    }
    , 'attributelink': {
        MD_CLASSNAME: "AttributeLink",
        MD_CLASSF: AttributeLink,
        MD_PROPERTIES: {
            'thisfield': {MD_PROPERTYNAME: "ThisField", MD_DATATYPE: ptObjectRef},
            'originpkey': {MD_PROPERTYNAME: "OriginPKey", MD_DATATYPE: ptObjectRef}
        }
    }
    , 'formalparameter': {
        MD_CLASSNAME: "FormalParameter",
        MD_CLASSF: FormalParameter,
        MD_NAMEATTRNAME: "ParameterName",
        MD_PROPERTIES: {
            'parametername': {MD_PROPERTYNAME: "ParameterName", MD_DATATYPE: ptString},
            'byreference': {MD_PROPERTYNAME: "ByReference", MD_DATATYPE: ptBoolean},
            'datatype': {MD_PROPERTYNAME: "DataType", MD_DATATYPE: ptObjectRef},
            'datalength': {MD_PROPERTYNAME: "DataLength", MD_DATATYPE: ptInteger}
        }
    }
    , 'method': {
        MD_CLASSNAME: "Method",
        MD_CLASSF: Method,
        MD_NAMEATTRNAME: "MemberName",
        MD_ANCESTOR: "ClassMember",
        MD_PROPERTIES: {
            'visibilitycode': {MD_PROPERTYNAME: "VisibilityCode", MD_DATATYPE: ptInteger},
            'methodtype': {MD_PROPERTYNAME: "MethodType", MD_DATATYPE: ptInteger},
            'methodrole': {MD_PROPERTYNAME: "MethodRole", MD_DATATYPE: ptInteger},
            'methoddirective': {MD_PROPERTYNAME: "MethodDirective", MD_DATATYPE: ptInteger},
            'returntype': {MD_PROPERTYNAME: "ReturnType", MD_DATATYPE: ptObjectRef},
            'returnlength': {MD_PROPERTYNAME: "ReturnLength", MD_DATATYPE: ptInteger},
            'parameters': {MD_PROPERTYNAME: "Parameters", MD_DATATYPE: ptObjectVect},
            'methodid': {MD_PROPERTYNAME: "MethodID", MD_DATATYPE: ptInteger}
        }
    }
    , 'targetdbinfo': {
        MD_CLASSNAME: "TargetDBInfo",
        MD_CLASSF: TargetDBInfo,
        MD_PROPERTIES: {
        }
    }
    , 'odldatabase': {
        MD_CLASSNAME: "ODLDatabase",
        MD_CLASSF: ODLDatabase,
        MD_PROPERTIES: {
            'tables': {MD_PROPERTYNAME: "Tables", MD_DATATYPE: ptObjectVect, MD_ADDERF: ODLDatabase.addTable, MD_NOADDRESS: None}
        }
    }
    , 'odltable': {
        MD_CLASSNAME: "ODLTable",
        MD_CLASSF: ODLTable,
        MD_PROPERTIES: {
            'fields': {MD_PROPERTYNAME: "Fields", MD_DATATYPE: ptObjectVect, MD_ADDERF: ODLTable.addField, MD_NOADDRESS: None},
            'tablename': {MD_PROPERTYNAME: "TableName", MD_DATATYPE: ptString},
            'shared': {MD_PROPERTYNAME: "Shared", MD_DATATYPE: ptBoolean}
        }
    }
    , 'odlfield': {
        MD_CLASSNAME: "ODLField",
        MD_CLASSF: ODLField,
        MD_PROPERTIES: {
            'fieldname': {MD_PROPERTYNAME: "FieldName", MD_DATATYPE: ptString},
            'typeid': {MD_PROPERTYNAME: "TypeID", MD_DATATYPE: ptInteger},
            'dataformat': {MD_PROPERTYNAME: "DataFormat", MD_DATATYPE: ptString},
            'length': {MD_PROPERTYNAME: "Length", MD_DATATYPE: ptInteger},
            'isprimarykey': {MD_PROPERTYNAME: "IsPrimaryKey", MD_DATATYPE: ptBoolean},
            'keyorder': {MD_PROPERTYNAME: "KeyOrder", MD_DATATYPE: ptInteger}
        }
    }
}

sobject.sobjhelper.processMDInheritances(odlMetadata)
