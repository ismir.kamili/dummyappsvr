# generate internal interpreter pseudo-code for python expression

import os
import sys
import types
import ast
from ast import *
from copy import copy

class Code:
    def __init__(self, mnemonic, *argv):
        self.mnemonic = mnemonic
        self.index = 0
        self.indent = 0
        self.prm = []
        i = 0
        for v in argv:
            self.__dict__['prm%d' % (i + 1)] = v
            self.prm.append(v)
            i += 1
        #-- for
        self.nPrm = len(argv)
        
    def __str__(self):
        nPrm = self.nPrm
        return self.mnemonic + ' ' + (('%s ' * nPrm)[: -1]) % tuple(self.prm)
    #-- def
    pass

#- class Instruction

def loadScript(fileName):
    f = file(fileName, 'rb')
    s = f.read()
    f.close()
    return s

class VarReference:
    def __init__(self):
        self.reference = ''
        self.node = None
        self.context = ''

    def __str__(self):
        return self.reference + ((' * %s' % self.context) if self.context != '' else '')

class AggregateReference:
    def __init__(self):
        self.context = ''
        self.aggrFunction = ''
        self.astNode = None

CODEGEN_MODE_FREESTYLE = 1
CODEGEN_MODE_RELATIONAL = 2


RELATIONAL_FUNCTIONS = ('sum', 'avg', 'min', 'max', 'count', 'year', 'month', 'day', 'to_date')
AGGREGATE_FUNCTIONS = ('sum', 'avg', 'min', 'max', 'count')

def isIdentifier(s):
    s = s.lower()
    return len(s) > 0 and ('a' <= s[0] <= 'z' or s[0] == '_') and \
        (filter(lambda x: (x < 'a' or x > 'z') and (x < '0' or x > '9') and x != '_', s) == '')

class AggrCriterionParser:

    def __init__(self, context):
        self._methodCache = {} # map ast class to method
        self.astNode = None
        self.nodeStack = []
        self.varRefs = []
        self.context = context

    def _registerVarRef(self, refText, node):
        vr = VarReference()
        vr.reference = refText; vr.node = node
        vr.context = self.context
        self.varRefs.append(vr)
        return vr

    def _getHandler(self, nm):
        if self._methodCache.has_key(nm):
            return self._methodCache.get(nm)
        else:
            mth = self.__class__.__dict__.get('_handle' + nm)
            if mth == None:
                raise Exception, 'Unsupported code for %s syntax element' % nm
            boundMth = types.MethodType(mth, self)
            self._methodCache[nm] = boundMth
            return boundMth
        #--
        pass
    #--

    def checkNode(self, astNode):
        self.astNode = astNode
        self.nodeStack = []
        self.varRefs = []
        self._checkNode(astNode)

    def _checkNode(self, node):
        nc = node.__class__
        self.nodeStack.append(node)
        try:
            if type(node) is list:
                ls = node
                for nd in ls:
                    self._genCode(nd)
            else:
                cname = nc.__name__
                handler = self._getHandler(cname)
                handler(node)
            #-
            pass
        finally:
            self.nodeStack.pop()

    def _getParentNode(self):
        ln = len(self.nodeStack)
        return None if ln < 2 else self.nodeStack[ln - 2]

    def _handleName(self, node):
        ctxClass = node.ctx.__class__
        if ctxClass not in (ast.Load, ast.Store):
            raise Exception, 'Unsupported syntax (multiple assignment)'

    def _handleNum(self, node): pass

    def _handleStr(self, node): pass

    def _handleUnaryOp(self, node):
        self._checkNode(node.operand)

    def _handleBinOp(self, node):
        self._checkNode(node.left)
        self._checkNode(node.right)

    def _handleBoolOp(self, node):
        self._checkNode(node.values[0])
        opName = node.op.__class__.__name__
        nr = len(node.values) - 1
        for i in range(nr):
            self._checkNode(node.values[i + 1])

    def _handleCompare(self, node):
        # push the left first
        self._checkNode(node.left)
        # next for each 'right', put on operator afterwise
        nr = len(node.comparators)
        for i in range(nr):
            self._checkNode(node.comparators[i])

    def _handleCall(self, node):
        nArgs = len(node.args)
        hasStarArgs = node.starargs != None
        hasKwArgs = node.kwargs != None
        if not isinstance(node.func, ast.Name):
            raise Exception, 'only global function is supported'
        if node.func.id not in RELATIONAL_FUNCTIONS:
            raise Exception, 'only standard relational function is supported'
        if node.func.id in AGGREGATE_FUNCTIONS:
            raise Exception, 'Aggregate function cannot be nested'
        if hasStarArgs or hasKwArgs or len(node.keywords) > 0:
            raise Exception, 'Only standard argument format is supported'

        for i in range(nArgs):
            arg = node.args[i]
            self._checkNode(arg)

    def _handleAttribute(self, node):
        ctxClass = node.ctx.__class__
        if ctxClass != ast.Load:
            raise Exception, 'Attribute context not supported (only load)'
        if node.value.__class__ not in (ast.Attribute, ast.Name): # only name and attribute ref accepted:
            raise Exception, 'Invalid reference'
        if node.value.__class__ == ast.Name and node.value.id not in ('member', 'obj'):
            raise Exception, 'Reference in aggregate filter must start with "member" or "obj" keyword'
        if self._getParentNode().__class__ == ast.Attribute:
            node.attrText = node.attr + '.' + self._getParentNode().attrText
        else:
            node.attrText = node.attr

        if node.value.__class__ == ast.Name:
            completeAttrRef = node.value.id + '.' + node.attrText
            self._registerVarRef(completeAttrRef, node.value)
        #--
        pass

        self._checkNode(node.value)

class CodeGenerator:
    def __init__(self, mode = CODEGEN_MODE_FREESTYLE):
        self._methodCache = {} # map ast class to method
        self.codes = []
        self.nodeStack = []
        self.varRefs = [] # contains all variable references
        self.aggrRefs = [] # contains all aggregate references
        self._aggregateFunctionMode = False
        self.mode = mode
        self.functionStack = [] # stack of function declaration
        self.mainDefined = False

    def _registerVarRef(self, refText, node, isList = False, aggrListRef = None):
        vr = VarReference()
        vr.reference = refText; vr.node = node; vr.isListReference = isList; vr.aggrListReference = aggrListRef
        self.varRefs.append(vr)
        return vr

    def _registerAggrRef(self, aggrContext, aggrFunction, astNode):
        aggrRef = AggregateReference()
        aggrRef.context = aggrContext
        aggrRef.aggrFunction = aggrFunction
        aggrRef.astNode = astNode

    def _getHandler(self, nm):
        if self._methodCache.has_key(nm):
            return self._methodCache.get(nm)
        else:
            mth = self.__class__.__dict__.get('_handle' + nm)
            if mth == None:
                raise Exception, 'Unsupported code for %s syntax element' % nm
            boundMth = types.MethodType(mth, self)
            self._methodCache[nm] = boundMth
            return boundMth
        #--
        pass
    #--

    def genCode(self, astNode):
        self.codes = []
        self.nodeStack = []
        self.functionStack = []
        self.varRefs = []
        self.aggrRefs = []
        self.mainDefined = False
        self._indentLevel = 0
        self._loopContext = []
        self._genCode(astNode)
        if self.mode == CODEGEN_MODE_FREESTYLE:
            if not self.mainDefined:
                raise Exception, 'Entry function main() not defined inside code'

    def _genCode(self, node, isStmtBlock = False):
        nc = node.__class__
        self._indentLevel += 1
        self.nodeStack.append(node)
        try:
            if type(node) is list: # can be statement block or list of expressions (defined in isStmtBlock parameter)
                ls = node
                for nd in ls:
                    self._genCode(nd)
                    if isStmtBlock and isinstance(nd, ast.Expr):
                        self.addCode('DISCARD') # discard expression 
            else:
                cname = nc.__name__
                handler = self._getHandler(cname)
                handler(node)
            #-
            pass
        finally:
            self._indentLevel -= 1
            self.nodeStack.pop()

    def _getParentNode(self):
        ln = len(self.nodeStack)
        return None if ln < 2 else self.nodeStack[ln - 2]

    def addCode(self, mnemonic, *argv):
        c = Code(mnemonic, *argv)
        iPos = len(self.codes)
        self.codes.append(c)
        c.indent = self._indentLevel
        c.index = iPos

    def printCodes(self, useIndent = True):
        codes = self.codes
        for c in codes:
            print '%06d%s %s' % (c.index, ('  ' * c.indent) if useIndent else '', str(c))

    def printVarRefs(self):
        for vr in self.varRefs:
            print str(vr)

    def setCodeAt(self, iPos, mnemonic, *argv):
        c = Code(mnemonic, *argv)
        self.codes[iPos] = c
        c.indent = self._indentLevel
        c.index = iPos

    def _checkIsAttributeBasic(self, attrNode):
        while attrNode.value.__class__ == ast.Attribute:
            attrNode = attrNode.value
        return (attrNode.value.__class__ == ast.Name, attrNode.value)

    def _pushFunction(self, fnName):
        self.functionStack.append({'fn_name': fnName, 'symtable': None})

    def _popFunction(self):
        self.functionStack.pop()

    def _handleNum(self, node):
        self.addCode('NUM', node.n)

    def _handleStr(self, node):
        self.addCode('STR', node.s)

    def _handleList(self, node):
        ctxClass = node.ctx.__class__
        if ctxClass != ast.Load:
            raise Exception, 'Unsupported syntax (multiple assignment)'
        n = len(node.elts)
        self._genCode(node.elts, False)
        self.addCode('ARRAY', n)

    _handleTuple = _handleList

    def _handleName(self, node):
        ctxClass = node.ctx.__class__
        if ctxClass not in (ast.Load, ast.Store):
            raise Exception, 'Unsupported syntax (multiple assignment)'
        if ctxClass == ast.Load:
            self.addCode('LOAD', node.id)
        else:
            self.addCode('STORE', node.id)

    def _handleExpr(self, node):
        self._genCode(node.value)

    def _handleUnaryOp(self, node):
        self._genCode(node.operand)
        self.addCode('UNOP', node.op.__class__.__name__)

    def _handleBinOp(self, node):
        self._genCode(node.left)
        self._genCode(node.right)
        self.addCode('BINOP', node.op.__class__.__name__)

    def _handleBoolOp(self, node):
        self._genCode(node.values[0])
        opName = node.op.__class__.__name__
        nr = len(node.values) - 1
        for i in range(nr):
            self._genCode(node.values[i + 1])
            self.addCode('BOOLOP', opName)

    def _handleCompare(self, node):
        # push the left first
        self._genCode(node.left)
        # next for each 'right', put on operator afterwise
        nr = len(node.comparators)
        for i in range(nr):
            self._genCode(node.comparators[i])
            self.addCode('COMPOP', node.ops[i].__class__.__name__)

    def _handleCall(self, node):
        nArgs = len(node.args)
        hasStarArgs = node.starargs != None
        hasKwArgs = node.kwargs != None
        if self.mode == CODEGEN_MODE_RELATIONAL:
            if not isinstance(node.func, ast.Name):
                raise Exception, 'Relational mode: only global function is supported'
            if node.func.id not in RELATIONAL_FUNCTIONS:
                raise Exception, 'Relational mode: only standard relational function is supported'

        isAggregate = isinstance(node.func, ast.Name) and node.func.id in AGGREGATE_FUNCTIONS # aggregate functions
        if isAggregate and self._aggregateFunctionMode :
            raise Exception, 'Aggregate function cannot be nested'

        if isAggregate and (nArgs != 4 or len(node.keywords) > 0 or hasStarArgs or hasKwArgs):
            raise Exception, 'Invalid number aggregate function argument (must be 4 arguments)'

        if isAggregate and node.args[0].__class__ != ast.Name and node.args[0].id != 'obj':
            raise Exception, 'Invalid aggregate function argument. (First argument must be "obj")'

        if isAggregate:
            for i in range(1, 4):
                if node.args[i].__class__ != ast.Str:
                    raise Exception, 'Invalid aggregate function argument (string constant required)'

        self._aggregateFunctionMode = isAggregate
        if isAggregate:
            self._aggrFunction = node.func.id.lower()
        try:
            for i in range(nArgs):
                arg = node.args[i]
                self._genCode(arg)
                pass
            #-- for

            if isAggregate:
                try:
                    self._analyzeAggregateStrParam((node.args[1].s, node.args[2].s, node.args[3].s), node)
                except:
                    sErr = '%s.%s' % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
                    raise Exception, 'Invalid aggregate function string parameter (line %d, col %d)\r\n%s' % (arg.lineno, arg.col_offset, sErr)
                #--
                pass
            #--

        finally:
            self._aggregateFunctionMode = False

        # push keywords next, each expression with one keyword name
        nKeywords = len(node.keywords)
        for kw in node.keywords:
            self._genCode(kw.value)
            self.addCode('KWPRM', kw.arg)
        # push starargs and kwargs
        if hasStarArgs:
            self._genCode(node.starargs)
        if hasKwArgs:
            self._genCode(node.kwargs)
        self.addCode('FCALL', node.func.id, nArgs, nKeywords, hasStarArgs, hasKwArgs)

    def _analyzeAggregateStrParam(self, params, node):
        sParam = params[0]
        if not isIdentifier(sParam):
            raise Exception, 'list name string required as first parameter'

        sParam = params[1]
        tokens = sParam.split('.')
        if len(tokens) == 0:
            raise Exception, 'member required'
        for t in tokens:
            if not isIdentifier(t):
                raise Exception, 'member required'
        if tokens[0] != 'member':
            raise Exception, 'member required'
        vr = self._registerVarRef(sParam, node)
        vr.context = params[0]
        #--

        sParam = params[2]
        if sParam != '':
            astNode = ast.parse(sParam)
            if len(astNode.body) > 1 or not isinstance(astNode.body[0], ast.Expr):
                raise Exception, 'expression required'
            checker = AggrCriterionParser(params[0])
            checker.checkNode(astNode.body[0].value)
            self.varRefs.extend(copy(checker.varRefs))
            self._registerAggrRef(node.func.id.lower(), params[0], astNode.body[0].value)

    def _handleAttribute(self, node):
        ctxClass = node.ctx.__class__
        if ctxClass not in (ast.Load, ast.Store):
            raise Exception, 'Attribute context not supported (only load and store)'
        self._genCode(node.value)
        if ctxClass == ast.Load and node.value.__class__ in (ast.Attribute, ast.Name):
            node.attrText = ''
            parentNode = self._getParentNode()
            if parentNode != None and parentNode.__class__ == ast.Attribute:
                node.attrText = node.attr + '.' + self._getParentNode().attrText
            else:
                node.attrText = node.attr

        if ctxClass == ast.Load and node.value.__class__ == ast.Name and node.value.id == 'obj':
            completeAttrRef = node.value.id + '.' + node.attrText
            self._registerVarRef(completeAttrRef, node.value)
        #--
        pass

        if ctxClass == ast.Load:
            self.addCode('GETATTR', node.attr)
        elif ctxClass == ast.Store:
            self.addCode('SETATTR', node.attr)

    def _handleIfExp(self, node):
        self._genCode(node.test)
        iPos = len(self.codes)
        self.codes.append(None)
        self._genCode(node.body)
        iEndBody = len(self.codes)
        self.codes.append(None)
        iElseTarget = len(self.codes)
        self.setCodeAt(iPos, 'JNE', iElseTarget)
        self._genCode(node.orelse)
        iEndTarget = len(self.codes)
        self.setCodeAt(iEndBody, 'JMP', iEndTarget)

    def _handleModule(self, node):
        self._genCode(node.body, True)

    def _handleAssign(self, node):
        if len(node.targets) > 1: # or not isinstance(node.targets[0], ast.Name):
            raise Exception, 'Unsupported syntax (multiple assignment)'
        self._genCode(node.value)
        self._genCode(node.targets[0])

    def _handlePrint(self, node):
        self._genCode(node.values, False)
        self.addCode('PRINT', len(node.values), int(node.nl))

    def _handlePass(self, node):
        pass

    def _handleIf(self, node):
        self._genCode(node.test)
        iPos = len(self.codes)
        self.codes.append(None)
        self._genCode(node.body, True)
        iEndBody = len(self.codes)
        if len(node.orelse) > 0: 
            self.codes.append(None)
        iElseTarget = len(self.codes)
        self.setCodeAt(iPos, 'JNE', iElseTarget)
        self._genCode(node.orelse, True)
        iEndTarget = len(self.codes)
        if len(node.orelse) > 0:
            self.setCodeAt(iEndBody, 'JMP', iEndTarget)

    def _handleWhile(self, node):
        iContext = len(self.codes)
        self.codes.append(None)
        iStart = len(self.codes)
        self._genCode(node.test)
        iCondition = len(self.codes)
        self.codes.append(None)
        self._genCode(node.body, True)
        self.addCode('JMP', iStart)
        iTarget = len(self.codes)
        self.setCodeAt(iContext, 'PUSHLOOPCTX', iTarget, iStart) # pushloopctx is used to define 'break' and 'continue' behavior
        self.setCodeAt(iCondition, 'JNE', iTarget)

    def _handleBreak(self, node):
        self.addCode('BREAK')

    def _handleContinue(self, node):
        self.addCode('CONTINUE')

    def _handleReturn(self, node):
        self._genCode(node.value)
        self.addCode('RETURN')

    def _handleFunctionDef(self, node):
        iStart = len(self.codes)
        self.codes.append(None)
        argInfo = node.args
        if self.mode == CODEGEN_MODE_FREESTYLE:
            if len(self.functionStack) == 0:
                if node.name == 'main':
                    if self.mainDefined:
                        raise Exception, 'Duplicate main() as entry function'
                    if len(argInfo.args) != 0:
                        raise Exception, 'main() shall contain no parameters'
                    self.mainDefined = True
                #-- 
                pass
            #--
            pass
        #--
        argNames = tuple(map(lambda x: x.id, argInfo.args))
        for val in argInfo.defaults:
            self._genCode(val)
        self.addCode('SETPRM', argNames, len(argInfo.defaults), argInfo.vararg, argInfo.kwarg)
        self._pushFunction(node.name)
        try:
            self._genCode(node.body, True)
            iFinish = len(self.codes)
            self.setCodeAt(iStart, 'DEFUN', node.name, iFinish)
        finally:
            self._popFunction()

    def _handleImport(self, node):
        names = node.names
        for als in names:
            if als.asname != None:
                self.addCode('IMPORT', als.name, als.asname)
            else:
                self.addCode('IMPORT', als.name, als.name)

#import rpdb2; rpdb2.start_embedded_debugger('000')
