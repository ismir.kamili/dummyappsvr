# convertodl.py
# convert ODL (and load all of its references recursively), and then convert to pickle format

import sys
import os
from is_lib import sobject
from is_lib import odlsys
from is_lib import stream
import time
import pickle as pickle
#import dill
import weakref

def main():
    baseArg = 1 if 'python' in sys.argv[0] else 0
    if len(sys.argv) >= baseArg + 1 and sys.argv[baseArg] == '-u':
        baseArg += 1

    if len(sys.argv) < baseArg + 2:
        print('use: python convertodl.py <file name>')
        return

    mdtName = sys.argv[baseArg + 1]
    #import rpdb2; rpdb2.start_embedded_debugger('000')

    odl = odlsys.ODL()
    aStream = stream.StrFileStream(mdtName, "rb")
    odl.fileName = mdtName
    aWrapper = sobject.sobjtextwrapper(aStream)
    sobj = sobject.sobjhelper(0, odlsys.odlMetadata, odl, aWrapper)
    t1 = time.time()

    sobj.loadInstance()
    t2 = time.time()
    print("parse successful (%f secs)!" % (t2 - t1))
    
    #import rpdb2; rpdb2.start_embedded_debugger('000')
    odl.createPhysicalDB()
    for cl in odl.classes:
        cl.buildClsImplStructure()

    odlsys.odlMetadata.clear()

    sys.setrecursionlimit(8192)
    outFileName = mdtName.rsplit('.', 1)[0] + '.pmd'
    outFile = open(outFileName, 'wb')
    try:
        pickle.dump(odl, outFile, pickle.HIGHEST_PROTOCOL) # seek highest protocol
    finally:
        outFile.close()
    print("Metadata was successfully converted to pickle format")


if __name__ == '__main__':
    main()
